<?php 

$nouser = "";    
$nopass = "";
$bademail = "";
$badquery = "";
$userinuse = "";
$emailinuse = "";
$error = false;





    // First we execute our common code to connection to the database and start the session 
    require_once("common.php"); 
     
    // This if statement checks to determine whether the registration form has been submitted 
    // If it has, then the registration code is run, otherwise the form is displayed 
    if(!empty($_POST)) 
    { 
        // Ensure that the user has entered a non-empty username 
        if(empty($_POST['username'])) 
        { 
            // Note that die() is generally a terrible way of handling user errors 
            // like this.  It is much better to display the error with the form 
            // and allow the user to correct their mistake.  However, that is an 
            // exercise for you to implement yourself. 
            $nouser = "Please enter a username."; 
            $error = true;
        } 
         
        // Ensure that the user has entered a non-empty password 
        if(empty($_POST['password'])) 
        { 
            $nopass = "Please enter a password."; 
            $error = true;
        } 
         
        // Make sure the user entered a valid E-Mail address 
        // filter_var is a useful PHP function for validating form input, see: 
        // http://us.php.net/manual/en/function.filter-var.php 
        // http://us.php.net/manual/en/filter.filters.php 
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
        { 
            $noemail = "Invalid E-Mail Address";
            $error = true;
        } 
         
        // We will use this SQL query to see whether the username entered by the 
        // user is already in use.  A SELECT query is used to retrieve data from the database. 
        // :username is a special token, we will substitute a real value in its place when 
        // we execute the query. 
        $query = " 
            SELECT 
                1 
            FROM useraccounts 
            WHERE 
                username = :username 
        "; 
         
        // This contains the definitions for any special tokens that we place in 
        // our SQL query.  In this case, we are defining a value for the token 
        // :username.  It is possible to insert $_POST['username'] directly into 
        // your $query string; however doing so is very insecure and opens your 
        // code up to SQL injection exploits.  Using tokens prevents this. 
        // For more information on SQL injections, see Wikipedia: 
        // http://en.wikipedia.org/wiki/SQL_Injection 
        $query_params = array( 
            ':username' => $_POST['username'] 
        ); 
         
        try 
        { 
            // These two statements run the query against your database table. 
            $stmt = $conn->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) 
        { 
            // Note: On a production website, you should not output $ex->getMessage(). 
            // It may provide an attacker with helpful information about your code.  
            $badquery = "Failed to run query: " . $ex->getMessage(); 
            $error = true;
        } 
         
        // The fetch() method returns an array representing the "next" row from 
        // the selected results, or false if there are no more rows to fetch. 
        $row = $stmt->fetch(); 
         
        // If a row was returned, then we know a matching username was found in 
        // the database already and we should not allow the user to continue. 
        if($row) 
        { 
            $userinuse = "This username is already in use"; 
            $error = true;
        } 
         
        // Now we perform the same type of check for the email address, in order 
        // to ensure that it is unique. 
        $query = " 
            SELECT 
                1 
            FROM useraccounts 
            WHERE 
                email = :email 
        "; 
         
        $query_params = array( 
            ':email' => $_POST['email'] 
        ); 
         
        try 
        { 
            $stmt = $conn->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) 
        { 
            $badquery = "Failed to run query: " . $ex->getMessage(); 
            $error = true;
        } 
         
        $row = $stmt->fetch(); 
         
        if($row) 
        { 
            $emailinuse = "This email address is already registered"; 
            $error = true;
        } 
    
        if(!$error){

            // An INSERT query is used to add new rows to a database table. 
            // Again, we are using special tokens (technically called parameters) to 
            // protect against SQL injection attacks. 
            // $query = " 
            //     INSERT INTO useraccounts ( 
            //         username, 
            //         password, 
            //         salt, 
            //         email,
            //         con_code,
            //         login_type
            //     ) VALUES ( 
            //         :username, 
            //         :password, 
            //         :salt, 
            //         :email,
            //         :con_code,
            //         :login_type
            //     ) 
            // "; 
            $query = " 
                INSERT INTO useraccounts ( 
                    username, 
                    password, 
                    salt, 
                    email,
                    login_type
                ) VALUES ( 
                    :username, 
                    :password, 
                    :salt, 
                    :email,
                    :login_type
                ) 
            "; 

            //grab the username
            $username = $_POST['username'];
            $email =  $_POST['email'];
            //random email confirmation code
            // $con_code = generateRandomString(20);
            
            // A salt is randomly generated here to protect again brute force attacks 
            // and rainbow table attacks.  The following statement generates a hex 
            // representation of an 8 byte salt.  Representing this in hex provides 
            // no additional security, but makes it easier for humans to read. 
            // For more information: 
            // http://en.wikipedia.org/wiki/Salt_%28cryptography%29 
            // http://en.wikipedia.org/wiki/Brute-force_attack 
            // http://en.wikipedia.org/wiki/Rainbow_table 
            $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
            
            // This hashes the password with the salt so that it can be stored securely 
            // in your database.  The output of this next statement is a 64 byte hex 
            // string representing the 32 byte sha256 hash of the password.  The original 
            // password cannot be recovered from the hash.  For more information: 
            // http://en.wikipedia.org/wiki/Cryptographic_hash_function 
            $password = hash('sha256', $_POST['password'] . $salt); 
            
            // Next we hash the hash value 65536 more times.  The purpose of this is to 
            // protect against brute force attacks.  Now an attacker must compute the hash 65537 
            // times for each guess they make against a password, whereas if the password 
            // were hashed only once the attacker would have been able to make 65537 different  
            // guesses in the same amount of time instead of only one. 
            for($round = 0; $round < 65536; $round++) 
            { 
                $password = hash('sha256', $password . $salt); 
            } 

            
            
            // Here we prepare our tokens for insertion into the SQL query.  We do not 
            // store the original password; only the hashed version of it.  We do store 
            // the salt (in its plaintext form; this is not a security risk). 
            $query_params = array( 
                ':username' => $_POST['username'], 
                ':password' => $password, 
                ':salt' => $salt, 
                ':email' => $_POST['email'],
                // ':con_code' => $con_code,
                ':login_type' => 'Local'
            ); 
            
            try 
            { 
                // Execute the query to create the user 
                $stmt = $conn->prepare($query); 
                $result = $stmt->execute($query_params);
                
                //no error, so send confirmation email
                // $to = $email;
                // $subject = "Confirmation from BullCryp.com to $username";
                // $header = "From: stefano@bullcryp.com";
                // $message = "Please click the link below to verify and activate your account. rn";
                // $message .= "http://www.bullcryp.com/login/confirm.php?passkey=$con_code";

                // $sentmail = mail($to,$subject,$message, $header);
                
            } 
            catch(PDOException $ex) 
            { 
                // Note: On a production website, you should not output $ex->getMessage(). 
                // It may provide an attacker with helpful information about your code.  
                $badquery = "Failed to run query: " . $ex->getMessage()
                ." salt:".$salt
                ." pass:".$password
                ." email:".$email
                ." con_code:".$con_code; 
                $error = true;
            } 
        }
        // This redirects the user back to the login page after they register 
        if(!$error){
            header("Location: ../oauth/login.php?regsuccess=true");
            die("Redirecting to login.php");  
        }
        
    } 

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //$badquery = "";  //comment for db debugging
?> 

<?php 
    if($badquery){
        echo '<div class="error">' . $badquery . '</div>';
    }
?>
<form action="login.php?register=true" method="post"> 
    <?php if($nouser){
        echo '<div class="error">' . $nouser . '</div>';
    }
    if($userinuse){
        echo '<div class="error">' . $userinuse . '</div>';
    } ?>
    <input class="form-control bb" placeholder="Username" type="text" name="username" value="" /> 
    <br /><br /> 
    <?php if($emailinuse){
        echo '<div class="error">' . $emailinuse . '</div>';
    }
    if($bademail){
        echo '<div class="error">' . $bademail . '</div>';
    } ?>
    <input class="form-control bb"  placeholder="Email" type="text" name="email" value="" /> 
    <br /><br /> 
    <?php if($nopass){
        echo '<div class="error">' . $nopass . '</div>';
    } ?>
    <input class="form-control bb"  placeholder="Password" type="password" name="password" value="" /> 
    <br /><br /> 
    <input class="loginbutton btn-sm noshadow waves-effect waves-light" style="background-color: white" type="submit" value="Register" />  
</form>
<div class="loginregister"><a href="login.php">Return to Sign in</a></div>