<?php

require_once("common.php"); 

$passkey = $_GET['passkey'];

$query = "UPDATE useraccounts SET concode=NULL WHERE concode= :passkey";
$query_params = array(
  ':passkey' => $passkey
);

try 
{ 
    // Execute the query to update the user 
    $stmt = $conn->prepare($query); 
    $result = $stmt->execute($query_params); 

    echo '<div>Your account is now active. You may now <a href="https://www.bullcryp.com/oauth/login.php">Log In</a></div>';
} 
catch(PDOException $ex) 
{ 
    // Note: On a production website, you should not output $ex->getMessage(). 
    // It may provide an attacker with helpful information about your code.  
    die("An error occured. Please contact the administrator, or try again"); 
    
    


} 


