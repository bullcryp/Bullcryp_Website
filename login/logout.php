<?php
// First we execute our common code to connection to the database and start the session 
    require_once("common.php"); 

    //remove the token
    $query = "UPDATE useraccounts SET access_token = NULL WHERE salt = :salt";
            $query_params = array(
                ':salt' => $_SESSION['user']['salt']
            );
            try 
            { 
                // Execute the query to update the user 
                $stmt = $conn->prepare($query); 
                $result = $stmt->execute($query_params); 
            } 
            catch(PDOException $ex) 
            { 
                // Note: On a production website, you should not output $ex->getMessage(). 
                // It may provide an attacker with helpful information about your code.  
                //die("Failed to run query: " . $ex->getMessage()); 
                $error = true;
            } 

     
    // We remove the user's data from the session 
    unset($_SESSION['user']); 
     
    // We redirect them to the login page 
    header("Location: ../oauth/login.php"); 
    die("Redirecting to: ../oauth/login.php");
?>