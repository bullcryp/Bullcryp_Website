<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
	<title>BullCryp | Contact Us</title>
    <meta name="description" content="Have a question about our cryptocurrency trading platform? Let us know. We'll answer within 24 hours. Redefine your investing with BullCryp Trader.">
    <meta name="keywords" content="contact,question,faq,contact us,crypto,cryptocurrency,tokens,btc price,btc trader,bitcoin trader,short bitcoin,trade crypto,trade coins,day trading,swing trading,daytrader,trading tool,charts,crypto charting,technical analysis,trends,dashboard,crypto dash,cryptocurrency bot">
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
	<!-- <link rel="stylesheet" href="css/index.css"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<link href="css/bootstrap.css" rel="stylesheet" />
    <!-- <link href="css/bootstrap-theme.css" rel="stylesheet" /> -->
    <link href="css/contact.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-md fixed-top navbar-light">
        <a class="navbar-brand" href="https://bullcryp.com">
            <img src="img/logo.png" class="d-inline-block align-top brand-img" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" href="features.php">Features</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="https://medium.com/bullcryp">Blog</a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="#">Support</a>
                </li> -->
                <li class="nav-item">
                <a class="nav-link orange-button btn" href="oauth/login.php" id="dashboard-btn">Try the Beta</a>
                </li>
        </ul>
        </div>
    </nav>
    <div class="container-fluid trader-background" id="contact-header">
        <h1 class="white-text centered">Contact</h1>
        <p class="dark-orange-text centered">We'll respond within 24 hours.</p>
    </div>
    <div class="container-fluid">
        <h2>Reach out to us.</h2>
        <p>From feature requests to bug reports, we'd be happy to help you out. We'll never share any of your personal information.</p>
        <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputFirstName" placeholder="First Name" required>
              </div>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputLastName" placeholder="Last Name" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputEmail" placeholder="Email" required>
              </div>
              <div class="form-group col-md-6">
                <select id="inputTopic" class="form-control">
                  <option selected>General Inquiry</option>
                  <option>Product Feedback / Suggestions</option>
                  <option>Technical Issue</option>
                  <option>Business Inquiries</option>
                </select>
              </div>
            </div>
            <div class="form-group">
                <textarea class="form-control" id="inputDetails" placeholder="How can we help?" rows="10" required></textarea>
            </div>
            <button type="submit" class="btn orange-button">Submit</button>
          </form>
    </div>
    <!-- <div class="container-fluid trader-background">
        <h2 class="white-text" id="faq-footer">Get an instant answer.</h2>
        <p class="dark-orange-text">We've posted answers to popular questions on our FAQ.</p>
        <a href="#" class="orange-button btn">See FAQ</a>
    </div> -->
   
    <?php include("footer.php"); ?>
</body>	
</html>
