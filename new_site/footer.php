<footer>
  <div class="footer">
      <div class="row">
          <div class="col-md-4 d-none d-md-block">
              <div id="footer-logo-bullcryp" class="d-inline-block"><img src="img/logo.png" class="brand-img"></div>
              <div id="footer-bullcryp" class="d-inline-block">
                  <h5 class="footer-title">BullCryp</h5>
                  <p class="footer-sub-text">Cryptocurrency Trading, Reinvented.</p>
                  <p class="dark-orange-text footer-sub-text">Made with ♥ in Vancouver, Canada</p>
              </div>
          </div>
          <div class="col-md-4">
              <h5 class="footer-title">Connect</h5>
              <div>
                  <div class="d-inline-block">
                      <a href="https://www.facebook.com/bullcryp " target="_blank " title="BullCryp Facebook">
                      <img class="social" src="img/icons/facebook.png ">
                      </a>
                  </div>
                  <div class="d-inline-block">
                      <a href="https://www.instagram.com/bullcryp/ " target="_blank " title="BullCryp Instagram">
                      <img class="social" src="img/icons/instagram.png ">
                      </a>
                  </div>
                  <div class="d-inline-block">
                      <a href="https://twitter.com/realbullcryp " target="_blank " title="BullCryp Twitter ">
                      <img class="social" src="img/icons/twitter.png ">
                      </a>
                  </div>
                  <div class="d-inline-block">
                      <a href="https://www.linkedin.com/company/bullcryp/" target="_blank " title="BullCryp LinkedIn">
                      <img class="social" src="img/icons/linkedin.png ">
                      </a>
                  </div>
                  <div class="d-inline-block">
                      <a href="https://medium.com/bullcryp" target="_blank " title="BullCryp Medium">
                      <img class="social" src="img/icons/medium.png ">
                      </a>
                  </div>
              </div>
          </div>
          <div class="col-md-2">
                  <div class="footer-links">
                          <a href="oauth/login.php">Dashboard</a><br>
                          <a href="features.php">Features</a><br>
                          <a href="https://medium.com/bullcryp">Blog</a><br>
                  </div>
              </div>
          <div class="col-md-2">
              <div class="footer-links">
                      <a href="contact.php">Support</a><br>
                      <a href="https://bullcryp.com/terms.html" onclick="javascript:void window.open('https://bullcryp.com/terms.html','1537382275936','width=700,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Terms | Privacy</a><br>
                      <span class="dark-orange-text">
                      Copyright &#169; 2018 Bullcryp
                      </span>
              </div>
          </div>
      </div>
  </div>
</footer>
<!-- <script src="js/bootstrap.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- @@@ load tracking js file -->
<?php include("tracking.html") ?>