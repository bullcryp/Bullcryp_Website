<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
	<title>BullCryp | The Better, Smarter Way to Trade Cryptocurrency</title>
    <meta name="description" content="BullCryp is an all-in-one cryptocurrency trading and portfolio management platform. We integrate with various exchanges and use data aggregation and visualization to make you a better trader. Our dashboard is free, secure, and fast - sign up now!">
    <meta name="keywords" content="crypto,cryptocurrency,tokens,btc price,btc trader,bitcoin trader,short bitcoin,trade crypto,trade coins,day trading,swing trading,daytrader,trading tool,charts,crypto charting,technical analysis,trends,dashboard,crypto dash,cryptocurrency bot">
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
	<!-- <link rel="stylesheet" href="css/index.css"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<link href="css/bootstrap.css" rel="stylesheet" />
    <!-- <link href="css/bootstrap-theme.css" rel="stylesheet" /> -->
    <link href="css/homepage.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-md fixed-top navbar-light">
        <a class="navbar-brand" href="https://bullcryp.com">
            <img src="img/logo.png" class="d-inline-block align-top brand-img" alt="">
        </a>
        <button class="navbar-toggler " type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="features.php">Features</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="https://medium.com/bullcryp">Blog</a>
            </li>
            <!-- <li class="nav-item">
            <a class="nav-link" href="#">Support</a>
            </li> -->
            <li class="nav-item">
            <a class="nav-link orange-button btn" href="oauth/login.php" id="dashboard-btn">Try the Beta</a>
            </li>
        </ul>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-6 order-md-2" id="dashboard-preview">
                    <img src="img/pc_preview.png" class="img-fluid" alt="">
            </div>
            <div class="col-md-6">
                <h1>BullCryp Trader</h1>
                <h4>A free-to-use, multi-exchange dashboard that lets you trade and manage cryptocurrencies with ease.</h2>
                <div id="header-buttons">
                    <a href="oauth/login.php" class="orange-button btn">Get Started</a>
                    <a href="features.php" class="orange-border-button btn">Learn More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid grey-background">
        <h2 class="feature-container-header">The all-in-one cryptocurrency solution</h2>
        <div class="row align-items-center">
            <div class="col-md-6">
                <img src="img/integration_graphic.png" class="img-fluid feature-img" alt="">
            </div>
            <div class="col-md-6">
                <p>No more jumping between exchanges. BullCryp lets you trade across multiple exchanges on one intuitive platform. We plug into multiple data sources so that you are always in the know.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <h2 class="feature-container-header">Feature packed, yet easy to use</h2>
        <div class="row align-items-center">
            <div class="col-md-6 centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">100+ Technical Indicators</p>
                </div>
            </div>
            <div class="col-md-6 centered">
                <div class="feature-item">
                        <img src="img/tick.png" class="tick-mark" alt="">
                        <p class="d-inline-block">Live news &amp; social feeds</p>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
                <div class="col-md-6 centered">
                    <div class="feature-item">
                        <img src="img/tick.png" class="tick-mark" alt="">
                        <p class="d-inline-block">Advanced order placement</p>
                    </div>
                </div>
                <div class="col-md-6 centered">
                    <div class="feature-item">
                            <img src="img/tick.png" class="tick-mark" alt="">
                            <p class="d-inline-block">Smart price monitoring</p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                    <div class="col-md-6 centered">
                        <div class="feature-item">
                            <img src="img/tick.png" class="tick-mark" alt="">
                            <p class="d-inline-block">Live orderbook depth charts</p>
                        </div>
                    </div>
                    <div class="col-md-6 centered">
                        <div class="feature-item">
                                <img src="img/tick.png" class="tick-mark" alt="">
                                <p class="d-inline-block">24/7 portfolio tracking</p>
                        </div>
                    </div>
                </div>
        <div class="centered">
            <a href="features.php" class="btn orange-border-button">See All Features</a>
        </div>
    </div>
    <div class="container-fluid grey-background">
        <h2 class="feature-container-header">Security is our #1 priority</h2>
        <div class="row align-items-center">
            <div class="col-md-6">
                <img src="img/security_symbol.png" class="img-fluid feature-img" alt="">
            </div>
            <div class="col-md-6">
                <p>The safety of your funds is of utmost importance. We don't store your API keys, nor do we have withdrawal permissions. We've also implemented the latest encryption protocols, so that you can trade without worry.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid trader-background">
        <h2 class="white-text" id="get-started-header">It's easy to get started.</h2>
        <p class="dark-orange-text">Cryptocurrency management, redefined. Effortlessly manage and analyze your crypto-assets across a multitude of exchanges.</p>
        <a href="oauth/login.php" class="orange-button btn">Start Now</a>
    </div>
    
    <?php include("footer.php"); ?>
</body>	
</html>
