<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
	<title>BullCryp | Cryptocurrency Dashboard Features</title>
    <meta name="description" content="BullCryp is an all-in-one cryptocurrency trading and portfolio management platform. See our entire list of features - including 24/7 portfolio monitoring and the latest market movers.">
    <meta name="keywords" content="features,crypto,cryptocurrency,tokens,btc price,btc trader,bitcoin trader,short bitcoin,trade crypto,trade coins,day trading,swing trading,daytrader,trading tool,charts,crypto charting,technical analysis,trends,dashboard,crypto dash,cryptocurrency bot">
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
	<!-- <link rel="stylesheet" href="css/index.css"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  	<link href="css/bootstrap.css" rel="stylesheet" />
    <!-- <link href="css/bootstrap-theme.css" rel="stylesheet" /> -->
    <link href="css/features.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-md fixed-top navbar-light">
        <a class="navbar-brand" href="https://bullcryp.com">
            <img src="img/logo.png" class="d-inline-block align-top brand-img" alt="">
        </a>
        <button class="navbar-toggler " type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="features.php">Features</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="https://medium.com/bullcryp">Blog</a>
            </li>
            <!-- <li class="nav-item">
            <a class="nav-link" href="#">Support</a>
            </li> -->
            <li class="nav-item">
            <a class="nav-link orange-button btn" href="oauth/login.php" id="dashboard-btn">Try the Beta</a>
            </li>
        </ul>
        </div>
    </nav>
    <div class="container-fluid trader-background">
        <h1 class="white-text centered">Features</h1>
        <p class="dark-orange-text centered">One platform. Everything you need.</p>
    </div>
    <div class="container-fluid-wide">
        <div class="row">
            <div class="col-md-4">
                <img src="img/statistics.png" class="img-fluid top-feature-img centered" alt="">
                <h4 class="centered">Multi-exchange integration</h4>
            </div>
            <div class="col-md-4">
                <img src="img/money-magnify-amber.png" class="img-fluid top-feature-img centered" alt="">
                <h4 class="centered">24/7 Portfolio Monitoring</h4>
            </div>
            <div class="col-md-4">
                <img src="img/gears-desktop-amber.png" class="img-fluid top-feature-img centered" alt="">
                <h4 class="centered">Advanced charting & analysis</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid grey-background">
        <img src="img/transfer.png" class="img-fluid feature-img centered" alt="">
        <h3 class="dark-orange-text centered feature-container-header">Live data feeds to keep you up to date.</h3>
        <div class="feature-details">
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Portfolio analysis - price tracking, asset weight analysis, relevant news</p>
                </div>
            </div>
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Market Analysis - Top gainers and losers, latest market activity</p>
                </div>
            </div>
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Live market data feed - news, social media, cryptocurrency events</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <img src="img/statistics.png" class="img-fluid feature-img centered" alt="">
        <h3 class="dark-orange-text centered feature-container-header">Advanced analysis. Easy Trading</h3>
        <div class="feature-details">
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Multi-exchange integration - Poloniex & Binance currently supported</p>
                </div>
            </div>
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Advanced TradingView charts - 100+ technical indicators & annotations</p>
                </div>
            </div>
            <div class="centered">
                <div class="feature-item">
                    <img src="img/tick.png" class="tick-mark" alt="">
                    <p class="d-inline-block">Market & Portfolio statistics - holdings analysis and the latest market movers</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid grey-background">
            <img src="img/networking.png" class="img-fluid feature-img centered" alt="">
            <h3 class="dark-orange-text centered feature-container-header">Live data from countless integrations</h3>
            <div class="feature-details">
                <div class="centered">
                    <div class="feature-item">
                        <img src="img/tick.png" class="tick-mark" alt="">
                        <p class="d-inline-block">Live CoinMarketCap data to keep track of the hottest cryptos</p>
                    </div>
                </div>
                <div class="centered">
                    <div class="feature-item">
                        <img src="img/tick.png" class="tick-mark" alt="">
                        <p class="d-inline-block">Latest market news from CoinTelegraph, CoinDesk, and much more</p>
                    </div>
                </div>
            </div>
        </div>
    <div class="container-fluid trader-background">
        <h2 class="white-text" id="get-started-header">Fully Featured. Completely Free</h2>
        <p class="dark-orange-text">Have a question? We'll respond within 24 hours. Have a feature request? We're happy to take suggestions.</p>
        <a href="contact.php" target="_blank" class="orange-button btn">Contact Us</a>
    </div>
   
    <?php include("footer.php"); ?>

</body>	
</html>
