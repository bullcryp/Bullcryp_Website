<?php
// written by Alvin Khoo (c) May 2017
// API - returnPair?pair=BTC_EMC2&start=2017-05-20T21:10:00Z&end=2017-05-21T01:10:00Z
// default - returns pair=BTC_ETH --> start = NOW(), end == - 4 hours
// hour_group - returns pair=BTC_ETH&group=true --> start = NOW(), end == - 4 hours

ini_set('memory_limit', '512M');

$pair = isset($_REQUEST['pair']) ? $_REQUEST['pair'] : 'BTC_ETH' ;
$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : '' ; // !important! start and end flipped
$end = isset($_REQUEST['end']) ? $_REQUEST['end'] : '';
$group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';

// PDO style
$servername = "localhost";
$username = "polo_user";
$password = "HeaVenLy";
$myDB = "polodb";

try {
    $pdo_conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password );
    // set the PDO error mode to exception
    $pdo_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully $start $end\n<br>";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    exit(1);
}


//$sql = "SELECT * from `$pair` WHERE `startDate` < NOW() AND `endDate` > DATE_SUB(NOW(),INTERVAL 4 HOUR);"; // default
$sql = "SELECT * from `$pair` ORDER by `startDate` DESC limit 480";
//$sql_start = "SELECT * from `$pair` WHERE `startDate` < :start AND `endDate` > DATE_SUB( :start2, INTERVAL 4 HOUR );";
$sql_end = "SELECT * from `$pair` WHERE `endDate` <= :end ORDER by `startDate` DESC limit 480;";
$sql_startend = "SELECT * from `$pair` WHERE `endDate` <= :end AND `startDate` > :start ORDER by `startDate` DESC ";
$sql_group_all = "select endDate, close, sell_amount, buy_amount, buy_trades, sell_trades, low,  high from `$pair` order by endDate DESC"; // 1 week 
$sql_group_week = "select endDate, close, sell_amount, buy_amount, buy_trades, sell_trades, low,  high from `$pair` WHERE  (`endDate` < :end AND `endDate` > DATE_SUB(:end2,INTERVAL 1 WEEK)) ORDER BY endDate DESC"; // entire DB
$sql_group_minute = "select concat( date(endDate) , ' ', sec_to_time(time_to_sec(endDate)- time_to_sec(endDate)%(15*60) + (15*60))) as pairDate,AVG(close) as close ,SUM(sell_amount) as sell_amount ,SUM(buy_amount) as buy_amount, SUM(buy_trades) as buy_trades, SUM(sell_trades) as sell_trades, MIN(low) as low, MAX(high) as high from `$pair` group by pairDate";
$sql_group_hour = "select CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d %H'), ':00:00') as endDate,AVG(close) as close ,SUM(sell_amount) as sell_amount ,SUM(buy_amount) as buy_amount, SUM(buy_trades) as buy_trades, SUM(sell_trades) as sell_trades, MIN(low) as low, MAX(high) as high from `$pair` group by  CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d %H'), ':00:00')";
//$sql_group_hour = "select CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d %H'), ':00:00') as endDate, SUM(sell_amount) as sell_amount ,SUM(buy_amount) as buy_amount, SUM(buy_trades) as buy_trades, SUM(sell_trades) as sell_trades, MIN(low) as low, MAX(high) as high,  SUBSTRING_INDEX(GROUP_CONCAT(CAST(close AS CHAR) ORDER BY endDate DESC), ',', 1) AS close from `$pair` group by  CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d %H'), ':00:00')";
$sql_group_month = "select floor(hour(endDate) / 4) as hourgroup, MIN(CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d %H'), ':00:00')) as endDate, CONCAT(DATE_FORMAT(endDate, '%Y-%m-%d '), '00:00:00') as daygroup,AVG(close) as close ,SUM(sell_amount) as sell_amount ,SUM(buy_amount) as buy_amount, SUM(buy_trades) as buy_trades, SUM(sell_trades) as sell_trades, MIN(low) as low, MAX(high) as high from `$pair` group by  daygroup, hourgroup";


if ($start && $end) {
	$sql = $sql_startend; // contains $start & $end
} else if ($group == 'all') {
	$sql = $sql_group_all; // contains $start only
} else if ($group == 'week') {
	$sql = $sql_group_week; // contains $start only
} else if ($group == 'month') {
	$sql = $sql_group_month; // contains $start only
} else if ($group == 'hour') {
	$sql = $sql_group_hour; // contains $start only
} else if ($group) {
	$sql = $sql_group_minute; // contains $start only
} else if ($end) {
	$sql = $sql_end; // contains $start only
}

// echo $sql; // debug

$coin_pairs = array("BTC_AMP","BTC_ARDR","BTC_BCN","BTC_BCY","BTC_BELA","BTC_BLK","BTC_BTCD","BTC_BTM","BTC_BTS","BTC_BURST","BTC_CLAM","BTC_DASH","BTC_DCR","BTC_DGB","BTC_DOGE","BTC_EMC2","BTC_ETC","BTC_ETH","BTC_EXP","BTC_FCT","BTC_FLDC","BTC_FLO","BTC_GAME","BTC_GNO","BTC_GNT","BTC_GRC","BTC_HUC","BTC_LBC","BTC_LSK","BTC_LTC","BTC_MAID","BTC_NAUT","BTC_NAV","BTC_NEOS","BTC_NMC","BTC_NOTE","BTC_NXC","BTC_NXT","BTC_OMNI","BTC_PASC","BTC_PINK","BTC_POT","BTC_PPC","BTC_RADS","BTC_REP","BTC_RIC","BTC_SBD","BTC_SC","BTC_SJCX","BTC_STEEM","BTC_STR","BTC_STRAT","BTC_SYS","BTC_VIA","BTC_VRC","BTC_VTC","BTC_XBC","BTC_XCP","BTC_XEM","BTC_XMR","BTC_XPM","BTC_XRP","BTC_XVC","BTC_ZEC","ETH_ETC","ETH_GNO","ETH_GNT","ETH_LSK","ETH_REP","ETH_STEEM","ETH_ZEC","USDT_BTC","USDT_DASH","USDT_ETC","USDT_ETH","USDT_LTC","USDT_NXT","USDT_REP","USDT_STR","USDT_XMR","USDT_XRP","USDT_ZEC","XMR_BCN","XMR_BLK","XMR_BTCD","XMR_DASH","XMR_LTC","XMR_MAID","XMR_NXT","XMR_ZEC");

// simple check to make sure $pair exists in $coin_pairs
// https://stackoverflow.com/questions/19363634/php-pdo-query-troubleshoot
if (!in_array($pair, $coin_pairs)) {
	return [];
}

//echo $sql;

$myResults = getpair($pdo_conn, $sql);
echo json_encode( $myResults ); // output in [{..trade..}, {..trade2..}, {..}] JSON format

//foreach ($myResults as $key => $value) {
    //echo "{$key} => {$value} ";
//    $output[] = ($value["attributes"]);
//}


$pdo_conn = null; // close() connection;

/* Helper DB function */
function getpair($conn, $query) {
	global $start, $end, $group;
	$stmt = $conn->prepare($query);

	if ( !empty($end) && !empty($start) ) { // if $end, assume $start exists
		$stmt->bindParam(':start', $start, PDO::PARAM_STR);
		$stmt->bindParam(':end', $end, PDO::PARAM_STR);
	} else if ($group == 'week') { // else check for $start
		$stmt->bindParam(':end', $end, PDO::PARAM_STR);
		$stmt->bindParam(':end2', $end, PDO::PARAM_STR);
	} else if (!empty($end)) { // else check for $start
		$stmt->bindParam(':end', $start, PDO::PARAM_STR);
	}

	$stmt->execute();
	$row = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// Check if array is empty
	if (empty($row)) { // EMPTY
	    return [];
	} else { // FOUND EMAIL
	    return array_reverse($row);
	}
}


