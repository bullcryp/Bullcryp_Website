<?php

ob_start();
  /**
   * initialize Hybridauth.
   */
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

  include 'vendor/autoload.php';
  include 'config.php';

  use Hybridauth\Hybridauth;

  $hybridauth = new Hybridauth($config);
  $adapters = $hybridauth->getConnectedAdapters();

    if (!$adapters) {
        // Not Logged In With Oauth 

        //.. so Check for Regular login 
        // First we execute our common code to connection to the database and start the session 
        require_once("../login/common.php"); 
    
        // At the top of the page we check to see whether the user is logged in or not 
        if(empty($_SESSION['user'])) 
        { 
            // If they are not, we stay on the login page. 
        } else {
            //we can redirect them to the trader
            header('Location: https://bullcryp.com/trader/');
            die("directing to trader");
        }
    }else{
        //user is Oauth logged in, redirect the user to the trader 
        header('Location: https://bullcryp.com/trader/'); 
        die("directing to trader");
    }
?> 


<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Bullcryp login</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet"> 

    <link rel="stylesheet" href="style.css">
    <!-- <link href = "style.css" rel="stylesheet"> -->
    
</head>
<body>

    <div class="login">
        <div class="mmessage">BullCryp is not yet optimized for mobile, but it's coming soon. In the meantime, have a look around..</div>
        <div id="logo">
            <img id="bullcryp_logo" src="../imgs/app_icon.png" alt="Bullcryp logo">
            <?php $a = $_SERVER["QUERY_STRING"]; 
                  if(strpos($a,'register') !==false): ?>
                <h1>Register for BullCryp</h1> 
            <?php else: ?>
                <h1>BullCryp Sign In</h1>
            <?php endif ?>
            
        </div>
        
        <div class="standard-login">
            <div>
                <?php 
                if ( strpos($a,'register') !==false){
                    include '../login/register.php';
                }else{
                    include '../login/login.php';
                } 
                ?>
            </div>
            <div style="position: relative; padding-right: 6px">
                <div class="or">
                    or 
                </div>
                <div class="verLn"></div>
            </div>
            
        </div>
            
        <div class="social-login">
            
            <div class="buttonwrapper waves-effect">
                <a href="https://bullcryp.com/oauth/callback.php?provider=LinkedIn">
                    <button type="button" class="btn btn-primary waves-effect" style="background-color: #2a3e49 !important">
                        <i class="fab fa-2x fa-linkedin"></i>
                        Sign in with linkedin
                    </button>
                </a>
            </div>
            <div class="buttonwrapper waves-effect">
                <a href="https://bullcryp.com/oauth/callback.php?provider=Twitter">
                    <button type="button" class="btn btn-primary waves-effect" style="">
                        <i class="fab fa-2x fa-twitter"></i>
                        Sign in with Twitter
                    </button>
                </a>
            </div>
            <div class="buttonwrapper waves-effect">
                <a href="https://bullcryp.com/oauth/callback.php?provider=Google">
                    <button type="button" class="btn btn-primary waves-effect" style="background-color: #c83525 !important;">
                        <i class="fab fa-2x fa-google-plus"></i>
                        Sign in with Google
                    </button>
                </a>
            </div>
            <div class="buttonwrapper waves-effect">
                <a href="https://bullcryp.com/oauth/callback.php?provider=Facebook">
                    <button type="button" class="btn btn-primary waves-effect" style="background-color: #374c8d !important;">
                        <i class="fab fa-2x fa-facebook"></i>
                        Sign in with facebook
                    </button>
                </a>
            </div>
        </div>
        <div class="backhome">
            <a href="http://www.bullcryp.com">Return to Home Page</a>
        </div>  
        <div class="mainmessage">We recommend Chrome browser for the best experience</div>      
    </div>


        
</body>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-2864028-14"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-2864028-14');
    </script>


</html>

