<?php
/**
 * Build a simple HTML page with multiple providers.
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

include 'vendor/autoload.php';
include 'config.php';

use Hybridauth\Hybridauth;
// use Hybridauth\HttpClient;
$hybridauth = new Hybridauth($config);
$adapters = $hybridauth->getConnectedAdapters();
?>

<?php if (!$adapters) : ?>
    <!-- Not logged in -->
<?php endif; ?>


<?php if ($adapters) { 
    if (isset( $_GET['debug']) ) {  ?>
    <ul>
        <?php foreach ($adapters as $name => $adapter) : ?>
            <li>
                <strong><?php print $adapter->getUserProfile()->displayName; ?></strong> from
                <i><?php print $name; ?></i>
                <span>(<a href="<?php print $config['callback'] . "?logout={$name}"; ?>">Log Out</a>)</span><br>
                <i><?php print json_encode( $adapter->getUserProfile() ); ?></i>
                <i><?php print json_encode( $adapter->getAccessToken() ); ?></i>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
    }else{
        HttpClient\Util::redirect('https://bullcryp.com/trader/index.php');
    }
}; 
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Oauth login</title>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
	<!-- Bootstrap core CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">

    <!-- <link href = "style.css" rel="stylesheet"> -->
    <style>
        
        @media (max-width: 959px) {
            .login {
                grid-template-columns: 1fr
            }
        }
        
        body{
            background-image: url(../../images/unsplash_stocks.jpg);
            background-size: cover;
        }
        .login {
            margin: 20% auto;
            color: white;
            text-align: center;
            width: 90%;
            max-width: 90%;
            display: grid;
            grid-template-columns: 1fr 1fr 1fr;
            grid-template-rows: 90px 200px 200px 200px;
            grid-gap: 140px;
        }
        .btn {
            font-size: 2vw !important;
            width: 300px;
            height: 300px;
        }
        .fa-2x {
            font-size: 5vw !important;
        }
        .login-title h5 {
           font-size: 3rem;
        }

        
    </style>
</head>
<body>



            <div class="login">
                <div class="login-title"><h5>Please login through a provider below</h5></div>
                                                    <div class="buttonwrapper">
                        <a href="https://bullcryp.com/oauth/callback.php?provider=Facebook">
                            <button type="button" class="btn btn-primary">
                                <i class="fab fa-2x fa-facebook"></i><br>
                                Facebook                            </button>
                        </a>
                    </div>
                                                                    <div class="buttonwrapper">
                        <a href="https://bullcryp.com/oauth/callback.php?provider=LinkedIn">
                            <button type="button" class="btn btn-primary">
                                <i class="fab fa-2x fa-linkedin"></i><br>
                                LinkedIn                            </button>
                        </a>
                    </div>
                                                                    <div class="buttonwrapper">
                        <a href="https://bullcryp.com/oauth/callback.php?provider=Google">
                            <button type="button" class="btn btn-primary">
                                <i class="fab fa-2x fa-google-plus"></i><br>
                                Google                            </button>
                        </a>
                    </div>
                                
            </div>


            <!-- <div class="login">
                <div class= "login-title"><h5>Please login through a provider below</h5></div>
                <php foreach ($hybridauth->getProviders() as $name) : ?>
                <php if (!isset($adapters[$name])) : ?>
                    <div class="buttonwrapper">
                        <a href="<php print $config['callback'] . "?provider={$name}"; ?>">
                            <button type="button" class="btn btn-primary">
                                <i class="fab fa-2x <php echo $config['providers'][$name]['font'] ?>"></i><br>
                                <php echo $name ?>
                            </button>
                        </a>
                    </div>
                <php endif; ?>
                <php endforeach; ?>

            </div> -->
</body>
<footer>                  
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js"></script> -->
</footer>   

</html>

