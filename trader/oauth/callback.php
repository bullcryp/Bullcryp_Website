<?php
/**
 * A simple example that shows how to use multiple providers.
 */

include 'vendor/autoload.php';
include 'config.php';
require '/usr/share/nginx/html/api/db.php';

use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

try {
    /**
     * Feed configuration array to Hybridauth.
     */
    $hybridauth = new Hybridauth($config);

    /**
     * Initialize session storage.
     */
    $storage = new Session();

    /**
     * Hold information about provider when user clicks on Sign In.
     */
    if (isset($_GET['provider'])) {
        $storage->set('provider', $_GET['provider']);

        //verify this only happens at signin
        
    }

    /**
     * When provider exists in the storage, try to authenticate user and clear storage.
     *
     * When invoked, `authenticate()` will redirect users to provider login page where they
     * will be asked to grant access to your application. If they do, provider will redirect
     * the users back to Authorization callback URL (i.e., this script).
     */
    if ($provider = $storage->get('provider')) {
        $hybridauth->authenticate($provider);

	// get adapters and user profile
	$adapters = $hybridauth->getConnectedAdapters();
	$adapter_arr = array_keys($adapters);

	$user_profile = $adapters[$adapter_arr[0]]->getUserProfile();
    $user_token = $adapters[$adapter_arr[0]]->getAccessToken();
    // $provider_name = $user_profile->providerName;
	$user_token_partial = substr($user_token['access_token'], -20);

	$user_email = $user_profile->email;
	$user_firstname = $user_profile->firstName;
	$user_lastname = $user_profile->lastName;

	// debug file
	$file = '/tmp/callback.txt';
	$content = $user_email . " - " . $user_token_partial . "\n";

	file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
	// end debug file

	db_account_login($user_email, $user_token_partial, $user_firstname, $user_lastname);

        $storage->set('provider', null);
    }

    /**
     * This will erase the current user authentication data from session, and any further
     * attempt to communicate with provider.
     */
    if (isset($_GET['logout'])) {
        $adapter = $hybridauth->getAdapter($_GET['logout']);
        $adapter->disconnect();
    }

    /**
     * Redirects user to login page (i.e., login.php in our case)
     */
    // HttpClient\Util::getCurrentUrl()
    HttpClient\Util::redirect('https://bullcryp.com/oauth/login.php');
} catch (Exception $e) {
    echo $e->getMessage();
}

