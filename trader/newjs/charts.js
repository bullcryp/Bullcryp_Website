

var pieChartOptions = {
  responsive:true,
  animation: {
    duration: 1000,
    // easing: 'easeOutBack'
  },
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data){
        try {
          let result = app.getCoinInfo(data.labels[tooltipItem.index]);
          let lbl = [];
          let pct = Number(data.datasets[0].data[tooltipItem.index]);
          if(pct < 4) { lbl.push(pct + "% of total holdings"); }
          
          lbl.push("Price (USD): $" + Number(result.price).toFixed(2) );
          lbl.push("24hr Change: " + (Number(result.gainLoss) >= 0?"+":"") + result.gainLoss + "%" );

        return lbl;
          // let label = data.labels[tooltipItem.index] + ": " + 
          //             data.datasets[0].data[tooltipItem.index] + "%"
          // return label
        }catch(e){ console.error(e); }
      },
      title: function(tooltipItem, data){
        let result = app.getCoinInfo(data.labels[tooltipItem[0].index]);
        return result.name;
      }
    },
    displayColors: false
  },

  legend: {
    display: false
  },
  cutoutPercentage: 10,
  title: {
    display: true ,
    text: "",
    fontsize: 20
  },
  scales: {
    display: false,
    yAxes: [],
    xAxes:[]
  },
  pieceLabel: {
    // https://github.com/emn178/Chart.PieceLabel.jsd
    // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
      render: function(label){
        let lbl;
        if(label.percentage >=2){
          lbl =  label.label; 
        }
        if (label.percentage >=4){
          lbl = lbl + "\n" + label.percentage + "%"; 
        }

        if(label.percentage < 5){
          this.fontSize = 8;
          this.fontStyle = "normal";
        }else if(label.percentage < 10){
          this.fontStyle = "bold";
          this.fontsize = 12;
        }else{
          this.fontStyle = "bold";
          this.fontsize = 24;
        }
        return lbl;
        //}else{
          //return  label.label 
        //}
        //label.label = label.dataset.label[label.index]
      },

      // precision for percentage, default is 0
      precision: 1,

      // identifies whether or not labels of value 0 are displayed, default is false
      showZero: false,

      // font size, default is defaultFontSize
      fontSize: 12,

      // font color, can be color array for each data or function for dynamic color, default is defaultFontColor
      fontColor: "#fff",

      // font style, default is defaultFontStyle
      fontStyle: "bold", //normal

      // font family, default is defaultFontFamily
      fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

      // draw text shadows under labels, default is false
      textShadow: false,

      // text shadow intensity, default is 6
      shadowBlur: 10,

      // text shadow X offset, default is 3
      shadowOffsetX: -5,

      // text shadow Y offset, default is 3
      shadowOffsetY: 5,

      // text shadow color, default is 'rgba(0,0,0,0.3)'
      shadowColor: "rgba(0,0,0,0.3)",

      // draw label in arc, default is false
      arc: false,

      // position to draw label, available value is 'default', 'border' and 'outside'
      // default is 'default'
      position: "border",

      // draw label even it's overlap, default is false
      overlap: true,

      // show the real calculated percentages from the values and don't apply the additional logic to fit the percentages to 100 in total, default is false
      showActualPercentages: true,

      // set images when `render` is 'image'
      images: [
        {
          src: "image.png",
          width: 16,
          height: 16
        }
      ],

      // add padding when position is `outside`
      // default is 2
      outsidePadding: 4,

      // add margin of text when position is `outside` or `border`
      // default is 2
      textMargin: 0
  }
};
var chartDateFormat = "MMM-DD";  //'M/DD-H:mm'

var holdingsHistoryChartOptions = {
    responsive: true,
    animation:{
      duration: 1000
    },
    elements:{
      line: {
        tension: .15
      }
    },
    maintainAspectRatio: false,
    title: {
      display: true ,
      fontsize: 20,
      text: "Account Value Over Time"
    },
    legend: {
      display: false,
      //position: "top"
    },
    tooltips: {
      mode: "index",
      intersect: false,
      callbacks: {
        label: function(tooltipItems,data){
          if(tooltipItems.datasetIndex == 0){
            return " $" + tooltipItems.yLabel ;
          }else{
            return " " + tooltipItems.yLabel ;
          }
        }
      }
    },
    hover: {
      mode: "nearest",
      intersect: true
    },
    scales: {
      yAxes: [{
        id: "usd",
        type: "linear",
        position: "left",
        scaleLabel: {
          display: true,
          labelString: "USD"
        },
        ticks: {
          fontColor: "rgba(0,0,0,0.5)",
          fontStyle: "bold",
          beginAtZero: false,
          // maxTicksLimit: 20,
          //suggestedMin: min,
          padding: 20
        },
        gridLines: {
          display: false,
          zeroLineColor: "transparent"
        }
      },
      {
        id: "btc",
        type: "linear",
        position: "right",
        scaleLabel: {
          display: true,
          labelString: "BTC"
        },
        ticks: {
          fontColor: "rgba(0,0,0,0.5)",
          fontStyle: "bold",
          beginAtZero: false
        },
        gridLines: {
          display: false,
          zeroLineColor: "transparent"
        }

      }],
      xAxes: [{
        type: "time",
        time: {
          displayFormats: {
            "millisecond":chartDateFormat,
            "second": chartDateFormat,
            "minute": chartDateFormat,
            "hour": chartDateFormat,
            "day":chartDateFormat,
            "week": chartDateFormat,
            "month":chartDateFormat,
            "quarter": chartDateFormat,
            "year": chartDateFormat,
          }
        },
        gridLines: {
          display: false,
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 20,
          fontColor: "rgba(0,0,0,0.5)",
          fontStyle: "bold"
        }
      }]
    }
  
};

function prepOverviewChart(){
  if(window.holdingsPieChart){
    window.holdingsPieChart.destroy();
    window.holdingsPieChart = undefined;
    $("#holdings").remove();
  }
  $("#overviewChartWrap").html("<canvas id=\"holdings\" width=\"100%\" height=\"100%\"></canvas>");
}


function makeOverviewChart() {
  // if(devmode){return}
  
  if(!app.getExchangesEnabled()){return;}

  try {
   
    let tmp = overviewPieChartData();
    let data = tmp.data;
    let labels = tmp.labels;
    let data2 = tmp.data2;
    let labels2 = tmp.labels2;
    let el = "holdings";

    if( !window.overviewPieChart || data.length != window.overviewPieChart.data.datasets[0].data.length){
      pieChartOptions.animation.duration = 1000;
    }else{
      pieChartOptions.animation.duration = 0;
    }

    prepOverviewChart();

    console.log("Make Pie Chart");

    //pieChartOptions.title.text = "Holdings - BTC Value: " + app.calcBTC.total

    let dataSets = {
      labels: labels,
      datasets: [{
        data: data,
        backgroundColor: getColorArray(),
        borderColor: [
          hexToRgb("#ffffff"),
        ],
        borderWidth: 1
      }]
    };


    let ctx = document.getElementById(el).getContext("2d");
    let myChart = new Chart(ctx, {
      type: "pie",//'doughnut',
      data: dataSets,
      options: pieChartOptions
    });
    window.holdingsPieChart = myChart;

    // $("#" + el).click(
    //   function (evt) {
    //     var activePoints = myChart.getElementsAtEvent(evt);
    //     if (!activePoints[0]) { return }
    //     var index = activePoints[0]._index
    //     var coin = myChart.data.labels[index]
    //     var value = myChart.data.datasets[0].data[index]

    //     showMessage("test", "Coin:" + coin + "  value:" + value);
    //   }
    // );

    window.overviewPieChart = myChart;

    window.addEventListener("resize", function () {
      try {
        window.overviewPieChart.resize();
        window.holdingsHistoryChart.resize();
      }catch(e){  }
    });

  }catch(e){ }

}
window["makeOverviewChart"] = makeOverviewChart;  //closure-compiler export

function prepHoldingsChart(){
  if(window.holdingsHistoryChart){
    window.holdingsHistoryChart.destroy();
    //window.holdingsHistoryChart = undefined
    $("#account_history").remove();
  }
  $("#accounthistorywrap").html("<canvas id=\"account_history\" width=\"100%\" height=\"100%\"></canvas>");
}
function makeHoldingsHistoryChart(){
// if(devmode){return}
if(!app.getExchangesEnabled()){return;}
  prepHoldingsChart();

  let tmp = holdingsHistoryChartData(true);
  let USDdata = tmp.map(e => e.data);
  let min = Math.min.apply( Math, USDdata ) - .00001;
  let labels = tmp.map(e => e.label);

  tmp = holdingsHistoryChartData(false);
  let BTCdata = tmp.map(e => e.data);
  min = Math.min.apply( Math, USDdata ) - 5;

  let el = "account_history";

  let datasets = [
    {
      label: "USD",
      yAxisID: "usd",
      pointRadius: 0,
      // lineTension: .2,
      data: USDdata,
      backgroundColor: hexToRgb("#1565C0",.2),
      borderColor: hexToRgb("#1565C0",1),
      borderWidth: 2
    },
    {
        label: "BTC",
        yAxisID: "btc",
        pointRadius: 0,
        data: BTCdata,
        backgroundColor: hexToRgb("#FFA000",.2),
        borderColor: hexToRgb("#FFA000",1),
        borderWidth: 2
    }
  ];
  
  if( !window.holdingsHistoryChart){
    holdingsHistoryChartOptions.animation.duration = 1000;
  }else{
    holdingsHistoryChartOptions.animation.duration = 0;
  }
  
  
  var ctx = document.getElementById(el).getContext("2d");
  var lineChart = new Chart(ctx,{
    type: "line",
    height: 400,
    width: "100%",
    data: {
      labels: labels,
      datasets: datasets
    },
    options: holdingsHistoryChartOptions,
    

  });
  window.holdingsHistoryChart = lineChart;

}
window["makeHoldingsHistoryChart"] = makeHoldingsHistoryChart;  //closure-compiler export

/**Chart Helper Functions */
function holdingsHistoryChartData(USD){
  let result = []; //{
      //{label: "", data: 0 }
  //}

  let data =  histories.accountBalanceHistory.data;
  if(data.length <2){
    data.push(
      {
        date: new Date().getTime() - 1000,
        totalBtc: app.totalBTC(),
        BTCprice: app.data._BTCPrice,  // set current BTC price
        holdings: []
      }
    );
    setAccountHistory(true);
  }

  for(hi of histories.accountBalanceHistory.data){
      if(hi.BTCprice && hi.BTCprice > 0){
        result.push(
            {
                label:  new Date(hi.date).toLocaleString(),//convert | stringify hi.date
                data: USD?Number(parseFloat(hi.totalBtc) * parseFloat(hi.BTCprice)).toFixed(2):parseFloat(hi.totalBtc)
            }
        );
      }
  }
  //add a current live item
  result.push(
    {
        label:  new Date().toLocaleString(),//convert | stringify hi.date
        data: USD?parseFloat(app.totalBTC()) * parseFloat(app.data._BTCPrice):parseFloat( app.totalBTC() )
    }
);
  result = result.filter(e => e.data != 0);
  
  return result;
}
window["holdingsHistoryChartData"] = holdingsHistoryChartData;  //closure-compiler export

//store stacks highest stack per ex
var hStack = 1;
function overviewPieChartData(exch, coinname, pixels, margin, indx) {
  //optional params return a pixel value calculation for height and optionally margin
  btc = 0;
  let result = {
      //{ label:"", data: 0 }
  };

  try {

      if(exch){
        exch.trading.holdings.forEach(function (coin) {
          let amount = parseFloat(coin.btcValue);
          btc += amount || 0;
          if(result[coin.name]){
              result[coin.name].data += btc;
          }else{
              result[coin.name] = {
                  label: coin.name,
                  data: btc
              };
          }
          btc=0;
          });
          
      }else{
        for(ex of app.data.settings.exchanges){
            ex.trading.holdings.forEach(function (coin) {
                    let amount = parseFloat(coin.btcValue);
                    btc += amount || 0;
                if(result[coin.name]){
                    result[coin.name].data += btc;
                }else{
                    result[coin.name] = {
                        label: coin.name,
                        data: btc
                    };
                }
                btc=0;
            });
        }
      }
      
      //sort and push into final array
      let result2 = Object.keys(result).sort(function(a,b){return result[b].data - result[a].data;});
      let final = {
          labels: [],
          data: []
      };
      for (coin in result2){
          final.labels.push(result[result2[coin]].label);
          final.data.push(result[result2[coin]].data );
      }

      // map result data into percentage data
      let sum = final.data.reduce((prev,curr) => prev + curr);
      let x = final.data.map(  k => (k/sum * 100).toFixed(1)   );
      final.data = x;
      if(coinname){  //return a % index, from 0 to 1
        let index = final.labels.findIndex(e=> e == coinname); 
        let val = Number(final.data[index]) / 100;
        if(pixels){
          let h = val * pixels;
          if(indx == 0){
            hstack = 100/h;
          }
          h = Math.max(1,h * hstack);
          if(margin){
            return (margin - h) + "px";
          }else{
            return h + "px";
          }
        }else{
          return val;
        }
      }else{
        return final; 
      }

  }catch(e){ console.log(e); }
}
window["overviewPieChartData"] = overviewPieChartData;  //closure-compiler export

function getColorArray(){
  let arr = [];
  arr.push(hexToRgb("#018fc1"));
  arr.push(hexToRgb("#d83512"));
  arr.push(hexToRgb("#ff8d01"));
  arr.push(hexToRgb("#128d10"));
  arr.push(hexToRgb("#8c028d"));
  arr.push(hexToRgb("#2c5bc5"));
  arr.push(hexToRgb("#db3b69"));
  arr.push(hexToRgb("#5da100"));
  arr.push(hexToRgb("#af2729"));
  arr.push(hexToRgb("#2e568a"));
  arr.push(hexToRgb("#8c3d8e"));
  arr.push(hexToRgb("#1fa08e"));
  arr.push(hexToRgb("#a29d1f"));
  arr.push(hexToRgb("#5d2fcf"));
  arr.push(hexToRgb("#e76205"));
  arr.push(hexToRgb("#840405"));
  arr.push(hexToRgb("#56125b"));
  arr.push(hexToRgb("#28885a"));
  arr.push(hexToRgb("#4b669d"));
  arr.push(hexToRgb("#466695"));
 
  return arr;
}
// function getColorArray(){
//   let color1 = hexToRgb("#42A5F5") //"rgb(21,101,192)"
//   //let color2 = hexToRgb("#E65100") // "rgb(230,81,0)"
//   let color3 = hexToRgb("#FFD54F") // "rgb(255,224,130)"
//   let finalArray
//   finalArray = interpolateColors(color1,color3,12)
//   //finalArray = finalArray.concat(interpolateColors(color2,color3,6))

//   return finalArray
// }

function interpolateColors(color1, color2, steps) {
  color1 = color1 || "rgb(21,101,192)";
  color2 = color2 || "rgb(230,81,0)";
  steps = steps || 12;
  var stepFactor = 1 / (steps - 1),
      interpolatedColorArray = [];

  color1 = color1.match(/\d+/g).map(Number);
  color2 = color2.match(/\d+/g).map(Number);

  for(var i = 0; i < steps; i++) {
      interpolatedColorArray.push(interpolateColor(color1, color2, stepFactor * i));
  }

  function interpolateColor(color1, color2, factor) {
    if (arguments.length < 3) { 
        factor = 0.5; 
    }
    var result = color1.slice();
    for (var i = 0; i < 3; i++) {
        result[i] = Math.round(result[i] + factor * (color2[i] - color1[i]));
    }
    //return result;
    return "rgba(" + result[0] + "," + result[1] + "," + result[2] + ",1)";
  };

  return interpolatedColorArray;
}

function hexToRgb(hex, t) {
  t = t || .75;
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? 
    "rgba(" + 
      parseInt(result[1], 16) + "," +
      parseInt(result[2], 16) + "," +
      parseInt(result[3], 16) + "," + t + ")"
   : null;
}






// function generateTints(){
//   return new window.colorValues('#E65100').tints(8).map( function(e) { 
//     return 'rgba(' + e.rgb.r + ',' + e.rgb.g + ',' + e.rgb.b + ')'
//   })
// }



// [
        //     'rgba(255, 99, 132, 1)',
        //     'rgba(54, 162, 235, 1)',
        //     'rgba(255, 206, 86, 1)',
        //     'rgba(75, 192, 192, 1)',
        //     'rgba(153, 102, 255, 1)',
        //     'rgba(255, 159, 64, 1)',
        //     'rgba(255, 99, 132, 1)',
        //     'rgba(54, 162, 235, 1)',
        //     'rgba(255, 206, 86, 1)',
        //     'rgba(75, 192, 192, 1)',
        //     'rgba(153, 102, 255, 1)',
        //     'rgba(255, 159, 64, 1)',
        // ],