
//access external API which doesn't have CORS issue
async function getApi(URL,method, timeout,dataToSend) {

  method= method || "get";
  timeout = timeout || 15000;

  try {
    let result = await $.ajax({
        url: URL,
        method: method,
        dataType: "text",
        timeout: timeout,
        data: dataToSend
    });

    return JSON.parse(result);
  }catch(e){ 
    return e.statusText;
  }
}
window.getApi = getApi;

//access outside API through our server, to prevent CORS error
function getExternalAPI(URI, callback){
  var params = {
    feed: URI,
    type: "raw"
  };
  getAPIDataAsync(exchangeEnum.polo,"api",params,true,false,function(err,data){
    if(!err){
      callback(data);
    }else{
      callback();
    }
  });

}
window.getExternalAPI = getExternalAPI;

function getBinanceInfo(){
  try {
    if(histories.exchanges[0].exchangeInfo.serverTime){return;}
  }catch(e){  }
  getExternalAPI("https://www.binance.com/api/v1/exchangeInfo", 
  function(data){
    histories.exchanges[0].exchangeInfo = JSON.parse(data);
    // let hist = data.map(function(e){
    //   let f = {}
    //   f.close = e.close.toFixed(2)
    //   f.time = moment.unix(e.time).format()
    //   return f
    // })
    // app.data.btcPriceHistory = hist.reverse()

  });
}
window.getBinanceInfo = getBinanceInfo;

function getBTCPriceHistory(){
    
//TODO decide if we need this
  //TODO - get current history last timestamp
  //       and only get the needed new data
    getExternalAPI("https://min-api.cryptocompare.com/data/histohour?aggregate=1&e=CCCAGG&extraParams=CryptoCompare&fsym=BTC&limit=8760&tryConversion=false&tsym=USD", 
        function(data){
          data = JSON.parse(data).Data;
          let hist = data.map(function(e){
            let f = {};
            f.close = e.close.toFixed(2);
            f.time = moment.unix(e.time).format();
            return f;
          });
          app.data.btcPriceHistory = hist.reverse();

        });

    

}
function refreshCMC(){
  getCoinMarketCapTicker(true);
  setTimeout(() => {
    $("#refreshCMC").prop("disabled",false);
  }, 60000 * 30);
}
window.refreshCMC = refreshCMC;

var getcmcRunning = false;
async function getCoinMarketCapTicker(clicked){
  let start = 1;
  let num = 800;
  let done = false;
  let ticker = [];
  let metadata; 
  let foundError = false;
  let chunkSize = 100;  
  let curChunk;
  let result;
  let interval;
  let sleepTime = 5000;
  let errorCount=0;

  if(getcmcRunning){return;}
  getcmcRunning = true;

  $("#refreshcmc").removeClass("hide");
  $("#refreshCMC").prop("disabled",true);

  do {
    //grab 100 records
    let curChunk = Math.min(parseInt(start + chunkSize-1), num);
    console.log("Getting CMC data:" + start + "-" + curChunk + "  of:" + num);
    if(errorCount >= 3){
      break;
    }

    let result = await getTickerData(start,chunkSize);
    if(result == "timeout"){
      //do something if we got a timeout
      result = await getTickerData(start,chunkSize); // give it another try
      if(result == "timeout"){  //if it times out again, set to re-try in 60, and bail out
        setTimeout(() => {
          getCoinMarketCapTicker();
        }, 60000);
        getcmcRunning = false;
        return;
      }
    }
    foundError = true;

    try {
      foundError = result.metadata.error;
      // num = result.metadata.num_cryptocurrencies
      
      metadata = result.metadata;
      let tmpArray = objectToArray(result.data);

      //map the object into array of one object
      tmpArray = tmpArray.map(function(e){
        return e[1];
      } );

      //append array to ticker array
      ticker = ticker.concat( tmpArray );
      cmcData.data = ticker;
    }catch(e){ 
      //got an error, redo the loop
      errorCount +=1;
      continue;
    }

    setGlChartSize();

    if(start > 200){ sleepTime = 3000;}
    if(start > 800){ sleepTime = 2000;}

    //slow things down on first load
    if(!clicked){
      await sleep(sleepTime);
    }

    start += chunkSize; 
    //check if we are done
    if(start >= num){done = true;}
  } while (!done && !foundError);

  $("#refreshcmc").addClass("hide");
  if(!clicked){
    $("#refreshCMC").prop("disabled",false);
  }
  
  if(!foundError){
    ticker = _.sortBy(ticker,"name");
    cmcData.metadata = metadata;
    cmcData.data = ticker;
  }
  
  getcmcRunning = false;
  
  async function getTickerData(start, limit){
    return await getApi("https://api.coinmarketcap.com/v2/ticker/?sort=rankh&convert=BTC&start=" + start + "&limit=" + limit);
  }
}
window.getCoinMarketCapTicker = getCoinMarketCapTicker;

//set the gainers/losers diagram
function setGlChartSize(){
  let g = app.getGainers(1000).length;
  let l = app.getLosers(1000).length;
  let mttl = g + l;
  let ttl = 700; //g + l
  let area = 200*200;
  g = Math.sqrt(area * g/ttl);
  l = Math.sqrt(area * l/ttl);

  if(devmode == true && mttl > 100){return;}

  let go = $(".status_gainers");
  let lo = $(".status_losers");

  //set margins of labels if element is small
  if(g < 55){
    go.after($(".sg_label"));
    $(".sg_label").css("color","var(--gray)").css("left","-85px").css("top","10px");
  }else{
    go.append($(".sg_label"));
    $(".sg_label").css("color","white").css("left","0px").css("top","0px");
  }
  if(l < 55){
    lo.after($(".sl_label"));
    $(".sl_label").css("color","var(--gray)");
  }else{
    lo.append($(".sl_label"));
    $(".sl_label").css("color","white");
  }

  $(".status_gainers").stop();
  $(".status_losers").stop();

  //set line heights to same as div height to vertically center
  if(g>54){
    $(".sg_label").animate({"line-height":(g)+"px"},1000);
  }else{
    $(".sg_label").css("line-height", 0);
  }
  if(l>54){
    $(".sl_label").animate({"line-height":(l)+"px"},1000);
  }else{
    $(".sl_label").css("line-height", 0);
  }

  g= Math.max(2,g);
  l= Math.max(2,l);

  //set gainers margin-right size relative to divs size
  let m = (mttl/700) * 30;
  $(".status_gainers").css("margin-right", "-" + m + "px");
  
  
  $(".status_gainers").animate({width: g, height:g},1000);

  $(".status_losers").animate({width: l, height:l},1000);

  $(".status_gainers").css("z-index",Math.round(g));
  $(".status_losers").css("z-index",Math.round(l));
}