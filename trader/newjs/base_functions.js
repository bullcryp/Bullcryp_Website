// define a new console
// var console = (function(oldCons){
//   return {
//       log: function(text){
//           oldCons.log(text);
//           // Your code
//       },
//       info: function (text) {
//           oldCons.info(text);
//           // Your code
//       },
//       warn: function (text) {
//           oldCons.warn(text);
//           // Your code
//       },
//       error: function (text) {
//           oldCons.error(text);
//           // Your code
//       }
//   };
//}(window.console));
var disableMessages = true;
function messagelog(message){
  if(disableMessages){return;}
  console.log(message);
}

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}
function handleVisibilityChange(){
  if(document.hidden){
    app.data.currentState.visibility.windowHidden = true;
  }else{
    app.data.currentState.visibility.windowHidden = false;
  }
}
document.addEventListener(visibilityChange, handleVisibilityChange, false);

