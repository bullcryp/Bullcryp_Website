

var  app;


function makeVueModel(){

    //make any required HTML elements that are dynamically created
    //before initializing the vue models
    makeTables();

    
    //generic movers structure
    let movers = {
        filterPair: "",
        pairMovers: [["Loading..."]]
    };
    //generic holdings and orders structure
    let trading= {
        holdings: [],
        orders: [],
    };
    //generic exchange settings structure
    let exchangeSettings= {
        enabled: false,
        key: "",
        secret: ""
    };

    //app settings structure
    let settings = {
        initDone: false,
        gotAccountData: false,
        account: {
            user: {
                firstname: "",
                lastname: "",
                photo: "",
                email: ""
            },
            subscription:{
                brand: "",
                created: 23456789,
                id: "",
                receiptNumber: "",
                subscriptionLevel: {},
                features: {},
            },
        },
        accountBalanceHistory: {
            lastSave: 99999,
            data:[
            // {
            //     date: 99999,
            //     totalBtc: 0,
            //     BTCprice: 0,
            //     holdings: [
            //         {
            //             coin: "BTC",
            //             amount: 0
            //         },
            //         {
            //             coin: "ETH",
            //             amount: 0
            //         }
            //     ]
            // }
        ]},
        generalSettings: {

        },
        dashboard: {
            hideSmallBalances: {
                check: true,
                label:"Hide Small Balances", 
                value: false, 
                hideInSettings: true
            },
            toggleview: {
                check: true,
                label:"Toggle View", 
                value: false, 
                hideInSettings: true
            },
            hideSmallBalancesAmount: {text: true,
                width: 30,
                label:"Hide Small Balances Amount (USD)",
                value: 2,
                tooltip: "Holdings below this value will be hidden if 'Hide small balances' is enabled on that screen"
            },
            mystuffGainers: false,
        },
        feedback: {
            exchanges: [
                "Huobi",
                "Bittrex",
                "Bithumb",
                "Bitfinex",
                "GDAX",
                "Kraken",
                "HitBTC"
            ]
        },

        //TODO move large properties such as 
        //tradeHistory, cmcData, 
        //off the VueModel
        //perhaps just use local storage and use temp variables.
        exchanges: [
                {
                id: 1,
                name: "Binance",
                shortname: "bin",
                pubURL: "https://api.binance.com",
                privURLPart: "binance/",
                settings: makeNewObj(exchangeSettings),
                movers: makeNewObj(movers),
                trading: makeNewObj(trading),
                markets:["BTC","ETH","BNB","USDT"],
                pairs: [],
                ticker: {},
                BTCPrice: 0,
                bantime: 0
            },
            {
                id: 2,
                name: "Poloniex",
                shortname: "polo",
                pubURL: "https://poloniex.com/public?command=",
                privURLPart: "polo/",
                settings: makeNewObj(exchangeSettings),
                movers: makeNewObj(movers),
                trading: makeNewObj(trading),
                markets:["BTC","ETH","XMR","USDT"],
                pairs: [],
                ticker: {},
                BTCPrice: 0,
                bantime: 0
            },
        ],
        tabSettings: {
            exchange: [
                {
                    name: "Binance",
                    id: 1,
                    isActive: true
                },
                {
                    name: "Poloniex",
                    id: 2,
                    isActive: false
                }
            ],
            chartTabs: [
                [

                ],
                [
                "BINANCE:BTCUSDT",
                "BINANCE:BTCUSDT|1y"
                ],
                [
                "POLONIEX:BTCUSDT",
                "POLONIEX:BTCUSDT|1y"
                ]
            ],
        },
        system: {
            orderbookLength: 5,
            dataPairHistoryLength: 10,
            ajaxTimeout: 25000,
            initComplete: false,
            sellFactor: .85,                //percent range between high bid and low ask
            buyNowFactor: 1.25,
            dumpFactor: .75,
            holdingsCount: null,
            ordersCount: null
        },
        crypto:{
            publickey:null,
        },
        oauthProfile: {},    
    };

    let curState = {
        exchangeStatus: {
            binance: {
                APIActive: false,
                lastPing: 0,
                gotInitialHoldings: false,
                enabled: false  //has keys
            },
            poloniex: {
                APIActive: false,
                lastPing: 0,
                gotInitialHoldings: false,
                enabled: false //has keys
            }
        },
        userMarketToggleUSD: 0,
        userMarketPreferredToggleUSD: 0,
        advancedChartToggle: 0,
        clicked: {
            coin: "BTC",
            market: "BTC",
            pair: {ChartSymbol:"BTCUSDT",coin:"BTC",market:"BTC", pair: "BTCUSDT"}, //"USDBTC",
            exchange: "Binance"
        },
        visibility:{
            windowHidden: false,
            navTab: "Overview",
            homwtab: "holdingspane",
            orderbookVisible: true, //TODO
            marketMoversVisible: true, //TODO
            activeExchangeTab: {
                name: "Binance",
                id: 1,
                isActive: true
            },
            newsCurrentPage: 0,
            portBalanceBTC: false
        },
        newstab: 0,
        newsFeedsToShow: 3,
        newsArticlesToShow: 20,
        
        tradeHistoryExists: true,
        tradeHistoryUpdateInProgress: false,
    };

    let appModel = {
        data: {
            settings: settings,
            btcPriceHistory: [],
            _BTCPrice: 0,  // swt btc price from binance trade stream
            currentState: curState,
            umt: 0
        }
    };

    window["app"] = app = new Vue({
        el: "#app",
        data: appModel,
        watch: {
            watchExchangeSettings: {
                handler(after, before){
                    this.debouncedsaveSettings();
                },
                deep: true
            },
            watchHoldingsOrders: {
                handler(after,before){
                    this.debouncedholdingsOrdersUpdate();
                },
                deep:true
            },
            "data.currentState.userMarketToggleUSD": function(nv,ov){
                //console.log("UMT value is " + this.data.currentState.userMarketToggleUSD)

                let USD = this.data.currentState.userMarketToggleUSD;
                let market;
                
                market = USD?"USDT":"BTC";
                
                if(market == "BTC" && app.data.currentState.clicked.coin == "BTC"){
                    market = "USDT";
                }else if(market == "USDT" && app.data.currentState.clicked.coin.includes("USD")){
                    market = "BTC";
                }
                //set current clicked coin market
                app.data.currentState.clicked.market = market;

                setBuyPair();

                setTimeout(() => {
                setTVChart();
                }, 250);
            }
        },
        created: function () {
            //create debouced methods for potentially expensive methods
            //debounce methods will only call on the last call
            this.debouncedholdingsOrdersUpdate = _.debounce(this.holdingsOrdersUpdate, 1000);
            this.debouncedsaveSettings = _.debounce(this.saveSettings,1000);

            this.throttlePieChart = _.throttle( makeOverviewChart, 3000,{"leading": false,"trailing": true} );
            this.throttleHoldingChart = _.throttle( makeHoldingsHistoryChart, 3500,{"leading": false,"trailing": true} );

        },
        methods: {
            BTCPrice: vm.BTCPrice,

            init: vm.init,

            saveRestart: vm.saveRestart,

            setExchangeTab: vm.setExchangeTab,

            homwTab: vm.homwTab,

            newsTab: vm.newsTab,

            saveSettings: vm.saveSettings,

            loadSettings: vm.loadSettings,

            loadAccountHistory: vm.loadAccountHistory,

            holdingsOrdersUpdate: vm.holdingsOrdersUpdate,

            setMovers: vm.setMovers,

            getExchange: vm.getExchange,

            getExchangesEnabled: vm.getExchangesEnabled,

            getExchangeEnabled: vm.getExchangeEnabled,

            getExchangeByName: vm.getExchangeByName,

            getExchangeByIndex: vm.getExchangeByIndex,

            getExchangeAPIStatus: vm.getExchangeAPIStatus,

            getExchangeIndex: vm.getExchangeIndex,

            getExchangeSettings: vm.getExchangeSettings,

            getExchangeVisibility: vm.getExchangeVisibility,

            exchangeSettingsUpdate: vm.exchangeSettingsUpdate,

            verifyExchangeKeys: ()=>{},

            filteredMovers: vm.filteredMovers,

            getGainers: vm.getGainers,

            getLosers: vm.getLosers,

            getHoldings: vm.getHoldings,

            isInHoldings: vm.isInHoldings,

            getTopHoldings: vm.getTopHoldings,

            getHolding: vm.getHolding,

            getOrders: vm.getOrders,

            getPairs: vm.getPairs,

            getCoinImage: vm.getCoinImage,

            getCoinInfo: vm.getCoinInfo,

            getCMCCoinInfo: vm.getCMCCoinInfo,

            doUserMarketClick: vm.doUserMarketClick,

            toggleChartType: vm.toggleChartType,

            //doUserMarketToggle: vm.doUserMarketToggle,

            makePairObject: vm.makePairObject,

            doCoinClick: vm.doCoinClick,

            doPairClick: vm.doPairClick,

            setNewBuyPrice: vm.setNewBuyPrice,

            // getPairCoin: vm.getPairCoin, TODO

            getStandardPairName: vm.getStandardPairName,

            getPairMarketName: vm.getPairMarketName,

            coinPriceBTC: vm.coinPriceBTC,

            getCoinAvailableAmount: vm.getCoinAvailableAmount,

            totalBTC: vm.totalBTC,

            totalUSD: vm.totalUSD,

            overviewPieChartData: overviewPieChartData,

            checkVisible: vm.checkVisible,

            getLocalDate: vm.getLocalDate,

            getUserInput: getUserInput,

            fixFloat: vm.fixFloat,

            scientificToDecimal: scientificToDecimal,

            fixBinVal: vm.fixBinVal,

            filterArticle: vm.filterArticle,

            vbuyCoin: vm.buyCoin,

            vdumpCoin: vm.vdumpCoin,

            vsell: vm.vsell,

            vsellAt: vm.vsellAt,

            vmoveOrder: vm.vmoveOrder,

            vcancelOrder: vm.vcancelOrder
            
        },
        computed: {
            watchExchangeSettings: function(){
                let tmp = [];
                for(ex of this.data.settings.exchanges){
                    tmp.push(ex.settings);
                }
                return tmp;
            },
            watchHoldingsOrders: function(){
                let tmp = [];
                for(ex of this.data.settings.exchanges){
                    tmp.push(ex.trading.holdings);
                    tmp.push(ex.trading.orders);
                }
                return tmp;
            },
            marketToggleState:function(){
                let st = (this.data.currentState.clicked.coin == "USDT" || this.data.currentState.clicked.coin == "BTC");
                if(st) {this.data.currentState.userMarketToggleUSD = 1;}
                return st;
            },
            advancedChartToggleState: function(){

            },
            calcBTC: function () {
                if (!this.data) { return; }
                let BTCPrice = this.data._BTCPrice;
                let result = {};
                let totalBTC = 0;
                for(ex of this.data.settings.exchanges){
                    let exBTC = 0;
                    ex.trading.holdings.forEach(function (coin) {
                        if (coin.name == "USDT") {
                            totalBTC += parseFloat(coin.available) / parseFloat(BTCPrice);
                            exBTC += parseFloat(coin.available) / parseFloat(BTCPrice);
                        } else {
                            totalBTC += parseFloat(coin.btcValue);
                            exBTC += parseFloat(coin.btcValue);
                        }
                    });
                    result[ex.name] = exBTC.toFixed(8);
                }
                result.total = totalBTC.toFixed(8);
                
                return result;
                
            }
            
        },
    });
    // Vue.directive("select", {
    //     twoway: true,
    //     bind: function(el,binding,vnode) {
    //         $(el).Select2().on("select2:select", (e) => {
    //             //const event = new Event("change", { bubbles: true, cancelable: true });
    //             el.dispatchEvent(new Event("change", { target: e.target }));
    //         });
    //     }
    // });
    
}
window["makeVueModel"] = makeVueModel;  //closure-compiler export


// Vue Model Helpers
function makeTables(){
    makeSettingsPage();
    makeMoversTables();
    makeHoldingsTable();
    makeOrdersTable();
    makeGainersTables();
    makeBuySection();
    makeFeedbackPage();
}



function makeBuySection(){
    let buy = 
    `
    <div><h3 class="sectiontitle">Trade</h3></div>
    <!-- Exchange Tabs -->
            <div id="exchange_tabs" class="tabsholder">
                <div style="float:left;margin: 10px 20px 0 10px;">Select Exchange</div>
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item waves-effect exchangetab" v-for="tab in data.settings.tabSettings.exchange">
                        <a :class="{'nav-link':true, 'active':tab.isActive}" data-toggle="tab" role="tab" @click.prevent="setExchangeTab(tab); setNewBuyPrice()">{{tab.name}}</a>
                    </li>
                    <!-- https://jsfiddle.net/gmsa/gfg30Lgv/  https://jsfiddle.net/0cr3xbdv/ bootstrap tabs in Vue-->
                </ul>
            </div>
    
            <div :id="'buy' + ex.id" class="buygrid" v-for="ex in data.settings.exchanges" v-show="data.currentState.visibility.activeExchangeTab.id == ex.id">
                <div :id="'market_selector' + ex.id" class="market_selector">
                    <div style="float:left;margin: 0 10px;">Buy with </div>
                    <span style="font-weight:bold">BTC </span><input type="checkbox"  :id="'prefered.market'+ex.id" v-model="data.currentState.userMarketToggleUSD" @click="doUserMarketClick(ex)"> <span style="font-weight:bold"> USDT</span>  
                </div>
                <div>
                    <select :id="'buypair' + ex.id" class="buypair pairselect" v-model="data.currentState.clicked.pair.pair">
                        <option v-for="pr in ex.pairs" class="" :value="pr">{{pr}}</option>
                    </select>
                </div>
                <div>
                    <div style="float: left; margin-right: 10px; margin-top: 6px">
                        <label v-if="data.currentState.userMarketToggleUSD" for="buyAmt">USD amount to spend</label>
                        <label v-if="!data.currentState.userMarketToggleUSD" for="buyAmt">BTC amount to spend</label>
                    </div>
                    <div v-if="data.currentState.userMarketToggleUSD" class="md-form"><input class="form-control" style="width: 50px; margin-top:6px" :id="'buyAmt' + ex.id" type="text" value="200" data-toggle="tooltip" title="Input the USD amount to spend"></input></div>
                    <div v-if="!data.currentState.userMarketToggleUSD" class="md-form"><input class="form-control" style="width: 50px; margin-top:6px" :id="'buyAmt' + ex.id" type="text" value=".02" data-toggle="tooltip" title="Input the BTC amount to spend"></input></div>
                    <div style="font-size:10px; position:absolute">
                        <span v-if="data.currentState.userMarketToggleUSD">Available: {{getCoinAvailableAmount(data.currentState.clicked.exchange,"USDT").toFixed(2)}}</span>
                        <span v-if="!data.currentState.userMarketToggleUSD">Available: {{getCoinAvailableAmount(data.currentState.clicked.exchange,"BTC")}}</span>
                    </div>
                </div>

                

                <div>
                    <div style="float: left; margin-right: 10px; margin-top: 6px">
                        <label for="buyprice">Enter the price</label>
                    </div>
                    <div class="md-form"><input class="form-control" style="width: 100px; margin-top:6px" :id="'buyprice' + ex.id" type="text" value="" data-toggle="tooltip" title="Input the price for your bid"></input></div>
                </div>
                
                <div>
                    <button :id="'bbut'+ex.id" @click="vbuyCoin(ex)" class="btn btn-sm btn-warning noshadow bullbutton waves-effect waves-light" data-toggle="tooltip" title="Open a dialog box to place an order">Place Order..</button>
                    
                </div>
                
            </div>
    `;
    $("#buy").replaceWith(buy);
    $("#exchange_tabs").insertBefore("#trade_section");
}
function makeTVTicker(BTC){
    let currency = "USD";
    let element = "tvtickerusd";
    if(BTC){
        currency = "BTC";
        element = "tvtickerbtc";
    }
    let holdings = app.getTopHoldings(5);
    
    let symbols = "{\"symbols\":[", market;
    if(!holdings.some(e => e.name == "BTC") ){  //if they don't have BTC
        if(holdings.length){holdings.length = 4;}
        symbols = symbols + 
        "{\"description\":\"Bitcoin / USDT\"," +
        "\"proName\": \"BINANCE:BTCUSDT\"},";
    }
    if(!holdings.length){
        if(BTC){
            symbols = symbols + 
                `{"description":"Ripple / BTC", "proName": "BINANCE:XRPBTC"},
                {"description":"Lisk / BTC", "proName": "BINANCE:LSKBTC"},
                {"description":"0x / BTC", "proName": "BINANCE:ZRXBTC"},
                {"description":"BitShares / BTC", "proName": "BINANCE:BTSBTC"},`;
        }else{
            symbols = symbols + 
                `{"description":"Ripple / USDT", "proName": "BINANCE:XRPUSD"},
                {"description":"Lisk / USDT", "proName": "BINANCE:LSKUSD"},
                {"description":"0x / USDT", "proName": "BINANCE:ZRXUSD"},
                {"description":"BitShares / USDT", "proName": "BINANCE:BTSUSD"},`;
        }
    }else{
        for(item of holdings){
        item.longName = app.getCoinInfo(item.name).name;
        market = item.name=="BTC"?"USDT":currency;
        symbols = symbols + 
            "{\"description\":\"" + item.longName + " / " + market + "\"," +
            "\"proName\": \"BINANCE:" + item.name + market + "\"},";
        }
    }

    symbols = symbols.slice(0,symbols.length-1);
    symbols = symbols + "],\"locale\":\"en\"}";
    
    let tvt = `
    <div class="tradingview-widget-container tvtick `+element+`">
        <div class="tradingview-widget-container__widget"></div>
        <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Quotes</span></a> by TradingView</div>
        <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>`
        + symbols +
        `</script>
    </div>`;

    $("." + element).replaceWith(tvt);
    if("." + element == ".tvtickerbtc"){
        $("." + element).show();
    }
    
    //remove the mysterious clone iframe TV creates in the head section.. ??
    setTimeout(() => {
        $("head iframe").remove();
    }, 2000);
}
window.makeTVTicker = makeTVTicker;

function makeHoldingsTable(){
    let ho = 
    `
    
        <div :id="'ho' + ex.id" :class="{'hidden':!getExchangeVisibility(ex.id),'hogroup':true}" :data-index="ex.id + ''" v-for="ex in data.settings.exchanges">
            <div id="hobanner" style="float: left">{{ex.name}} - BTC Value: {{totalBTC(ex)}} </div>
            <div style="float:left;margin: 0 10px;">Hide small balances</div>
            <div style="float:left"><input type="checkbox"  id="" v-model="data.settings.dashboard.hideSmallBalances.value" @change="saveSettings('dashboard')"></div>
            <div style="float:left;margin: 0 10px 0 20px;">Stack View</div>
            <div style="float:left"><input type="checkbox"  id="" v-model="data.settings.dashboard.toggleview.value" @change="saveSettings('dashboard')"></div>
            <!--<div style="margin: 0 20px;clear: both">Stack View</div>-->
            <div style="clear:both" id="howrap" :class="{'rowview':!data.settings.dashboard.toggleview.value,'cardview':data.settings.dashboard.toggleview.value}">
                <div class="horow">
                    <div></div>
                    <div>Coin</div>
                    <div>Amount</div>
                    <div>On Order</div>
                    <div>BTC Value</div>
                    <div>USD Value</div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="horow" v-for="holding, index in getHoldings(ex)" @click="doCoinClick(ex,holding.name)" v-if="!data.settings.dashboard.hideSmallBalances.value || Number(holding.btcValue) * Number(BTCPrice()) > data.settings.dashboard.hideSmallBalancesAmount.value">
                  <template v-if="checkVisible(ex,'holdings')">
                    <template v-if="!data.settings.dashboard.toggleview.value">
                        <div class="rowsymbol"><img :src="getCoinImage(holding.name)"></div>
                        <div>{{holding.name}}</div>
                        <div>{{ fixFloat(Number(holding.available) + Number(holding.onOrders),8) }}</div>
                        <div>{{ fixFloat(holding.onOrders,8) }}</div>
                        <div>{{ fixFloat(holding.btcValue,8) }}</div>
                        <div>{{ fixFloat( Number(holding.btcValue) * Number(BTCPrice()),2) }}</div>
                        <div><button class="btn btn-sm btn-warning noshadow bullbutton" v-show="holding.name != 'USDT'" @click="vdumpCoin(holding)" v-tooltip:left="'Dump ' + holding.name + ' at the current market asking prices'">Dump</button></div>
                        <div><button class="btn btn-sm btn-warning noshadow bullbutton" v-show="holding.name != 'USDT'" @click="vsellAt(holding)" v-tooltip:left="'Set the amount of  ' + holding.name + ' to sell, and price you want to sell at'">Sell..</button></div>
                    </template>
                    <template v-if="data.settings.dashboard.toggleview.value">
                        <div class="rowsymbol">
                            <img :src="getCoinImage(holding.name)">
                            <div class="stack" :style = "{'height': overviewPieChartData(ex,holding.name, 109,null,index ), 'margin-top': overviewPieChartData(ex,holding.name, 109 , 109, index )}"></div>
                        </div>
                        <div>
                            <div>{{holding.name}}</div>
                            <div>{{ fixFloat(Number(holding.available) + Number(holding.onOrders),8) }}</div>
                            <div>{{ fixFloat(holding.onOrders,8) }}</div>
                            <div>{{ fixFloat(holding.btcValue,8) }}</div>
                            <div>{{ fixFloat( Number(holding.btcValue) * Number(BTCPrice()),2) }}</div>
                            <div><button class="btn btn-sm btn-warning noshadow bullbutton" v-show="holding.name != 'USDT'" @click="vdumpCoin(holding)" v-tooltip:left="'Dump ' + holding.name + ' at the current market asking prices'">Dump</button></div>
                            <div><button class="btn btn-sm btn-warning noshadow bullbutton" v-show="holding.name != 'USDT'" @click="vsellAt(holding)" v-tooltip:left="'Set the amount of ' + holding.name + ' to sell, and price you want to sell at'">Sell...</button></div>
                        </div>
                    </template>
                  </template>
                </div>
            </div>
        </div>
    `;
    // <div><button class="btn btn-sm btn-warning noshadow" v-show="holding.name != 'USDT'" @click="vsell(holding)">Sell Now</button></div>
    $("#ho").replaceWith(ho);
}

function makeOrdersTable(){
    let or =
    `
    <div :id="'or' + ex.id" :class="{'hidden':!getExchangeVisibility(ex.id),'orgroup':true}" :data-index="ex.id + ''" v-for="ex in data.settings.exchanges">
        <div id="orbanner">{{ex.name}}({{getOrders(ex).length}}) </div>
        <div id="orwrap" class="rowview">
            <div class="orrow">
                <div></div><div></div><div></div>
                <div class="orhead">Pair</div>
                <!--<div class="orhead">Start Amount</div>-->
                <div class="orhead">Type</div>
                <div class="orhead">Amount</div>
                <div class="orhead">Bid</div>
                <div></div><div></div>
            </div>
            <div :id="order.orderNumber" class="orrow" v-for="order, index in getOrders(ex)" @click="doPairClick(ex,order.symbol)">
                <template v-if="checkVisible(ex,'orders')">    
                    <div v-if="order.type=='sell'" class="rowsymbol"><img :src="getCoinImage( makePairObject(ex,order.symbol).coin )"></div>
                    <div v-if="order.type=='buy'" class="rowsymbol"><img :src="getCoinImage( makePairObject(ex,order.symbol).market )"></div>
                    <div><img src="imgs/arrows.png"></div>
                    <div v-if="order.type=='sell'" class="rowsymbol"><img :src="getCoinImage( makePairObject(ex,order.symbol).market )"></div>
                    <div v-if="order.type=='buy'" class="rowsymbol"><img :src="getCoinImage( makePairObject(ex,order.symbol).coin )"></div>
                    <div class="orhead">{{order.symbol}}</div>
                    <div :class="'orhead' + ' ' + order.type">{{order.type}}</div>
                    <div class="orhead">{{order.amount}}</div>
                    <div class="orhead">{{order.price}}</div>
                    <div><button v-if="ex.shortname != 'bin'" class='btn btn-sm btn-warning noshadow bullbutton' @click="vmoveOrder(order.orderNumber,order.price,order.amount,order.symbol)" :id="order.orderNumber">Change...</button></div>
                    <div><button class='btn btn-sm btn-warning noshadow bullbutton' @click="vcancelOrder(ex,order.orderNumber,order.symbol)" :id="order.orderNumber">Cancel Order</button></div>
                </template>
            </div>

        </div>
    </div>
    `;
    //subItem.orderNumber, subItem.rate,subItem.amount, item[0]
    $("#or").replaceWith(or);
}

function makeMoversTables(){
    let movers = 
    `
    <div :id="'movers' + ex.id" :class="{'hidden':!getExchangeVisibility(ex.id) ,'moversgroup':true}" :data-index="ex.id + ''" v-for="ex in data.settings.exchanges">
        <div id="moversbanner" class="md-form"><div style="float: left">{{ex.name}}({{filteredMovers(ex).length}}): Filter for pair:</div><div><input type="text" class="form-control" style="width: 100px" v-model="ex.movers.filterPair"></div></div>
        <div id="moversWrap">
            <div class="moversrow">
                <div class="moverscount moversdata movershead"></div>
                <div class="movers moversdata movershead">Pair</div>
                <div class="movers moversdata movershead">24 hrs</div>
                <div class="movers moversdata movershead">12 hrs</div>
                <div class="movers moversdata movershead">8 hrs</div>
                <div class="movers moversdata movershead">4 hrs</div>
                <div class="movers moversdata movershead">2 hrs</div>
                <div class="movers moversdata movershead">30 mins</div>
                <div class="movers moversdata movershead">15 mins</div>
                <div class="movers moversdata movershead">5 mins</div>
            </div>
            <template v-if="checkVisible(ex,'movers')"> 
                <div class="moversrow" v-for="row,index in filteredMovers(ex)" @click="doPairClick(ex,row[0])">
                    <div class="moverscount">{{index + 1}}</div>
                    <div v-for="item in row" :class="{'movers':true,'moversdata':true,'movers-green': item>=0,'movers-red':item<0}">{{item}}</div>
                </div>
            </template
        </div>
    </div>
    `;
    $("#mo").replaceWith(movers);
}
function makeGainersTables(){
    let gainers = 
    `
    <div id="gainersSwitch">
    <div style="float:left;margin: 0 10px;">Show My Coins</div>
    <input type="checkbox"  id="mystuffG" v-model="data.settings.dashboard.mystuffGainers" @change="saveSettings('dashboard')">
    </div>
    <div id="gainers" class="gainersgroup">
        <div id="gainersWrap">
            <div class="gainersrow">
                <div class="gainerscount gainershead"></div>
                <div></div>
                <div class="gainers gainersdata gainershead">Symbol</div>
                <div class="gainers gainersdata gainershead">Coin</div>
                <div class="gainers gainersdata gainershead">Price (USD)</div>
                <div class="gainers gainersdata gainershead">Price (BTC)</div>
                <div class="gainers gainersdata gainershead">Change %</div>
            </div>
            <template v-if="checkVisible(null,'gainers')"> 
            <div class="gainersrow" @click="getCMCCoinInfo(row.name)" title="Click for info.." v-for="row,index in getGainers(1000)" v-if="!data.settings.dashboard.mystuffGainers || isInHoldings(row.symbol)">
                <div class="gainerscount">{{index + 1}}</div>
                <div class="gainers gainersdata rowsymbol"><img :src="'https://s2.coinmarketcap.com/static/img/coins/16x16/' + row.id + '.png'" ></div> 
                <div class="gainers gainersdata">{{row.symbol}}</div>
                <div class="gainers gainersdata">{{row.name}}</div>
                <div class="gainers gainersdata">{{scientificToDecimal(row.quotes.USD.price)}}</div>
                <div class="gainers gainersdata">{{scientificToDecimal(row.quotes.BTC.price)}}</div>
                <div class="gainers gainersdata" :class="{'gainers':true,'gainersdata':true,'movers-green': row.quotes.USD.percent_change_24h>=0,'movers-red':row.quotes.USD.percent_change_24h<0}">+{{row.quotes.USD.percent_change_24h}}</div>
            </div>
            </template>
        </div>
    </div>
    `;
    //<div class="gainers gainersdata rowsymbol"><img :src="'imgs/coins32/' + row.symbol + '.png'" ></div> 
    $("#gainers").replaceWith(gainers);

    let losers = 
    `
    <div id="losers" class="gainersgroup">
        <div id="gainersWrap">
            <div class="gainersrow">
                <div class="gainerscount gainersdata gainershead"></div>
                <div></div>
                <div class="gainers gainersdata gainershead">Symbol</div>
                <div class="gainers gainersdata gainershead">Coin</div>
                <div class="gainers gainersdata gainershead">Price (USD)</div>
                <div class="gainers gainersdata gainershead">Price (BTC)</div>
                <div class="gainers gainersdata gainershead">Change %</div>
            </div>
            <template v-if="checkVisible(null,'gainers')"> 
            <div class="gainersrow" @click="getCMCCoinInfo(row.name)" title="Click for info.." v-for="row,index in getLosers(1000)" v-if="!data.settings.dashboard.mystuffGainers || isInHoldings(row.symbol)">
                <div class="gainerscount">{{index + 1}}</div>
                <div class="gainers gainersdata rowsymbol"><img :src="'https://s2.coinmarketcap.com/static/img/coins/16x16/' + row.id + '.png'"></div>
                <div class="gainers gainersdata">{{row.symbol}}</div>
                <div class="gainers gainersdata">{{row.name}}</div>
                <div class="gainers gainersdata">{{scientificToDecimal(row.quotes.USD.price)}}</div>
                <div class="gainers gainersdata">{{scientificToDecimal(row.quotes.BTC.price)}}</div>
                <div class="gainers gainersdata" :class="{'gainers':true,'gainersdata':true,'movers-green': row.quotes.USD.percent_change_24h>=0,'movers-red':row.quotes.USD.percent_change_24h<0}"> {{row.quotes.USD.percent_change_24h}}</div>
            </div>
            </template>
        </div>
    </div>
    `;
    $("#losers").replaceWith(losers);
}

function makeSettingsPage(){
    let msg = "Your API keys are never stored on our server. <br><br>Obtain your API Key and Secret by logging in to your ";
    let helpButtonBin = makeHelpButton("Enter a new key and secret",msg + "Binance account<br><br><img src=imgs/bin_api.jpg>");
    let helpButtonPolo = makeHelpButton("Enter a new key and secret",msg + "Poloniex account<br><br><img src=imgs/polo_api.jpg>");
    let settingsExchanges = 
    `
    <div>
        <div><h3 class="sectiontitle">Exchange Settings</h3></div>
        <div v-for="exchange in data.settings.exchanges" :class="exchange.name + ' formgrid'">
            <div class="title"><h4>{{exchange.name}}</h4></div>
            <div><label>{{exchange.name}} Key</label></div>
            <div class="md-form">
                <div style="float:left">
                    <input type="password" class="form-control" v-model.trim="exchange.settings.key" :name="exchange.shortname + '-key'" :id="exchange.shortname + '-key'" :placeholder="exchange.name +' key'" disabled>
                </div>
                <div>
                    <button class='btn btn-sm btn-warning noshadow bullbutton' @click="getUserInput('Enter New Key',function(key){exchange.settings.key = key})">Change Key</button>
                    <template v-if="exchange.shortname=='bin'">`+helpButtonBin+"</template><template v-if=\"exchange.shortname=='polo'\">"+helpButtonPolo+`</template>
                </div>
            </div>
            <div><label>{{exchange.name}} Secret</label></div>
            <div class="md-form">
                <div style="float:left">
                    <input class="form-control" type="password" v-model.trim="exchange.settings.secret" :name="exchange.shortname + '-sec'" :id="exchange.shortname + '-sec'" :placeholder="exchange.name + ' secret'" disabled>
                </div>
                <div>
                    <button class='btn btn-sm btn-warning noshadow bullbutton' @click="getUserInput('Enter New Secret',function(sec){exchange.settings.secret = sec})">Change Secret</button>
                    <template v-if="exchange.shortname=='bin'">`+helpButtonBin+"</template><template v-if=\"exchange.shortname=='polo'\">"+helpButtonPolo+`</template>
                </div>
            </div>
        </div>
        <div>
            <button class='btn btn-warning noshadow' @click="saveRestart()">Save and Restart</button>
        </div>
    </div>
    <div id="qrcode"><button class='btn btn-warning noshadow' onclick='makeQr()'>Export Settings</button></div>
    <img id="imgbuf" src="./imgs/qr_icon.png" style="display:none">
    `;
    //<div><input type="checkbox" :id="exchange.shortname + '-enabled'" v-model="exchange.settings.enabled" ></div>
  
    $("#settings_exchanges").html(settingsExchanges);

    let dashboardSettings =
    `
    <div><h3 class="sectiontitle">Dashboard Settings</h3></div>
      <div class="dashboard formgrid">
        <template v-for="setting in data.settings.dashboard" v-if="!setting.hideInSettings">
            <div>{{setting.label}}</div>
            <div class="md-form">
                <input type="checkbox" id="" v-model="setting.value" v-if="setting.check">
                <input type="text" class="form-control" v-model="setting.value" v-if="setting.text && !setting.tooltip" :style="{'text-align':'center',width:setting.width + 'px'}">
                <input type="text" class="form-control" v-model="setting.value" v-if="setting.text && setting.tooltip" data-toggle="tooltip" :title="setting.tooltip" :style="{'text-align':'center',width:setting.width + 'px'}">
            </div>
        </template>
      </div>
    `;
    // :style="{width:setting.width + 'px'}"
    $("#settings_dashboard").html(dashboardSettings);
  }

  function makeFeedbackPage() {
      let feedback = `
        <div id="feedback_form" class="tabcontentholder">
            <!--<form action="../api/feedback.php" method="post">-->
                <div><h3 class="sectiontitle">Feedback</h3></div>
                <div class="title" style = "margin-left: 20px">
                    <h4>Which Exchange Should We Support Next?</h4>

                    <select id="newexchange" name="newexchange" class="buypair pairselect">
                        <option value="Select..">Select..</option>
                        <option v-for="ex in data.settings.feedback.exchanges" class="" :value = "ex">{{ex}}</option>
                    </select>
                    <!-- <div style="display: grid; grid-template-columns: 80px 100px">
                    <template v-for="ex in data.settings.feedback.exchanges">
                        <div>{{ex}}</div>
                        <div><input type="checkbox"  :id="ex" ></div>
                    </template>
                    </div> -->
                    <br><br>
                    <h4>What do you not like about BullCryp?</h4>
                        <textarea name="dislike" id="dislike" rows="7" cols="70"></textarea>
                    <br>
                    <h4>What feature(s) would you like to see next?</h4>
                        <textarea name="features" id="features" rows="5" cols="70"></textarea>
                    <div>
                        <button name="feedback" type="" class="btn btn-warning noshadow" onclick="sendFeedback()">Send Feedback</button>
                    </div>
                </div>
            <!--</form>-->
            <div id="feedbackmsg"></div>
        </div>
    `;

    $("#feedback").html(feedback);

    $(`<div class="feedbackbutton">
    <button class="btn btn-warning noshadow" onclick = "doNav('#Feedback')" title="Tell us what you think">Feedback</button>
    </div>`).insertAfter("#header");

    

  }

function sendFeedback(){
        $.ajax({
            url: "https://bullcryp.com/api/feedback.php",
            type: "POST",
            data: {
                feedback: "true",
                email: user.email,
                nextexchange: $("#newexchange").val(), 
                dislike: $("#dislike").val(), 
                features: $("#features").val()
            },
            success: function(msg) {
                $("#feedback_msg").html("<br><br><center><H3 style='color:green'>Thank you! Your feedback has been sent.</h3></center>");
            },
            error: function(msg){
                $("#feedbackmsg").html("<br><br><center><H3 style='color:red'>Feedback service is temporarily offline for maintenance.. Please try again later!</h3></center>");
            }
        });
        setTimeout(() => {
            $("#feedbackmsg").html("");
        }, 30000);
   
}
window.sendFeedback = sendFeedback;

  //https://s.tradingview.com/widgetembed/?frameElementId=tradingview_02f87&symbol=BINANCE%3ABTCUSDT&interval=3&hidesidetoolbar=0&symboledit=1&saveimage=0&toolbarbg=f1f3f6&details=1&studies=%5B%5D&theme=Light&style=1&timezone=Etc%2FUTC&withdateranges=1&studies_overrides=%7B%7D&overrides=%7B%7D&enabled_features=%5B%5D&disabled_features=%5B%5D&locale=en&utm_source=bullcryp.com&utm_medium=widget&utm_campaign=chart&utm_term=BINANCE%3ABTCUSDT