/** account info */
function getBinanceAccountInfo(){
  try {
    if(histories.exchanges[0].accountInfo ){return;}
    APIBull(exchangeEnum.binance,"account",true,function(err,data){
      data.balances = [];
      histories.exchanges[0].accountInfo = data;
    });
  }catch(e){ }
}
window.getBinanceAccountInfo = getBinanceAccountInfo;

function getPoloAccountInfo(){
  try {
    //TODO put in equiv check as binance above not to grab data if we already have it
    APIBull(exchangeEnum.polo,"account",true,function(err,data){
      // if(err){
      //   //showMessage("Error",err,"error", 2000) //TODO handle Nonce error
      // }
      if(!data){return;}
      histories.exchanges[1].accountInfo = data.decoded;
    });
  }catch(e){  }
}
window.getPoloAccountInfo = getPoloAccountInfo;

/**deposit history */
async function getBinanceDepositHistory(){
  try {
    let data = await getAPIDataAsync(exchangeEnum.binance,"dephistory",null,true,true,"async");

    if(data && JSON.parse(data).depositList){
      histories.exchanges[0].depositHistory = JSON.parse(data).depositList;
    }

  }catch(e){ console.log(e); }
}
window.getBinanceDepositHistory = getBinanceDepositHistory;

function getPoloDepositHistory(){
  
}


/**Trade History */
async function getBinanceTradeHistory(){
  if(app.data.currentState.tradeHistoryUpdateInProgress){return;}
  app.data.currentState.tradeHistoryUpdateInProgress = true;
  if (!histories.exchanges[0].tradeHistory){
    histories.exchanges[0].tradeHistory = [];
  }
  //TODO get latest tradeId stored locally, and send the $fromTradeId param to API
  try {
    let lastDate = histories.exchanges[0].tradeHistory.lastTradeDate || moment().subtract(3,"year").unix();
    let newLastDate = lastDate;
    for(pair of app.getExchangeByIndex(0).pairs){
      let item = await getBinanceTradeHistoryItem(pair,lastDate);
      item = JSON.parse(item);
      if(item.length > 0){
        if(item[item.length - 1].time > lastDate){
          newLastDate = item[item.length - 1].time;
        }
        //TODO check if pair is already in array, and replace it
        histories.exchanges[0].tradeHistory.trades.push({pair: pair, data: item});
      }
      await sleep(100); // pause .1 seconds between each
    }
    histories.exchanges[0].tradeHistory.lastTradeDate = newLastDate;
    return "done";
  }catch(e){ 
    console.log(e);
  }
  
  app.data.currentState.tradeHistoryUpdateInProgress = false;
}
async function getBinanceTradeHistoryItem(symbol, date){
  try {
    var params = {
      currency: symbol,
      limit: 1000,
      startTime: date
    };
    let item = await getAPIDataAsync(exchangeEnum.binance,"history",params,true,true,"async");
    return item;
  }catch(e){ console.log(e); }
}

async function getPoloTradeHistory(){
  try {
    var result;
    let lastDate = moment().subtract(3,"year").unix();
    let newDate = moment().unix();
    let itemLastDate, trades=[], index, foundEnd , coin;
    let newLastDate = lastDate;
    for(pair of app.getPairs(exchangeEnum.polo)){
      if(histories.exchanges[1].tradeHistory.lastTradeDate>99999){
        lastDate = histories.exchanges[1].tradeHistory.lastTradeDate + 1;
      }else{
        lastDate = moment().subtract(3,"year").unix();
      }
      newDate = moment().unix();
      foundEnd = false;
      while (!foundEnd) {
        let params = {
          currency: pair,
          start: lastDate,
          end: newDate,
          limit: 9999
        }; 
        //TODO get the date of the oldest trade, set as the end date, and grab more data
        result = await getAPIDataAsync(exchangeEnum.polo,"history",params,true,true,"async");

        if(JSON.parse(result).decoded.length > 0){
          //got result, get the oldest date
          result = objectToArray(JSON.parse(result).decoded);
          
          if(result.length > 499){
            foundEnd = false;
            newDate = moment(result[result.length-1][1].date).unix();
            lastDate = moment.unix(newDate).subtract(3,"year").unix();
          }else{
            foundEnd = true;
          }
          
          //flatten the trades array, then store

          result = result.map(x=>x = x[1]);
          index = trades.findIndex(x => x.pair == pair);
            if(index >= 0){
              trades[index].trades = trades[index].trades.concat(result);
              trades[index].trades = _.uniqBy(trades[index].trades,"globalTradeID");
            }else{
              trades.push({pair:pair,trades:result}); 
            }
          
       
        }else{
          foundEnd = true;
        }
      }
      //pause between each pair, so not to exceed rate limit on polo
      //await sleep(500)
      
     
    }
    histories.exchanges[1].tradeHistory =  trades;
  }catch(e){ 
    console.log(e); 
  }
}