


const POLO_PUBLIC_API = "https://poloniex.com/public?command=";
const BINANCE_PUBLIC_API = "";

const PRIVATE_API = "https://bullcryp.com/api/";//'https://poloniex.com/tradingApi';

const USER_AGENT = "bullcryp-trader 1.1";

var request = window.returnExports;

var binanceTickerSocket, binanceTradeSocket;
var binanceTickerStreamCount = 0;
var binaceTradeStreamCount = 0;






/** AJAX API Call */
function APIBull(exchange, command , keys, callback){
  getAPIDataAsync(exchange, command, null, true, keys, function(err, result, exchange){
    callback(err, result, exchange);
  } );
}
window.APIBull = APIBull;

async function getAPIDataAsync(exchange, command, params, isPrivate, keys, callback, timeout ) {
  
  //callback signature: callback(strErr, Data)
  var URL, method, isAsync = false;

  // if we got the string "async" instead of a callback function,
  // the caller is expecting an async return
  isAsync = callback === "async";

  if(exchange.bantime && exchange.bantime > 0){
    if(exchange.bantime > Number(new Date()) + 30000 ){
      //still banned
      if (isAsync){
        return "banned";
      }else{
        callback("banned");
      }
      console.log("bounced banned request");
      console.log("remaining time: " + moment(exchange.bantime + 30000).diff( new Date(),"seconds" ));
      return;
    }else{
      //no longer banned
      exchange.bantime = 0;
    }
  }


  //dataToSend: token, key, secret, params
  dataToSend = {};
  dataToSend.token = getLs("token");
  if(params){
    dataToSend.params = JSON.stringify(params);
  }

  if(keys){
    if(exchange.shortname == "polo"){
      dataToSend.key = app.data.settings.exchanges[1].settings.key;
      dataToSend.secret = app.data.settings.exchanges[1].settings.secret;
    }else if (exchange.shortname == "bin"){
      dataToSend.key = app.data.settings.exchanges[0].settings.key;
      dataToSend.secret = app.data.settings.exchanges[0].settings.secret;
    }
  }
  
  if(isPrivate){
    URL = PRIVATE_API + exchange.privURLPart + command;
    method = "POST";
  }else{
    URL = exchange.pubURL + command;
    method = "GET";
  }

  timeout = timeout || app.data.settings.system.ajaxTimeout;

  if (isAsync){  //caller is expecting async return, handle it here
    let result = await $.ajax({
      url: URL,
      method: method,
      dataType: "text",
      timeout: timeout,
      data: dataToSend,
      exchange: exchange
    });
    return result;
  }
  
  try {
      $.ajax({
          url: URL,
          method: method,
          dataType: "text",
          timeout: timeout,
          data: dataToSend,
          exchange: exchange,
          success: function(result, textStatus, xhr){
            let resultcode = xhr.status;
            if (result == ""){
              console.log("empty result");
            }else
            if(result.includes("unauthorized") ){
              //not logged in,  Throw up a message, then redirect to the website
              console.log(result);
              if(oauthProviders){
                location.href = app.data.settings.oauthProfile.logoutLink;
              }else{
                location.href = user.logoutLink;
              }
            }else
            if (callback){
              if(result.toLowerCase().includes("error")){ 
                if(result.includes("banned")){
                  let bantime = Number(result.substr(result.indexOf("IP banned until ") + 16,13));
                  exchange.bantime = bantime;
                }
                if(result.includes("LOT_SIZE")){
                  console.log(result);
                  result = "Error: Binance minimum order size not met";
                }
                callback(result, null, this.exchange);
              }else{
                try {
                  
                  try{
                    result = JSON.parse(result);
                  }catch(e){}
                  callback(null, result, this.exchange);
                  
                }catch(e){ 
                  console.error(e); 
                }
              }
            }
          },
          error: function(xhr,status,error){
              console.log("ajax error: " + error);
          }
      });
      
  }catch(e){ console.log(e);  }

  return;
}
window.getAPIDataAsync = getAPIDataAsync;
/** end AJAX Call */

/** ticker */
function updatePoloTicker(){
  getAPIDataAsync(exchangeEnum.polo,"returnTicker", null,false,false,function (err,ticker, exchange){
    if(!err){
      let tickArr = objectToArray(ticker); 
      app.data.settings.exchanges[1].ticker =  tickArr;

      //set exchange BTC price
      let btcprice = parseFloat(tickArr.filter(e=> e[0]=="USDT_BTC")[0][1].last);
      app.data.settings.exchanges[1].BTCPrice = btcprice; 

      //if binance is down, set app BTC price
      if( app.getExchangeAPIStatus({shortname: "bin"}) == false){
          app.data._BTCPrice = btcprice;
      }
    }
  });
}
window.updatePoloTicker = updatePoloTicker;

function openPoloTickerStream(){
  let options = {
    "command": "subscribe",
    "channel": "ticker"
  };
  //TODO only getting back ping data, no ticker data 
  poloTickerSocket = new WebSocket("wss://api2.poloniex.com/ticker");

  
  poloTickerSocket.onopen = function(e) {
    console.log(e);
    //poloTickerSocket.send(JSON.stringify(options))
  };

  poloTickerSocket.onmessage = function(event){
    // event.data = event.data;
    // console.log(event);
  };

}

function openBinanceTickerStream(){
  binanceTickerSocket = new WebSocket("wss://stream.binance.com:9443/ws/!ticker@arr");

  binanceTickerSocket.onopen = function(e) {
    console.log(e);
  };

  binanceTickerSocket.onmessage = function(event) {
    let msNow = new Date();

    binanceTickerStreamCount += 1;
    //only update every 5th stream to save CPU cycles
    if(binanceTickerStreamCount > 4){
      binanceTickerStreamCount = 0;
      updateBinanceTicker(JSON.parse(event.data));
    }
  };

  binanceTickerSocket.onerror = function(error) {
    console.log("WebSocket Error: " + error);
  };

  binanceTickerSocket.onclose = function(event) {
    //binace will automatically close the stream after 24hrs
    //we can reopen the stream here:
    setTimeout(() => {
      openBinanceTickerStream();
    }, 5000);
    console.log("Disconnected from WebSocket.");
  };

  window.addEventListener("beforeunload", function() {
    binanceTickerSocket.close();
  });
// https://blog.sessionstack.com/how-javascript-works-deep-dive-into-websockets-and-http-2-with-sse-how-to-pick-the-right-path-584e6b8e3bf7
}
window.openBinanceTickerStream = openBinanceTickerStream;

function updateBinanceTicker(ticker){
  let newTicker = [];
  for(i of ticker){
    newTicker.push({
      pair: i.s,
      qvol: i.v,
      hbid: i.b,
      price: i.w,
      lask: i.a,
      perch: i.P,
      bvol: i.Q
    });
  }

  app.data.settings.exchanges[0].ticker = mergeTickerarrays(app.data.settings.exchanges[0].ticker, newTicker);

  function mergeTickerarrays(arrOrg,arrNew){
    if(!arrOrg.length){arrOrg = [];}
    
    var pushed = false;
    for(var i in arrNew){
      var found = false;
      for (var j in arrOrg){
          if (arrOrg[j].pair == arrNew[i].pair) {
              arrOrg[j] = arrNew[i];
              found = true;
              break;
          }
      }
      if(!found){
        arrOrg.push(arrNew[i]);
        pushed = true;
      }
    }
    if(pushed){
      arrOrg =  _.sortBy( arrOrg, "pair" );
    }

    return arrOrg;
  }
}
window.updateBinanceTicker = updateBinanceTicker;

// get binance trade stream for BTC to get live BTC price
// TODO look into multi-stream for multiple symbols
function openBinanceTradeStream(){
  binanceTradeSocket = new WebSocket("wss://stream.binance.com:9443/ws/btcusdt@trade");

  binanceTradeSocket.onopen = function(e){
    console.log(e);
  };

  var lastTickerStream = 0;
  binanceTradeSocket.onmessage = function(event){
    let msNow = new Date();
    
    if(msNow - lastTickerStream > 2000){ // max proces tick every 2 seconds
      lastTickerStream = msNow;
      let price = parseFloat(JSON.parse(event.data).p);
      if(price > 0){
        app.data._BTCPrice = price;
        app.data.settings.exchanges[0].BTCPrice = price;
      }
    }
  };

  binanceTradeSocket.onerror = function(error) {
    console.log("WebSocket Error: " + error);
  };

  binanceTradeSocket.onclose = function(event) {
    //binace will automatically close the stream after 24hrs
    //we can reopen the stream here:
    setTimeout(() => {
      openBinanceTickerStream();
    }, 5000);
    console.log("Disconnected from WebSocket.");
  };

  window.addEventListener("beforeunload", function() {
    binanceTradeSocket.close();
  });
}
window.openBinanceTradeStream = openBinanceTradeStream;

//load the mini history chart for BTC
// function initBTCMiniHistory(){
  
//   var BTCHistImage = new Image();
//   BTCHistImage.src = "https://s2.coinmarketcap.com/generated/sparklines/web/7d/usd/1.png" 

//   setTimeout(() => {
//     updateImage()
//   }, 300);
//   setInterval(() => {
//     updateImage()
//   }, 3600000 * 6);  //update mini-chart every 6 hours

//   function updateImage(){
//     document.getElementById("btchistmini").src = BTCHistImage.src;
//     BTCHistImage = new Image();
//     BTCHistImage.src = "https://s2.coinmarketcap.com/generated/sparklines/web/7d/usd/1.png?time=" + new Date();
//   }
// }
/**Movers */
function requestMoversData(exchange){
  //var command = buildCommandObject(exchange, "movers",undefined,true)
  APIBull(exchange,"movers",false,function(err,data, exchange){
    
    if(err){return;}

    updateMovers(data, exchange); 
  });
  function updateMovers(data, exchange){
    try {
      app.setMovers(data,exchange);
      doMoversSort(exchange.id);
    }catch(e){ console.log(e); }
  }
}
window.requestMoversData = requestMoversData;
/** end Movers */

/** Holdings */
function requestHoldings(exchange){
  if(exchange.is("polo") && app.getExchangeEnabled(exchangeEnum.polo) ){
    APIBull(exchange,"balances",true,function(err,data){
      //if(err){
        //showMessage("Error",err,"error", 2000) //TODO handle Nonce error
      //}
      if(!data){return;}
      updatePoloHoldings( objectToArray(data) );
    });
  }else if(exchange.is("binance")  && app.getExchangeEnabled(exchangeEnum.binance)  ){
    APIBull(exchange,"balances",true,function(err,data){
      if(!data){return;}
      if(err){return;}
      app.data.currentState.exchangeStatus.binance.lastPing=new Date();
      updateBinanceHoldings( objectToArray(data) );
    });
  }
}
window.requestHoldings = requestHoldings;

function updateBinanceHoldings(data){
  var gotErr;
  if(!app.getExchangeEnabled(exchangeEnum.binance) ){return;}
  if(app){
    let holdings = [];
    try {
      data = data.filter( e=> e[1].available >0 || e[1].onOrder > 0 );
      data.forEach(function(item,index){
        let holding = item[1];
        holding.name = item[0];
        holding.onOrders = holding.onOrder;
        if(parseFloat(holding.btcValue) > 0){
          holding.btcValue = parseFloat(holding.btcValue);
        }else if(holding.name == "BTC"){
          holding.btcValue = parseFloat(holding.available) + parseFloat(holding.onOrder); 
        }else{
          holding.btcValue = app.coinPriceBTC(exchangeEnum.binance, holding.name) * ( parseFloat(holding.available) + parseFloat(holding.onOrder)  );
        }
        holding.buyat = 0;
        var prev_holding = app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.binance)].trading.holdings.filter((i)=>{return i.name==holding.name;} );
        if(prev_holding.length > 0 && prev_holding[0].buyat){
          holding.buyat = prev_holding[0].buyat;
        }
        holdings.push(holding);
      });
    }catch(e){ 
      gotErr = true;
    }
    if(gotErr){
      return;
    }

    app.data.currentState.exchangeStatus.binance.gotInitialHoldings = true;

    console.log("got Binance balances");
    app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.binance)].trading.holdings = holdings;

  }
}
window.updateBinanceHoldings = updateBinanceHoldings;

function updatePoloHoldings(data){
  if(!app.getExchangeEnabled(exchangeEnum.polo) ){return;}
  app.data.currentState.exchangeStatus.poloniex.lastPing=new Date();

  var gotErr;
  if(app){
    let holdings = [];
    try{
      data.forEach(function(item, index){
          //make item into single object 
          let holding = item[1];
          holding.name = item[0];
          if(holding.name == "USDT"){  //USDT only polo holding without BTC value included from API
            // calc the btc value
            holding.btcValue = parseFloat(holding.available) / parseFloat(app.coinPriceBTC(exchangeEnum.polo, "USDT") );
          }
          holding.buyat = 0;
          var prev_holding = app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.polo)].trading.holdings.filter((i)=>{return i.name==holding.name;} );
          if(prev_holding.length > 0 && prev_holding[0].buyat){
              holding.buyat = prev_holding[0].buyat;
          }
          holdings.push(holding);
      });
    }catch(e){
      gotErr = true;
    }

    if(gotErr){
      return;
    }
    /** move BTC to first spot */
    if(holdings.find(x => x.name == "BTC")){
        holdings.unshift(                      // add to the front of the array
            holdings.splice(                     // the result of deleting items
                holdings.findIndex(                // starting with the index where
                  x => x.name === "BTC"), // the name is BTC
            1)[0]                             // and continuing for one item
        );
    }
    /** move USDT holdings to front of the array */
    if(holdings.find(x => x.name == "USDT")){
        holdings.unshift(                      // add to the front of the array
            holdings.splice(                     // the result of deleting items
                holdings.findIndex(                // starting with the index where
                  x => x.name === "USDT"), // the name is USDT
            1)[0]                             // and continuing for one item
        );
    }
    
    app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.polo)].trading.holdings = holdings;
    
    app.data.currentState.exchangeStatus.poloniex.gotInitialHoldings = true;

    console.log("got Polo balances");
  }
}
window.updatePoloHoldings = updatePoloHoldings;

/** end Holdings */

/** Orders */
function requestOrders(exchange){
  if(exchange.is("polo") && app.getExchangeEnabled(exchangeEnum.polo)){
    APIBull(exchange,"orders",true,function(err,data){
      if(!data){return;}
      if(err){return;}
      console.log("got Polo Orders");
      updatePoloOrders( objectToArray(data) );
    });
  }else if(exchange.is("binance") && app.getExchangeEnabled(exchangeEnum.binance)){
    APIBull(exchange,"orders",true,function(err,data){
      if(!data){return;}
      if(err){return;}
      app.data.currentState.exchangeStatus.binance.lastPing=new Date();
      console.log("got Binance orders");
      updateBinanceOrders( data );
    });
  }
}
window.requestOrders = requestOrders;

function updatePoloOrders(data){
  app.data.currentState.exchangeStatus.poloniex.lastPing=new Date();

  var gotErr;
  if(app){
    let orders = [];
    try {
      data.forEach(function(item, index){
        item[1].forEach(function(o, oi){
          let order = {
            symbol: item[0],
            startAmount: Number(o.startingAmount),
            type: o.type,
            amount: Number(o.amount),
            price: Number(o.rate),
            orderNumber: o.orderNumber,
            originalObject: item
          }; 
          orders.push(order );
        });
      });
    }catch(e){ 
      gotErr = true; 
    }
    if(gotErr){
      return;
    }
    app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.polo)].trading.orders = orders;
  }
}
function updateBinanceOrders(data){
  var gotErr;
  if(app){
    let orders = [];
    try {
      data.forEach(function(item, index){
          let order = {
            symbol: item.symbol,
            startAmount: Number(item.origQty),
            type: item.side.toLowerCase(),
            amount: Number(item.origQty) - Number(item.executedQty),
            orderNumber: item.orderId,
            price: Number(item.price),
            originalObject: item
          }; 
          orders.push(order );
        });
    }catch(e){ 
      gotErr = true; 
    }
    if(gotErr){
      return;
    }
    app.data.settings.exchanges[app.getExchangeIndex(exchangeEnum.binance)].trading.orders = orders;
  }
}
/** end Orders */


/** buy commands */
// function buyCoin(ex) {
//   let buyAmt
//   if (!ex){ ex = app.getExchangeByIndex() }
//   let pr = app.makePairObject( ex,$("#buypair").val() )
//   let pair = pr.pair
//   let base = pr.market
//   let coin = pr.coin
//   let price = app.coinPriceBTC(ex,coin)
//   let amt = parseFloat($('#buyAmt').val() ) //buy amount box
//   let available = app.getCoinAvailableAmount(ex,base)

//   if(amt){
//       if(!base.includes("USD")) { buyAmt = amt/parseFloat(app.BTCPrice()) } //convert it to BTC
//       buyAmt = buyAmt>0 ? buyAmt : amt  //if buyAmt is 0, use available
//       if(available < buyAmt){ buyAmt = available} //if available is less than buyAmt, use available
//       available = buyAmt
//   }
//   if (available <= 0) {
//       swal({ title: "No " + base + " available", text: "You do not have any " + base + " available to buy " + coin, type: "info", timer: 3000 }).catch(swal.noop)
//   } else {
//       swal({
//           title: 'Buy ' + coin,
//           input: 'text',
//           showCancelButton: true,
//           inputValue: price,
//           html: 'Enter the amount of ' + base + ' to spend, and the price for ' + coin + '<br><br><label for="swal-amount">Amount ' + base + '</label><input id="swal-amount" class="swal2-input" value="' + available + '"><label>Price</label>',
//           inputValidator: function (value) {
//               return new Promise(function (resolve, reject) {
//                   if (jQuery.isNumeric(value) && jQuery.isNumeric($('#swal-amount').val())) {
//                       // calculate amount of coin from amount of base
//                       var amount = ($("#swal-amount").val() / value) * .998
                      
//                       //data = JSON.stringify(data)
//                       // sendToServer("buy", data)
//                       sendBuy(pr.exchange,base,coin,pair,value,amount,0,0,0)
//                       resolve()
//                   } else {
//                       reject('Please re-check your Amount and Price')
//                   }
//               })
//           },
//           onOpen:makeSwalDraggable()
//       }).then(function (result) {
//           if(result.dismiss){return}
//           swal({
//               type: 'success',
//               html: 'Buy order request for: ' + pair + " at bid of: " + result + " sent to server",
//               timer: 1500
//           }).catch(swal.noop)
//       }).catch(swal.noop)
//   }
// }
function buyCoin(ex) {
  if(  $("#buyprice" + ex.id).val()=="" || parseFloat($("#buyprice" + ex.id).val() ) <= 0 ){
    showMessage("Set a Price","You must set a price to buy this coin","info");
    return;
  }
  if (!ex){ ex = app.getExchangeByIndex(); }
  let pr = app.makePairObject( ex,$("#buypair" + ex.id).val() );
  let pair = pr.pair;
  let base = pr.market;
  let coin = pr.coin;
  let price = parseFloat($("#buyprice" + ex.id).val() );  //buy price box
  let baseAmt = parseFloat($("#buyAmt" + ex.id).val() ); //buy amount box
  
  let available = app.getCoinAvailableAmount(ex,base);

  if(baseAmt){
      //if(!base.includes("USD")) { buyAmt = amt/parseFloat(app.BTCPrice()) } //convert it to BTC
      //buyAmt = buyAmt>0 ? buyAmt : amt  //if buyAmt is 0, use available
      if(available < baseAmt){ baseAmt = available;} //if available is less than buyAmt, use available
      //available = baseAmt

  }
  let coinAmt = (baseAmt/ price) * .998;
  if (available <= 0) {
      swal({ title: "No " + base + " available", text: "You do not have any " + base + " available to buy " + coin, type: "info", timer: 3000 }).catch(swal.noop);
  } else {
      let text = "Place an order worth " + baseAmt + " " + base + " to buy " + coinAmt + " " + coin + " at a price of " + price + " from " + ex.name + "?"; 
      let title = "Buy " + coin;
      let confText = "Buy";
      let type = "question";

      doOkCancel(title,text,confText,undefined,type,function(result){
        if(result){
          sendBuy(ex,base,coin,pair,price,coinAmt,0,0,0);
          swal({
              type: "success",
              html: "Buy order request for: " + pair + " at bid of: " + price + " sent to server",
              timer: 1500
          }).catch(swal.noop);
        }
      });
  }
}
window.buyCoin = buyCoin;

function buyCoinNow(ex) {
  let buyAmt;
  if (!ex){ ex = app.getExchangeByIndex(); }
  let pr = app.makePairObject( ex,$("#buypair" + ex.id).val() );
  let pair = pr.pair;
  let base = pr.market;
  let coin = pr.coin;
  var price = app.coinPriceBTC(ex,coin);
  var amt = parseFloat($("#buyAmt"+ ex.id).val() ); //buy amount box

  var available = app.getCoinAvailableAmount(ex, base);
  if(amt){
    if(!base.includes("USD")) { 
      buyAmt = amt/parseInt(app.BTCPrice()); } //convert it to BTC
    buyAmt = buyAmt>0 ? buyAmt : available;  //if buyAmt is 0, use available
      if(available < buyAmt){ buyAmt = available;} //if available is less than buyAmt, use available
      available = buyAmt;
  }
  if (available <= 0) {
      swal({ title: "No " + base + " available", text: "You do not have any " + base + " available to buy " + coin, type: "info", timer: 3000 }).catch(swal.noop);
  } else {
      swal({
          title: "Buy " + coin,
          input: "text",
          showCancelButton: true,
          inputValue: available,
          html: "This will buy " + coin + " now at the current market asks<br>Enter the amount of " + base + " to spend<br><br><label>Amount</label>",
          inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                  if (jQuery.isNumeric(value)) {
                      // calculate amount of coin from amount of base
                      var amount =  (value / price) * .998;
                      // var data = {
                      //     pair: pair,
                      //     price: price,
                      //     amount: amount
                      // }
                      // data = JSON.stringify(data)
                      price = price * buyNowFactor;

                      sendBuy(pair.exchange,base,coin,pair.pair,price,amount,0,0,0);
                      resolve();
                  } else {
                      reject("Please re-check your Amount and Price");
                  }
              });
          }, 
          onOpen:makeSwalDraggable()
      }).then(function (result) {
          if(result.dismiss){return;}
          swal({
              type: "success",
              html: "Buy order request for: " + pair + " at bid of: " + result + " sent to server",
              timer: 1500
          }).catch(swal.noop);
      }).catch(swal.noop);
  }
}
window.buyCoinNow = buyCoinNow;

function sendBuy(exchange,base,coin, currency, rate, amount, fillOrKill, immediateOrCancel,postOnly){
  let newAmount;
  if(exchange.shortname == "bin"){
    //amount = amount * .99481
    rate = app.fixBinVal(coin,base,"p",rate);
    newAmount = app.fixBinVal(coin,base,"q",amount);
    if(amount.toFixed(8) == newAmount){
      newAmount = Num(newAmount) * .999;
      newAmount = app.fixBinVal(coin,base,"q",newAmount);
    }
  }else{
    rate = rOund(rate,8,true);
    newAmount = rOund(amount,8,true);
  }
  
  var params = {
    currency: currency,
    rate: rate,
    amount: newAmount,
    fillOrKill: fillOrKill,
    immediateOrCancel: immediateOrCancel,
    postOnly: postOnly
  };
  getAPIDataAsync(exchange,"buy",params,true,true,function(err,data){
    if (err){
      data = err;
    }
    console.log(data);
    showAlert("Buy Order Confirmation",data); 
    forceUpdateBalancesAndHoldings();
  });
}

/** Orders Commands */
function moveOrder(orderId, price, amount, pair) {
  if (!ex){ ex = app.getExchangeByIndex(); }
  let pr = app.makePairObject( ex,pair );
  let base = pr.market;
  let coin = pr.coin;
  swal({
      title: "Change Bid for " + pair,
      input: "text",
      showCancelButton: true,
      inputValue: price,
      html: "<label for=\"swal-amount\">Amount " + base + "</label><input id=\"swal-amount\" class=\"swal2-input\" value=\"" + amount + "\"><label>Price</label>",
      inputValidator: (value) => {
          return new Promise((resolve, reject) => {
              if(value == 0 || value == undefined || value == ""){
                  sendCancelOrder(ex,orderId);
                  return;
              }
              if (jQuery.isNumeric(value)) {
                  sendMoveOrder(ex,orderId, value, $("#swal-amount").val(), 0,0);
                  resolve();
              } else {
                  jQuery(".swal2-input").val(price);
                  reject("Please re-check your new price");
              }
          });
      }, 
      onOpen:makeSwalDraggable()
  }).then(function (result) {
      if(result.dismiss){return;}
      swal({
          type: "success",
          html: "Change request for: " + pair + " at bid of: " + result + " sent to server",
          timer: 1500
      }).catch(swal.noop);
  }).catch(swal.noop);
}
window.moveOrder = moveOrder;

function cancelOrder(ex,orderID,symbol) {
  swal({
      title: "Are you sure you want to cancel this order?",
      text: "You will not be able to undo once you agree",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, cancel it!", 
      onOpen:makeSwalDraggable()
  }).then(
      function (result) {
          if(result.dismiss){return;}
          swal({
              title: "Cancelling...",
              text: "Cancel order being sent to server",
              type: "info",
              timer: 1500
          }).catch(swal.noop);
          var btn = $("#" + orderID + ".cancelorder");
          // sendToServer('cancelOrder', orderID)
          sendCancelOrder(ex,orderID,symbol);
          flashItem("#" + orderID);
          btn.text("Cancelling..");
          setTimeout(function () {
              btn.text("Cancel Order");
          }, 5000);
      }
      ).catch(swal.noop);
}
window.cancelOrder = cancelOrder;

function sendMoveOrder(exchange,orderNum, price, amount, immediateOrCancel, postOnly){
  var params = {
    orderNumber: orderNum,
    rate: price,
    amount: amount,
    postOnly: postOnly,
    immediateOrCancel: immediateOrCancel
  };
  
  getAPIDataAsync(exchange,"moveOrder",params,true,true,function(err,data){
    if(err){
      data = err;
    }
    console.log(data);
    showAlert("Modify Order Confirmation",data); 
    forceUpdateBalancesAndHoldings();
  });
}
function sendCancelOrder(exchange,orderNum, pair){
  var params = {
    orderNumber: orderNum,
    currency: pair
  };

  
  getAPIDataAsync(exchange,"cancelOrder",params,true,true,function(err,data){
    if(err){
      data = err;
    }
    console.log(data);
    showAlert("Cancel Order Confirmation",data); 
    forceUpdateBalancesAndHoldings();
  });
}

/** Sell Commands */
function dumpCoin(holding) {
  let ex = app.getExchangeByIndex(); 
  let pair = app.makePairObject(null,null,holding.name);
  let coin = pair.coin;
  let base = pair.market;
  let pairSet = pair.pair;
  let curPrice = app.coinPriceBTC(ex,coin);
  if(coin != "BTC" && base.includes("USD")){curPrice = curPrice * app.BTCPrice();}
  let available = app.getCoinAvailableAmount(ex, coin);
  if (available <= 0) {
      swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 2000 }).catch(swal.noop);
  } else {
      swal({
          title: "Are you sure you want to dump all " + coin + " to " + base + "?",
          text: "This will immediately sell the coin starting at the highest bids price.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, dump it!",
          showLoaderOnConfirm: true,
          preConfirm: function () {
              return new Promise(function (resolve) {
                  setTimeout(function () {
                      resolve();
                  }, 1000);
              });
          }, 
          onOpen:makeSwalDraggable()
      }).then(
      function (result) {
          if(result.dismiss){return;}
          swal({ title: "Sending...", text: "Dump order being sent to server", type: "info", timer: 1500 }).catch(swal.noop);
          // var dumpOrder = {
          //     pair: pairSet,
          //     amount: amount,
          //     price: curPrice
          // }
          sendSell(ex,base,coin,pairSet,curPrice * dumpFactor,available,0,0,0);
          /** TODO if oder exists, change the order rather then putting in new order */
          // sendToServer("dumpcoin", JSON.stringify(dumpOrder))
      },
      function (dismiss) { }
      ).catch(swal.noop);
  }
}
window.dumpCoin = dumpCoin;

function sell(holding) {
  let ex = app.getExchangeByIndex(); 
  let pair = app.makePairObject(null,null,holding.name);
  let coin = pair.coin;
  let base = pair.market;
  let pairSet = pair.pair;
  let decimals = 7;
  let curPrice = app.coinPriceBTC(ex,coin);
  if(coin != "BTC" && base.includes("USD")){ 
    curPrice = curPrice * app.BTCPrice();
    decimals = 2;
  }
  let available = app.getCoinAvailableAmount(ex, coin);
  if (available <= 0) {
      swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 3000 }).catch(swal.noop);
  } else {
      // var base = (coin == "BTC") ? "USDT" : "BTC"
      // var pairSet = base + "_" + coin
      // var lowAsk = parseFloat(getPairLowAsk(pairSet)) TODO
      // var highBid = parseFloat(getPairHighBid(pairSet))
      // var sellPrice = round(highBid + ((lowAsk - highBid) * settings.system.sellFactor), 8)
      // sellPrice = sellPrice.replace(/0+$/, "")
      let percentAmount = 100;
      //if (sellPrice == lowAsk) { sellPrice = highBid }
      swal({
          title: "Sell " + coin + " to " + base + "?",
          // text: ,
          html: "This will place an order at your current price" //just below the low ask price. While not guaranteed, this should ensure you do not pay the taker fee."
          + "<br><br>"+makePercentSlider(available,coin), // +"<div>Low ask: " + lowAsk + "</div><div>High bid: " + highBid + "</div><br><div>Estimated bid: " + sellPrice + "</div>",
          type: "info",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, sell it!",
          showLoaderOnConfirm: true,
          preConfirm: function () {
              return new Promise(function (resolve) {
                  percentAmount =  parseFloat(available * parseFloat(jQuery(".rangepercent").val()/100)).toFixed(8);
                  setTimeout(function () {
                      resolve();
                  }, 1000);
              });
          }, 
          onOpen:makeSwalDraggable()
      }).then(
          function (result) {
              if(result.dismiss){return;}
              swal({ title: "Sending...", text: "Sell order being sent to server", type: "info", timer: 1500 }).catch(swal.noop);
             
              curPrice = rOund(curPrice,decimals);
              sendSell(ex,pairSet,curPrice,percentAmount,0,0,1);
          },
          function (dismiss) { }
      ).catch(swal.noop);
  }
}
window.sell = sell;

function sellAt(holding) {
  let ex = app.getExchangeByIndex();
  let pair = app.makePairObject(null,null,holding.name);
  let coin = pair.coin;
  let base = pair.market;
  let pairSet = pair.pair;
  let curPrice = app.coinPriceBTC(ex,coin);
  if(coin != "BTC" && base.includes("USD")){ curPrice = curPrice * app.BTCPrice(); }
  let available = app.getCoinAvailableAmount(ex, coin);
  if (available <= 0) {
      swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 3000 }).catch(swal.noop);
  } else {
      swal({
          title: "Sell " + coin + " to " + base + "?",
          text: "Enter your sell price",
          html: makePercentSlider(available,coin) + "This will place an immediate sell order for your Coin." + //<br><b>High bid: </b>" + getPairHighBid(pairSet) + 
                "<center><table style='margin-top:10px'><tr><td><label style='margin-right:5px;margin-bottom:0'>Bid: </label> </td><td id='swalbid'> " + curPrice + "</td></tr></table></center>" , //TODO <b>Low Ask: </b>" + getPairLowAsk(pairSet)  + "<br>
          input: "range",
          inputValue: 0, //round(curPrice, 8),
          inputAttributes: {
              min: -10, //round(Math.min(getPairHighBid(pairSet), curPrice) * (1 - sellAtRangeFactor), 8),
              max: 200, //round(Math.max(getPairLowAsk(pairSet), curPrice) * (1 + sellAtRangeFactor), 8),
              step: .5
          },
          type: "info",
          showCancelButton: true,
          //inputValue: curPrice,
          inputValidator: (value) => {
              return new Promise((resolve, reject) => {
                  // if (jQuery.isNumeric(value)) {
                      let sellOrder = {
                          pair: pairSet,
                          amount: parseFloat(available * parseFloat($(".rangepercent").val()/100) ),
                          price: $("#swalbid").text().trim()
                      };
                      //sendToServer("sellat", JSON.stringify(sellOrder))
                      
                      sendSell(ex,base, coin,pairSet,sellOrder.price,sellOrder.amount,0,0,0);

                      resolve();
                  // } else {
                  //     reject('Please re-check your price')
                  // }
              });
          }, 
          onOpen:makeSwalDraggable()
      }).then(
          function (result) {
              if(result.dismiss){return;}
              swal({
                  type: "success",
                  html: "Order request for: " + coin + " at bid of: " + result + " sent to server",
                  timer: 1500

              }).catch(swal.noop);
          }
          ).catch(swal.noop);
      jQuery(".swal2-range input").on("input", function () {
          var range = jQuery(".swal2-range input");
          var percent = (range.val()/100) + 1;
          var value = scientificToDecimal(curPrice * percent,8);
          jQuery("#swalbid").text(  value  );
          // range.prop('max', round(val * (1 + sellAtRangeFactor), 8))
          // range.prop('min', round(val * (1 - sellAtRangeFactor), 8))
      });
  }
}
window.sellAt = sellAt;

function sendSell(exchange,base, coin, currency, rate, amount, fillOrKill, immediateOrCancel,postOnly){
  
  //.1 % factor
  // get current account trading fees and calculate
  // .999 = .001 = .1% fee. 
  let newAmount;

  if(exchange.shortname == "bin"){
    //amount = amount * .99481
    rate = app.fixBinVal(coin,base,"p",rate);
    newAmount = app.fixBinVal(coin,base,"q",amount);
    if(amount.toFixed(8) == newAmount){
      newAmount = Num(newAmount) * .999;
      newAmount = app.fixBinVal(coin,base,"q",newAmount);
    }
  }else{
    rate = rOund(rate,8,true);
    newAmount = rOund(amount,8,true);
  }
  
  var params = {
    currency: currency,
    rate: rate,
    amount: newAmount,
    fillOrKill: fillOrKill,
    immediateOrCancel: immediateOrCancel,
    postOnly: postOnly
  };
  getAPIDataAsync(exchange,"sell",params,true,true,function(err,data){
    if (err){
      data = err;
    }
      console.log(data);
      showAlert("Sell Order Confirmation",data); 
      forceUpdateBalancesAndHoldings();
  });
}

/** Helpers */
function objectToArray(data){
  var dataArray = []; 
    Object.keys(data).forEach(function (key) {
      dataArray.push([key, data[key]]);
    });
    return dataArray;
}
window.objectToArray = objectToArray;


