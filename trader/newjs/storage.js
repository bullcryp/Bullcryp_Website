
//store the user email with exchange keys.. so that another user signing in with the system 
// doesn't have access to the keys


function setLs(name, data, isJSON, showLog, useremail){

    let email = useremail || user.email;

    //1 - stringify the data
    if(isJSON){
        data = JSON.stringify(data);
    };

    //2 - put the data in the storage object
    let storageObj = {
        storageUser: email,
        data: data
    };

    //3 - stringify the storage object
    storageObj = JSON.stringify(storageObj);
    
    //4 - LZCompress the storage object
    storageObj = LZString.compressToBase64(storageObj);

    //5 - append email to name
    name = name + "~" + email;

    if(showLog){ console.log("Saved: " + name + " with size of: " + roughSizeOfObject(storageObj)); }

    //5 - save the compressed storage object
    localStorage.setItem(name, storageObj);

}
window.setLs = setLs;

function getLs(name, isJSON, showLog, useremail){

    let data, storageObj;
    let email = useremail || user.email;
    try{

        //5 append email to name
        name = name + "~" + email;

        //4 - load the compressed storage object
        storageObj = localStorage.getItem(name);

        //3 - decompress the storage object
        storageObj = LZString.decompressFromBase64(storageObj) || storageObj;
        
        //2 - parse the storage object
        try {  //if not parsable, it was old format not in JSON
            storageObj = JSON.parse(storageObj);
        }catch(e){};

        //1.5 - check if this is actually a storage object, 
        //      or the old format straight object
        if(storageObj.storageUser){
            //this is a storage object = deconstruct and set data
            if(storageObj.storageUser == email){
                data = storageObj.data;
                //1 - destringify the data
                if(isJSON){
                    data = JSON.parse(data);
                };
            }
        }else{
            //this is not a storage object, it's just data
            //just set data = storageObj
            data = storageObj;
        }
        
        if(showLog){ console.log("Got: " + name ); }
        return data;
    }catch(e){
        return "";
    }


}
window.getLs = getLs;

function roughSizeOfObject( object ) {
    let objectList = [];
    let recurse = function( value ) {
        let bytes = 0;
  
        if ( typeof value === "boolean" ) {
            bytes = 4;
        } else if ( typeof value === "string" ) {
            bytes = value.length * 2;
        } else if ( typeof value === "number" ) {
            bytes = 8;
        } else if (typeof value === "object"
                 && objectList.indexOf( value ) === -1) {
            objectList[ objectList.length ] = value;
            for( i in value ) {
                bytes+= 8; // assumed existence overhead
                bytes+= recurse( value[i] );
            }
        }
        return bytes;
    };
    return recurse( object );
}
window.roughSizeOfObject = roughSizeOfObject;

function checkForNull(json) {
    for (key in json) {
        if (typeof (json[key]) === "object") {
            return checkForNull(json[key]);
        } else if (json[key] === null) {
            return true;
        }
    }
    return false;
}