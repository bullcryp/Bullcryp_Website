
var toolbarMin = false;
var devmode;
window.devmode = devmode;

const init = ()=>{

  if(window.location.search.includes("dev=true")){
    devmode = true;
  }

  //start chart placeholders pulsing..
  setTimeout(() => {
    pulse("#hhph",1000);
    pulse("#hph",1000);
  }, 2000);
  
  makeVueModel();  //vuemodels.js

  wrapCheckboxes();

  // Tooltips Initialization, HTML and VUE
  setToolTips();

  //get the logged in user info from variables
  getUserInfo();

  //initialize settings
  app.init(); 

  setTVChart();  //functions.js

  setMouseEvents();

  //create user pic
  try {
    $("#userpic").attr("src", oauthProviders[0].photoURL);
  }catch(e){ }


  //set current exchange keys status
  // app.getExchangeEnabled(exchangeEnum.polo)
  // app.getExchangeEnabled(exchangeEnum.binance)
  
  //start the timers group for Exchange API calls
  startTimers();
  

  //set chosen select controls
  setSelect2();

  

  //open binance ticker websocket stream
  openBinanceTickerStream(); 
  openBinanceTradeStream();
  // initBTCMiniHistory()


  //show hidden divs
  setTimeout(() => {
    $("#currPortfolio").css("opacity",1);
    $("#exStatus").css("opacity",1);
    $(".btcprice").css("opacity",1);
  }, 4000);

  makeSectionsCollapsible();

};

// Execute Init
init();

function setSelect2() {
  setTimeout(() => {
    $(".buypair").select2({
      dropdownParent: $("#mystuff")
    });
    $(".buypair").on("select2:open", function() {
      $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Search...");
    });
    $(".buypair").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
        // vm.doCoinClick(app.data.currentState.clicked.exchange, $(".buypair").val() );
        app.setNewBuyPrice();
    });   

    $(".buypair").on("select2:select", function (e) {
      if(this.id == "newexchange") {return;}
      let pr = app.makePairObject(app.data.currentState.clicked.exchange,e.params.data.id);
      app.data.currentState.clicked.pair = pr;
      app.data.currentState.clicked.coin = pr.coin;
      app.data.currentState.clicked.market = pr.market;
      setTVChart(pr);
    });

    $("#newexchange").select2({
      dropdownParent: $("#feedback_form"),
      minimumResultsForSearch: Infinity
    });

    setTimeout(() => {
      $("#buypair1").val("BTCUSDT").trigger("change");
      $("#buypair2").val("USDT_BTC").trigger("change");
    }, 1000);



  }, 5000);
  
}

function getUserInfo(){
  user = app.data.settings.account.user;
    try {
      if(oauthProviders){
        let oauth = app.data.settings.oauthProfile = oauthProviders[0];
        // let oauth = app.data.settings.oauthProfile
        if(!user.firstname && !user.email){
            //no user data, so we'll grab it from the oauth info
            user.firstname = oauth.firstName;
            user.lastname = oauth.lastName;
            user.email = oauth.emailVerified;
            user.photo = oauth.photoURL;
            user.token = app.data.settings.oauthProfile.token.access_token.slice(-20);
            app.saveSettings("account");
        }
      }else{
        user.firstname = idr.username;
        user.email = idr.email;
        user.token = idr.token;
        user.logoutLink = "../login/logout.php";
      }
      setLs("token", user.token );
    }catch(e){ console.log(e); };
}

function wrapCheckboxes(){
  //find all checkboxes <input type="checkbox"> and make into toggle switch
  $("html").find("input:checkbox").wrap("<label class='switch'></label>");
  $(".switch").append("<span class='slider round'></span>");
  //jQuery(".switch input:checked").val() returns "on" or undefined -->
}


function setMouseEvents(){
  //set left menu mouse expand
  $(".navel-wrap").mouseenter(function(e){
    setToolbar(true);
  }).mouseleave(function(e){
    setToolbar(false);
  });

  // cancel left menu mouse expand
  $("#main").mouseenter(function(e){
    setToolbar(false);
  });

  

  // Click events
  //set movers table click event for header sort
  $("#moversWrap > .moversrow:first-child > div:not(:first-child)").on("click",function(e){
    var index = Number( $(this).parent().parent().parent().attr("data-index")  );
    doMoversSort(index, $(this),);
  });
  // $('#moversWrap').on('click','> div:not(:first-child)',function(event){
  //   // event.preventDefault();
  //   var pair  = this.children[1].textContent

  //   flashItem($(this.children[1]),"yellow")

  //   // doPairClick(this,pair)
    
  // })

  // set nav button click events
  $(".nav_element").click(function(e){
    doNav(this);
  });

  $("#trade_tabs").on("click","a", function(e){
    e.preventDefault();
    if(!$(this).hasClass("add_advanced")) {
      $(this).tab("show");
      $(".tradingview-widget-container > div").hide();
      $( $(this)[0].hash ).show();
    }
  });
  $("#add_advanced").click(function(e){
    e.preventDefault();
    setTVChart(undefined,undefined,true);
  });

  // // connect the settings save button to method
  // $("#save_settings").click(function(e){
  //   app.saveSettings()
  // })

  //  // Toggle nav height
  // $('.fas').click(function(e){
  //   let head = $("#header")
  //   if(head.data("size")=='S'){
  //     toolbarMin = false
  //     head.data("size","L")
  //     head.css('height','154px').removeClass("collapsed")
  //     // $("#logo").css("top","36px")
  //     // $("#logo").css("right","8%")
  //     // $("#logo img").css("width","8vw")
  //     $(".greeting-wrap").css("padding","30px 0")
  //     $(".greeting").css("font-size", "4.5vw")
  //     $(".currPortfolio").css("font-size", "2vw")
  //     $("#toolbar").css("width","70px").removeClass("collapsed")
  //     $("#app").css("grid-template-columns","70px auto")
  //     //$("#toolbar div:not(:first)").fadeIn("slow")
  //     $(".nav_element").css("padding","14px").css("margin-bottom","0")
  //     $(".nav_element img").css("width","30px")
  //     $(".nav_element div:first-child").css("text-align","center")
  //     $(".navigation .nav_element div:last-child").css('display','none')
  //     $(".navigation .nav_element div:last-child").css('opacity', '0')
  //     // $("#userpic").css("width","initial")
  //     // $('.userpic').css("margin","10px")
  //     // $("i.fas").removeClass("fa-angle-double-right")
  //     // $("i.fas").css("margin-left", "26px")
  //     $("#trade").css("height", "calc(100% - 194px)")
  //   }else{
  //     toolbarMin = true
  //     head.data("size","S")
  //     head.css('height','50px').addClass("collapsed")
  //     // $("#logo").css("top","4px")
  //     // $("#logo").css("right","36%")
  //     // $("#logo img").css("width","40px")
  //     $(".greeting-wrap").css("padding","0")
  //     $(".greeting").css("font-size", "1.3vw")
  //     $(".currPortfolio").css("font-size", ".9vw")
  //     $("#toolbar").css("width","20px").addClass("collapsed")
  //     $("#app").css("grid-template-columns","20px auto")
  //     //$("#toolbar div:not(:first)").fadeOut("slow")
  //     $(".nav_element").css("padding","2px").css("margin-bottom","10px")
  //     $(".nav_element img").css("width","15px")
  //     $(".nav_element div:first-child").css("text-align","left")
  //     // $("#userpic").css("width","40px")
  //     // $('.userpic').css("margin","5px 10px")
  //     // $("i.fas").addClass("fa-angle-double-right")
  //     // $("i.fas").css("margin-left", "0")
  //     $("#trade").css("height", "calc(100% - 50px)")
  //   }
  // })
  //create logout link
  $("#Logout").click( function(){
    doOkCancel("Log Out","Are you sure you want to log out?","Log Out",null,"warning",function(confirm){
      if(confirm){
        if(oauthProviders){
          location.href = app.data.settings.oauthProfile.logoutLink;
        }else{
          location.href = user.logoutLink;
        }
      }
    });
  });
}

//left menu expansion handler
function setToolbar(enter){
  if(toolbarMin){return;}
  if(enter){
    $("#toolbar").css("width", "185px");
    $("#bullcryp_logo").css("width", "155px");
    // $("#logo").css("font-size",'20px')
    $("#logo").stop().animate({"margin-left":"10px"},200);
    // $("#logo div").stop().animate({"margin-left":"25px"},200)
    // $("#userpic").css('margin-left', '115px')
    $(".navigation .nav_element div:last-child").css("display","block");
    setTimeout(() => {
      $(".navigation .nav_element div:last-child").animate({opacity:1},500);
    }, 250);
  }else{
    $("#toolbar").css("width", "70px");
    $("#bullcryp_logo").css("width", "60px");
    // $("#logo").css("font-size","")
    $("#logo").stop().animate({"margin-left":""},200);
    // $("#logo div").stop().animate({"margin-left":""},200)
    // $("#userpic").css('margin-left', '10px')
    $(".navigation .nav_element div:last-child").css("display","none");
    $(".navigation .nav_element div:last-child").css("opacity", "0");
  }
}

//navigation handler
function doNav(el){
  if( !$(el).is("#Logout") ){
    
    //scroll to top of page
    jQuery("html,body").animate({ scrollTop: 0 }, 100);

    //set nav tab name
    app.data.currentState.visibility.navTab = el.id;

    if( !$(el).is("#Settings") && $("#Settings").hasClass("selected") ){
      //switching away from settings tab.. ensure one exchange min is enabled
      // checkEnabledExchanges()
      app.saveSettings();
    }

    if($(el).is("#Feedback")){
      $(".feedbackbutton").hide();
    }else{
      $(".feedbackbutton").show();
    }
    //close left menu on click
    setToolbar(false);

    if ( $(el).is("#Settings")) {
      $("#qrcode").html("<button class='btn btn-warning noshadow' onclick='makeQr()'>Export Settings</button>");
    }

    let showing, hiding;
    if( $(el).is("#Overview") ){
      if($("#header").prop("clientHeight") == 0){
        showing = true;
      }
    }else {
      if( $("#header").prop("clientHeight") == 300){
        hiding = true;
      }
    }

    //if hiding, close the header, then change page
    if(hiding){
      $("#header > div:nth-child(3)").fadeOut(300);
      setTimeout(() => {
        $("#header").slideUp(500, function(){
          finalFade();
        });
      }, 200);
    //if showing, change page, then show the header
    }else if(showing){
      finalFade(function(){
        $("#header").slideDown(500,function(){
          $("#header > div:nth-child(3)").fadeIn(300);
        });
      });
    }else{
      finalFade();
    }
    
  }
  function finalFade(callback){
    //change the visible panel
    $(".nav_element").removeClass("selected");
    $(el).addClass("selected");

    // Change main area section
    let sec;
    sec = "#" + $(el).data("sec");
    $(".section").fadeOut();
    $(sec).fadeIn("slow",function(){
      if(callback){callback();};
    });
  }
}
window.doNav = doNav;

function checkEnabledExchanges(){
  //if not at least one exchange is enabled, enable the first one (Binance)
  //TODO change this to use keys, secret check - no more enabled switch
  // if( !app.data.settings.exchanges.some(function(e){ return e.settings.enabled === true }) ){
  //   app.data.settings.exchanges[0].settings.enabled = true
    // app.saveSettings('exchange')
  // }
}

