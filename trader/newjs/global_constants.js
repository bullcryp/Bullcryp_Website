/***global variables */

//flag to use for script loader knowing if we've already loaded scripts in index file for debugging
var initComplete = false;

/**analysis module */
const detectDumpOn = 3;  //consecutive direction changes
const detectPumpOn = 3;

const largeChangeAmount = .1;  //percent change on one tick to classify as big change

//global user object
var user;

var news;

/** user options */
const defaultUsername = "Stefano";

/** Order options */
const sellFactor = window.sellFactor = .85;  //   percent range between high bid and low ask
const sellAtRangeFactor = window.sellAtRangeFactor = .1; //20% range in either direction
const buyNowFactor = window.buyNowFactor = 1.25;
const dumpFactor = window.dumpFactor = .75;
//global flash object for data input 
var flash;

//holdings and orders count
var holdingsCount, ordersCount;

var orderbookLength = 5;

/** history items */
var histories = window.histories = {
    exchanges: [
        {  //binance
            exchangeInfo: {},
            accountInfo: {},
            depositHistory: {},
            tradeHistory: {
                lastTradeDate: 99999,
                trades: []
            }
        },
        {  //polo
            exchangeInfo: {},
            accountInfo: {},
            depositHistory: {},
            tradeHistory: {
                lastTradeDate: 99999,
                trades: []
            }
        }
    ],
    accountBalanceHistory: {
        lastSave: 99999,
        data: []
    }
};

//store for CMC data
var cmcData = window.cmcData = {
    metadata: {},
    data: []
};


/**Pair Vue Models Enum */
var pairModelEnum = window.pairModelEnum ={
    pair: 0,
    last: 1,
    lowestAsk: 2,
    highestBid: 3,
    percentChange: 4,
    baseVolume: 5,
    quoteVolume: 6,
    isFrozen: 7,
    low24hr: 8,
    high24hr: 9,
    range24hr: 10,
    WS: 11,
    C: 12,
    PL: 13,
    historyBarLength: 14,
    sameCount: 15
};

/**Exchanges Enum */
const exchangeEnum = window.exchangeEnum = {
    binance: {
        index:0,
        id: 1,
        name: "Binance",
        shortname: "bin",
        pubURL: "https://api.binance.com",
        privURLPart: "binance/",
        is: function(name){
            return name.toLowerCase() == this.shortname || name.toLowerCase() == this.name.toLowerCase();
        }
      },
    polo: {
        index: 1,
        id: 2,
        name: "Poloniex",
        shortname: "polo",
        pubURL: "https://poloniex.com/public?command=",
        privURLPart: "polo/",
        is: function(name){
            return name.toLowerCase() == this.shortname || name.toLowerCase() == this.name.toLowerCase();
        }
    },
    nextexchange: {
        index: 2,
        id: 3,
        name: "Next",
        shortname: "nxt",
        pubURL:"http://next/api/",
        privURLPart: "next/",
        is: function(name){
            return name.toLowerCase() == this.shortname || name.toLowerCase() == this.name.toLowerCase();
        }
    }
  };