


var vm = {

  init: function(){

    app.loadSettings();
    
    //app.data.settings.system.currentExchange = app.data.settings.exchanges[0]
    
    //remove origingal dom element
    $("#oauth").replaceWith("");
    $("#session_user").replaceWith("");

    //initialize API status timers
    app.data.currentState.exchangeStatus.poloniex.lastPing = moment().subtract(2,"minute");
    app.data.currentState.exchangeStatus.binance.lastPing = moment().subtract(2,"minute");

    //show the gain/lose labels
    $(".gainvslose").removeClass("hide");


    //make tradingview tickers
    setTimeout(() => {
      makeTVTicker(true);
    }, 4000);
    setTimeout(() => {
      makeTVTicker();
    }, 5000);

    //reveal the header tint
    setTimeout(() => {
      $(".headertint").animate({"opacity":".6"},6000);
    }, 2000);

    if(app.getExchangesEnabled()){

      //set the current exchange
      app.data.currentState.clicked.exchange = app.getExchangeByIndex(0);

      setGreeting("Welcome back, ");


      //TODO start these one time API calls in staggered way.. so not to bog the startup

      //get the exchange info
      setTimeout(() => {
        getBinanceInfo();
        app.setNewBuyPrice();
      }, 1000);
      
      //get the account info
      setTimeout(() => {
        getBinanceAccountInfo();
        getPoloAccountInfo();
      }, 2500);
      

    }else{
      setGreeting("Welcome, ");
      //set greeting message
      $("#currPortfolio").text("Your portfolio balance is $0 USD, 0 BTC");
      //TODO start the onboarding sequence

      //set the main charts image placeholder
      $("#overview_home").css("display", "block").css("text-align","center").css("position","relative");
      $("#overview_home").html(
        `<img src='imgs/default_portfolio_image.png'>
         <div class="onboardOverlay" onclick="startOnboard()"></div>
         <div class="onboardButton">
         </div>
        `
      );
      $("#overview_home img").css("grid-column-start","1")
        .css("grid-column-end", "3")
        .css("width","80vw")
        .css("height","27vw")
        .css("max-height", "27vw");

      setTimeout(() => {
        let title = "Getting Started";
        let mess = `
                    <div>
                      To get started, set up your exchange keys from at least one trading account. 
                      These keys are stored on your local system, and are never stored on our servers
                    </div>
                    <button id="onboardButton1" onclick="startOnboard()" class="btn btn-warning btn-lg waves-effect waves-light animated flipInX">Configure<br> Your Exchanges</button>
                    `;
        showMessage(title,mess,"info");
      }, 4000);

      $(".onboardOverlay").css({"opacity":".6"});
      $(".onboardButton").html("<button id=\"onboardButton1\" onclick=\"startOnboard()\" class=\"btn btn-warning btn-lg waves-effect waves-light animated flipInX\">Configure<br> Your Exchanges</button>");

      //onboarding for trade section
      $("#trade_section").css("display", "block").css("text-align","center").css("position","relative");
      $("#trade_section").prepend("<div class=\"onboardOverlay\" onclick=\"startOnboard()\"></div>");
      $("#trade_section").append("<div class=\"onboardButton\"><button id=\"onboardButton2\" onclick=\"startOnboard()\" class=\"btn btn-warning btn-lg waves-effect waves-light animated flipInX\">Configure<br> Your Exchanges</button></div>");

      //onboarding for 'MyStuff' section
      $("#mystuff").css("display", "block").css("text-align","center").css("position","relative");
      $("#mystuff").prepend("<div class=\"onboardOverlay\" onclick=\"startOnboard()\"></div>");
      $("#mystuff").append("<div class=\"onboardButton\"><button id=\"onboardButton3\" onclick=\"startOnboard()\" class=\"btn btn-warning btn-lg waves-effect waves-light animated flipInX\">Configure<br> Your Exchanges</button></div>");

      //TODO get trading panel images - holdings/orders both binance/polo
      
      //next onboarding step

    }

    makeNewsFeeds();

    //alternate tickers display
    var showBTC = true;
    setInterval(() => {
      if(app.data.currentState.visibility.windowHidden){return;}
      if(showBTC){
        $(".tvtickerusd").fadeOut("fast",function(){
          $(".tvtickerbtc").fadeIn("fast");
        });
      }else{
        $(".tvtickerbtc").fadeOut("fast",function(){
          $(".tvtickerusd").fadeIn("fast");
        });
       
      }
      showBTC = !showBTC;
    }, 10000);
    

    app.data.initDone = true;
  },

  saveRestart: function(){
    app.saveSettings();
    location.reload();  //specify true to always reload from server
  },

  saveSettings: function(which){
    try{
      //directly save the settings and account models
      if(!which || which=="exchange"){

          //make a copy of exchanges, filter out movers, and save settings.
          let exchangeCopy = JSON.parse(JSON.stringify(app.data.settings.exchanges));

          for (ex of exchangeCopy){
              ex.movers = {};
          }
          setLs("settings.exchanges", exchangeCopy,true,true);
      }
      if(!which || which=="account"){
        setLs("settings.account", app.data.settings.account,true,true);
      }

      if(!which || which == "dashboard"){
        setLs("settings.dashboard",app.data.settings.dashboard,true,true);
      }
      
    }catch(e){ }
  },

  loadSettings: function(){
    try {
        let exchanges = getLs("settings.exchanges",true,true);
        let account = getLs("settings.account",true,true);
        let hist = getLs("histories",true,true);
        let dashboard = getLs("settings.dashboard",true,true);

        if(account){
          let account = getLs("settings.account",true,true);
          let set = makeNewObj(app.data.settings.account);
          // app.data.settings.account = {...set,...account};
          app.data.settings.account = _.merge({},set,account);
        }

        if(dashboard){
          let dashboard = getLs("settings.dashboard",true,true);
          let set = makeNewObj(app.data.settings.dashboard);
          // app.data.settings.dashboard = {...set,...dashboard};
          app.data.settings.dashboard = _.merge({},set,dashboard);

          //restore labels and tooltips that were saved with the settings so that they can be changed
          app.data.settings.dashboard.hideSmallBalances.label = set.hideSmallBalances.label;
          app.data.settings.dashboard.toggleview.label = set.toggleview.label;
          app.data.settings.dashboard.hideSmallBalancesAmount.label = set.hideSmallBalancesAmount.label;
          app.data.settings.dashboard.hideSmallBalancesAmount.tooltip = set.hideSmallBalancesAmount.tooltip;
        }

        if(hist){
          histories = hist;
          if(histories.accountBalanceHistory.data == []){
            app.data.currentState.tradeHistoryExists = false;
          }
        }else{
          app.data.currentState.tradeHistoryExists = false;
        }
    
        //merge exchange settings using Object.assign - so that newly introduced 
        //settings are not erased by loading the old settings.
        if(exchanges){
            let index = 0;
            for(ex of exchanges){
                ex.index = index;
                let set = app.getExchangeSettings(ex);
                app.data.settings.exchanges[index].trading = ex.trading;
                app.data.settings.exchanges[index].pairs = ex.pairs;
                app.data.settings.exchanges[index].ticker = ex.ticker;
                histories.exchanges[index].exchangeInfo = ex.exchangeInfo;
                histories.exchanges[index].accountInfo = ex.accountInfo;
                
                histories.exchanges[index].depositHistory = ex.depositHistory;

                // app.data.settings.exchanges[index].settings = {...set,...ex.settings};
                app.data.settings.exchanges[index].settings = _.merge({},set,ex.settings);

                //set last binance BTC price
                if(app.data._BTCPrice == 0){
                    if( ex.name == "Binance"){
                        app.data._BTCPrice = ex.BTCPrice;
                    }
                }
                index++;

            }
        }

    }catch(e){ }
  },

  getExchangeAPIStatus: function(ex){
    try {
        let msNow = moment();
        let diff = 0;
        let name="";
        if(ex.shortname == "polo"){
            diff = msNow.diff(app.data.currentState.exchangeStatus.poloniex.lastPing,"seconds");
            name = "poloniex";
        }else if(ex.shortname == "bin"){
            diff = msNow.diff(app.data.currentState.exchangeStatus.binance.lastPing,"seconds");
            name = "binance";
        }
        let active = Math.abs(diff) <= 60;
        app.data.currentState.exchangeStatus[name].APIActive = active;
        return active;  //true|false
    }catch(e){ 
        return false;
    }
  },

  BTCPrice: function(){
    try {
        return app.data._BTCPrice.toFixed(2);
    }catch(e){ 
      return 0;
    }
  },

  getCoinAvailableAmount: function(ex,coin){
    var amount;
    try {
      var obj = app.getHolding(ex,coin);
      if (obj.length == 0){
          amount = 0;
      }else{
          amount = obj[0].available;
      }
    }catch(e){ amount = 0; }

    return parseFloat(amount);
  },
  setExchangeTab: function (tab) {
    try {
      tab.isActive = true;
      app.data.currentState.visibility.activeExchangeTab = tab;
      app.data.settings.tabSettings.exchange.forEach(function (tab) {
          if (tab.id !== app.data.currentState.visibility.activeExchangeTab.id) { tab.isActive = false;}
      });

      //set default for clicked coin
      app.data.currentState.clicked.exchange = app.getExchangeByIndex(tab-1);
      app.data.currentState.clicked.coin = "BTC";
      app.data.currentState.clicked.pair = app.makePairObject();

      

      //wait for Vue to update, then refresh buypair
      // $("#buypair").parent().css("visibility","hidden")
      // setTimeout(() => {
      //     $("#buypair").trigger("change");
      //     $("#buypair").parent().css("visibility","visible")
      // }, 500);
      setTimeout(() => {
        setTVChart();
      }, 200);
    }catch(e){ }
  },

  homwTab: function(tab){
    try {
      if(tab){
          app.data.currentState.visibility.homwtab = tab;
      }else{
          return app.data.currentState.visibility.homwtab;
      }
    }catch(e){ }
  },

  newsTab: function(tab){
    try{
      if(tab || tab == 0){
          app.data.currentState.newstab = tab;
          return true;
      }else{
          return app.data.currentState.newstab;
      }
    }catch(e){ }
  },

  loadAccountHistory: async function(){
    try{
      let bh = getLs("histories",true,true);
      if(bh.data.length > 0){
          bh.data = bh.data.filter(e=>e.totalBtc != "NaN");
          histories.accountBalanceHistory = bh;
      }else{
        app.data.currentState.tradeHistoryExists = false;
        //TODO build account history here
        //get deposit histories
        // let data = await getBinanceDepositHistory()
        //   app.getExchangeByIndex(0).depositHistory = data
        //   data = undefined
        
        // let thresult = await getBinanceTradeHistory()

        // theresult = await getPoloTradeHistory()

        // theresult = theresult
        //TODO get polo deposit history
        
      }
    }catch(e){ }
  },

  holdingsOrdersUpdate: function(){
    //remake the chart whenever data changes
    app.throttlePieChart();
    app.throttleHoldingChart();
  },

  setMovers: function(data, ex){
    try{
      let exIndex = app.getExchangeIndex(ex);
      app.data.settings.exchanges[exIndex].movers.pairMovers = data;
      let pairs =  data.map(v => v[ 0] );
      pairs = _.union(app.data.settings.exchanges[exIndex].pairs,pairs);
      pairs.sort();
      if(pairs.length > 1){
          app.data.settings.exchanges[exIndex].pairs = pairs;
          app.saveSettings("exchange");
      }
    }catch(e){ }
  },

  getExchange: function(ex){
    try{ 
      let exc =  app.data.settings.exchanges.filter(x => x.shortname == ex.shortname);
      return exc[0];
    }catch(e){ }
  },
  getExchangesEnabled: function(){
    return app.getExchangeEnabled(exchangeEnum.polo) || app.getExchangeEnabled(exchangeEnum.binance);
  },
  getExchangeEnabled: function(ex){
    if(ex.shortname == "bin"){
      if(app.data.settings.exchanges[0].settings.key.length > 0 &&
        app.data.settings.exchanges[0].settings.secret.length > 0){
          app.data.currentState.exchangeStatus.binance.enabled = true;
          return true;
        }else{
          app.data.currentState.exchangeStatus.binance.enabled = false;
          return false;
        }
    }
    if(ex.shortname == "polo"){
      if(app.data.settings.exchanges[1].settings.key.length > 0 &&
        app.data.settings.exchanges[1].settings.secret.length > 0){
        app.data.currentState.exchangeStatus.poloniex.enabled = true;
        return true;
      }else{
        app.data.currentState.exchangeStatus.poloniex.enabled = false;
        return false;
      }
    }
  },
  getExchangeByName(name){
    let exc =  app.data.settings.exchanges.filter(x => x.shortname == name || x.name == name);
    return exc[0];
  },
  getExchangeByIndex(index){
    if(!index){index = app.data.currentState.visibility.activeExchangeTab.id-1;}
    return app.data.settings.exchanges[index];
  },
  getExchangeIndex: function(ex){
    try{
      return app.data.settings.exchanges.findIndex(x=> x.name == ex.name);
    }catch(e){ }
  },

  getExchangeSettings: function(ex){
    try{
      let exc =  app.getExchange(ex);
      return exc.settings;
    }catch(e){ 
      return {};
    }
  },

  getExchangeVisibility: function(id){
    try{
      //true if id = 0 (all) or id = id
      let activeTab = app.data.currentState.visibility.activeExchangeTab.id;
      return activeTab == 0 || activeTab == id;
    }catch(e){ }
  },

  exchangeSettingsUpdate: function(){
      app.saveSettings("exchange");
  },

  filteredMovers: function(exchange){
    let fp = exchange.movers.filterPair;
    let pm = exchange.movers.pairMovers;
    try {
        if(fp == "" || fp == undefined){
            return pm;
        }else{
            let fpm = pm.filter(function(i){
                return i[0].toUpperCase().indexOf(fp.toUpperCase()) >= 0;
                // return i[0] == fp
            });
            return fpm;
        }
    }catch(e){ 
      return [];
    }
  },

  getGainers: function(count){
    try{
      count = count || 10;
      let cmc = cmcData.data.filter(function(e){
          return e.rank < 701 && e.quotes.USD.percent_change_24h > 0;
      });
      cmc = _.orderBy(cmc,["quotes.USD.percent_change_24h"],["desc"]);
      cmc = cmc.slice(0,count);
      return cmc;
    }catch(e){
      return [];
     }
  },

  getLosers: function(count){
    try{
      count = count || 10;
      let cmc = cmcData.data.filter(function(e){
          return e.rank < 701 && e.quotes.USD.percent_change_24h <= 0;
      });
      cmc = _.orderBy(cmc,["quotes.USD.percent_change_24h"],["asc"]);
      cmc = cmc.slice(0,count);
      return cmc;
    }catch(e){
      return [];
     }
  },

  getHoldings: function(ex, sortBy, ascending){
    sortBy = sortBy || "btcValue";
    let holdings;
    try{
      if(ascending){
        holdings = makeNewObj(app.getExchange(ex).trading.holdings).sort(function(a,b){return a[sortBy] - b[sortBy];});
      }else{
        holdings = makeNewObj(app.getExchange(ex).trading.holdings).sort(function(a,b){return b[sortBy] - a[sortBy];});
      }
      return holdings;
    }catch(e){
      return [];
     }
  },

  isInHoldings(coin){
    try{
      let tmp;

      let holdingsb = this.getExchange(exchangeEnum.binance).trading.holdings;
      if( holdingsb.filter(x => x.name == coin).length > 0){return true;}
      
      let holdingsp = this.getExchange(exchangeEnum.polo).trading.holdings;
      if( holdingsp.filter(x => x.name == coin).length > 0){return true;}

      return false;

    }catch(e){
      return [];
     }
  },

  getTopHoldings(num){

    num = num || 1000;
  
    let holdings = makeNewObj(app.getExchange(exchangeEnum.binance).trading.holdings);
    let holdings2 = makeNewObj(app.getExchange(exchangeEnum.polo).trading.holdings);
    let index;
    for(item of holdings2){
      index = holdings.findIndex(x => x.name == item.name);
      if(index >= 0){
        holdings[index].btcValue = Number(holdings[index].btcValue) + Number(item.btcValue);
        holdings[index].available = Number(holdings[index].available) + Number(item.available);
      }else{
        holdings.push(item);
      }
    }
    
    holdings.sort(function(a,b){return b.btcValue - a.btcValue;});

    num = Math.min(num, holdings.length);

    return holdings.slice(0, num);
  },

  getHolding: function(ex, coin){
    if(coin=="USD"){coin="USDT";}
    return this.getHoldings(ex).filter(h=>h.name == coin);
  },

  getOrders: function(exchange){
    try{
      let orders = exchange.trading.orders;
      return orders;
    }catch(e){
      return [];
     }
  },

  getPairs: function(ex){
    try{
      ex = ex || app.data.currentState.clicked.exchange;
      if(typeof ex != "object"){
        ex = this.getExchangeByName(ex);
      }
      if(ex.shortname == "polo"){
          return app.data.settings.exchanges[1].pairs;
      }else if(ex.shortname == "bin"){
          return app.data.settings.exchanges[0].pairs;
      }
    }catch(e){ 
      return [];
    }
  },
  pairExists: function(ex,pair){

  },
  getCoinImage: function(coin){
    return "imgs/coins32/" + coin + ".png";
    // return 'https://s2.coinmarketcap.com/static/img/coins/16x16/' + coin + '.png'
  },
  getCoinInfo: function(coin){
    try {
    let longName, price, gainLoss;
    let tmp = cmcData.data.filter(x => x.symbol == coin)[0];
    longName = tmp.name;
    gainLoss = tmp.quotes.USD.percent_change_24h;
    if(coin == "BTC"){
      price = app.BTCPrice(coin);
    }else if(coin == "USDT"){
      price = 1;
    }else{
      price = (app.coinPriceBTC(exchangeEnum.binance,coin) * app.BTCPrice()).toFixed(4);
      if(price == 0){
        price = tmp.quotes.USD.price;
      }
    }
    return {name: longName, gainLoss: gainLoss, price: price};
  }catch(e){ 
    return {name: coin, gainLoss: "-", price: "-"};
   }
  },
  getCMCCoinInfo: function(name,symbol){
    let strWindowFeatures = "location=yes,height=600,width=960,scrollbars=yes,status=yes";
    if(!name && symbol){
      name = vm.getCMCCoinNameFromSymbol(symbol);
    }
    name = name.replace(" ","-");
    let url = "https://coinmarketcap.com/currencies/" + name;
    window.open(url,"_blank",strWindowFeatures);
  },
  getCMCCoinNameFromSymbol(symbol){
    for(coin of cmcData.data){
      if(coin.symbol == symbol){
        return coin.name;
      }
    }
  },
  getPairImages: function(pair){
    //TODO
  },
  doUserMarketClick: function(ex){
    try {
      app.data.currentState.userMarketPreferredToggleUSD = !app.data.currentState.userMarketToggleUSD;
      $("#buyprice"+ ex.id).val( price );
    }catch(e){  }
  },
  toggleChartType: function(type){
    setTVChart(undefined,undefined,!type);
  },
  // doUserMarketToggle: function(noclick, toggle){

  //   let market
  //   if(app.data.currentState.userMarketToggleUSD){  //set to opposite, this only changes after this method
  //     market = noclick?"USDT":"BTC"
  //   }else{
  //     market = noclick?"BTC":"USDT"
  //   }
  //   if(toggle){ app.data.currentState.userMarketToggleUSD = !app.data.currentState.userMarketToggleUSD}
  //   if(market == "BTC" && app.data.currentState.clicked.coin == "BTC"){
  //     market = "USDT"
  //   }else if(market == "USDT" && app.data.currentState.clicked.coin.includes("USD")){
  //     market = "BTC"
  //   }
  //   //set current clicked coin market
  //   app.data.currentState.clicked.market = market

  //   setBuyPair()

  //   setTimeout(() => {
  //     setTVChartBasic()
  //   }, 250);
    
  // },
  visibilityCheckDisabled: false,
  checkVisible: function(ex, element){
    if(!app || vm.visibilityCheckDisabled==true){return true;}
    try {
      cs = app.data.currentState.visibility;
      if (cs.windowHidden){return false;}
      if(cs.navTab == "Settings"){return false;}
      switch (element) {
        case "holdings":
          if(ex.name == cs.activeExchangeTab.name && cs.navTab == "Trade" && cs.homwtab == "holdingspane"){
            return true;
          }else{
            messagelog("hid " + ex.name + " holdings" );
            return false;
          }
          break;
        case "orders":
          if(ex.name == cs.activeExchangeTab.name && cs.navTab == "Trade" && cs.homwtab == "orderspane"){
            return true;
          }else{
            messagelog("hid " + ex.name + " orders" );
            return false;
          }
          break;
        case "movers":
          if(ex.name == cs.activeExchangeTab.name && cs.navTab == "Trade" && cs.homwtab == "moverspane"){
            return true;
          }else{
            messagelog("hid " + ex.name + " movers" );
            return false;
          }
          break;
        case "gainers":
          if (cs.navTab == "Overview"){
            return true;
          }else{
            messagelog("hid gainers/losers" );
            return false;
          }
          break;
      
        default:
          return true;
          break;
      }
    }catch(e){ return true; }
  },

  makePairObject: function(ex, pair, coin){

    //get the selected exchange
    ex = ex || app.getExchangeByIndex() || app.data.settings.exchanges[0];
    let Coin, Market, ChartSym;

    coin = coin || app.data.currentState.clicked.coin;

    if(typeof ex != "object"){
      ex = app.getExchangeByName(ex);
    }

    if(ex.shortname == "polo"){
      //if only coin, make a pair
      if(!pair && coin){
        if(coin=="BTC" || coin.includes("USD")){
          coin = "BTC";
          pair = "USDT_BTC";
          Market = "USDT";
          ChartSym = "BTCUSDT";  //set the chart symbol for TV charts
        }else{
          pair = app.data.currentState.clicked.market + "_" + coin;
        }
      }
      //polo - split the pair at _ and the left side is the market
      Coin = pair.split("_")[1];
      Market = pair.split("_")[0];
    }else if(ex.shortname == "bin"){
      //if only coin, make a pair
      if(!pair && coin){
        if(coin=="BTC" || coin.includes("USD")){
          coin = "BTC";
          pair = "BTCUSDT";
          Market = "USDT";
          ChartSym = "BTCUSDT";  //set the chart symbol for TV charts
        }else{
          pair = coin + app.data.currentState.clicked.market;
          Market = app.data.currentState.clicked.market;
        }
      }
      // extract market symbol from right side to get left side coin
      for(market of ex.markets){
        if( pair.substr(market.length * -1,market.length).includes(market) ){
          Coin = pair.substr(0,pair.length - market.length);
          Market = market; 
        }
      }
    }

    ChartSym = ChartSym || Coin + (Market == "USDT"?"USD":Market);  //set the chart symbol for TV charts

    let pairObj = {
      coin: Coin, 
      exchange: ex, 
      market: Market,
      pair: pair,
      chartSymbol: ChartSym
    };

    return pairObj;
  },
  

  doCoinClick: function(ex,coin){
    try{
      //TODO set buy pair - use base|BTC or base|USD for bitcoin
      app.data.currentState.clicked.exchange = ex;
      app.data.currentState.clicked.pair = app.makePairObject(ex,null,coin);
      app.data.currentState.clicked.coin = coin;
      setTVChart(app.data.currentState.clicked.pair); 
      
      //always reset the toggle labels after coin change TODO see if this makes sense
      // this.doUserMarketToggle(true)

      //set the buy pair
      setBuyPair(ex);

      app.setNewBuyPrice(ex,app.data.currentState.clicked.pair,coin);
      
    }catch(e){ }
  },

  doPairClick: function(ex,pair){
    try{
      //TODO set buy pair
      // load chart
      let pr = app.makePairObject(ex,pair);
      if(pr.market.includes("BTC") ){
        app.data.currentState.clicked.market = "BTC";
      }else if(pr.market.includes("USD")){
        app.data.currentState.clicked.market = "USDT";
      }

      this.doCoinClick(ex,pr.coin);

      // this.doUserMarketToggle(undefined,true)
      //setTVChartBasic(app.makePairObject(ex,pair)) 
    }catch(e){ }
  },
  setNewBuyPrice: function(ex,pair, coin){
    
    ex = ex || app.data.currentState.clicked.exchange;
    pair = pair || app.makePairObject( ex,$("#buypair"+ ex.id).val() );
    coin = coin || pair.coin;
    let price = app.coinPriceBTC(ex,coin);
    
    if(coin != "BTC"){
      if(app.data.currentState.userMarketToggleUSD){
        price = "";
      }
    }

    $("#buyprice"+ ex.id).val( price );
  },

  // getPairCoin: function(ex,pair){
  //   try{
  //     app.data.currentState.clicked.exchange = ex.name
  //     app.data.currentState.clicked.pair = pair
  //     if(ex.shortname == "polo"){
  //       //polo - split the pair at _ and the left side is the market
  //       app.data.currentState.clicked.coin = pair.split("_")[1]
  //       app.data.currentState.clicked.market = pair.split("_")[0]
  //       return pair.split("_")[1]
  //     }else if(ex.shortname == "bin"){
  //       // extract market symbol from right side to get left side coin
  //       for(market of ex.markets){
  //         if( pair.substr(market.length * -1,market.length) == market ){
  //           app.data.currentState.clicked.coin = pair.substr(0,pair.length - market.length)
  //           app.data.currentState.clicked.market = market
  //           return pair.substr(0,pair.length - market.length)
  //         }
  //       }
  //     }
  //   }catch(e){ }
  // },
  getStandardPairName: function(ex,pair){
    try{
      if(ex.shortname == "polo"){
          return pair.split("_")[1] + pair.split("_")[0];
      }else if(ex.shortname == "bin"){
          return pair;
      }
    }catch(e){ }
  },
  getPairMarketName: function(ex,pair){
    try{
      if(ex.shortname == "polo"){
          return pair.split("_")[0];
      }else if(ex.shortname == "bin"){
        for(market of ex.markets){
          if( pair.substr(market.length * -1,market.length) == market ){
            app.data.currentState.clicked.coin = pair.substr(0,pair.length - market.length);
            return market;
          }
        }
      }
    }catch(e){ } 
  },
  coinPriceBTC: function(ex,coin){
    try{
      if(ex.shortname == "polo"){
        if(!app.data.settings.exchanges[1].ticker.length){return 0;}
        let base;
        if(coin  != "USDT" && coin !="BTC"){
            base = "BTC_";
        }else{
            base="USDT_";
            coin="BTC";
        }
        for (tpair of app.data.settings.exchanges[1].ticker){
            if(tpair[0] == base + coin){
                return parseFloat(tpair[1].last).toFixed(8);
            }
        }
        return 0;
      }else if (ex.shortname == "bin"){
          if(!app.data.settings.exchanges[0].ticker.length){return 0;}
          let base;
          if(coin != "USDT" && coin!="BTC"){
            base = "BTC";
          }else{
            base = "USDT";
            coin = "BTC";
          }
          for (tpair of app.data.settings.exchanges[0].ticker){
              if(tpair.pair == coin + base){
                  return parseFloat(tpair.price).toFixed(8);
              }
          }
          return 0;
      }
    }catch(e){ 
      return 0;
    }
  },
  totalBTC: function (ex,fix) {
    try{
      if(ex){
          return app.calcBTC[ex.name];
      }else{
          return app.calcBTC.total;
      }
    }catch(e){
      return 0;
     }
  },

  totalUSD: function(ex,fix) {
    try{
      let result;
      if(ex){
          result = app.calcBTC[ex.name] * app.data._BTCPrice;
      }else{
          result = app.calcBTC.total * app.data._BTCPrice;
      }
      if(fix){result = Number(result).toFixed(fix);}
      return result;
    }catch(e){ 
      return 0;
    }
  },
  getLocalDate: function(date){
    try{
      return unixToHumanDate( moment.unix(date) );
    }catch(e){ }
  },
  
  fixFloat: function(value, places){
    try{
      return Number(value).toFixed(places);
    }catch(e){ 
      return value;
    }
  },

  fixBinVal: function(sym,base,type,num){
    //types: price, qty, base
    let data = histories.exchanges[0].exchangeInfo.symbols.filter(x => x.baseAsset == sym && x.quoteAsset == base)[0];
    let filters = data.filters;
    let digits = data.baseAssetPrecision;
    //first round number to ticksize, then 
    switch (type) {
      case "p":
        num = rOund(num, getPrecision(filters[0].tickSize),true ).toFixed(8);
        break;
      case "q":
        num = rOund(num, getPrecision(filters[1].stepSize),true ).toFixed(8);
        break;
       case "b":
        num = rOund(num, getPrecision(filters[2].minNotional),true ).toFixed(8);
        break;
    
      default:
        break;
    }

    return num;

    function getPrecision(str){
      return Math.max(str.indexOf("1")-1,0);  //where's the 1
    }
  
  },

  filterArticle: function(text){
    let found = text.indexOf("#8217");
    // if (found){
    //   found = found;
    // }
    text = text.replace(";", "");
    text = text.replace("&#8217", "'");
    text = text.replace("&#8216", "'");

    return text;
  },
  buyCoin: function(exchange){
    buyCoin(exchange);
  },

  vdumpCoin: function(holding){
    dumpCoin(holding);
  },
  vsell: function(holding){
    sell(holding);
  },
  vsellAt: function(holding){
    sellAt(holding);
  },
  vmoveOrder: function(orderId,price,amount,pair){
    moveOrder(orderId,price,amount,pair);
  },
  vcancelOrder: function(ex,orderId,symbol){
    cancelOrder(ex,orderId,symbol);
  },

  verifyExchangeKeys: ()=>{},

};
window["vm"] = vm;  //closure-compiler export