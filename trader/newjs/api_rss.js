


//news feed reader data

// get feeds from https://min-api.cryptocompare.com/

async function loadNews(asArray){

  let baseUrl = "https://min-api.cryptocompare.com/data/v2/news/?feeds=";
  let baseUrlEnd = "&extraParams=" + app.data.settings.account.user.firstname;
  let feeds = await getApi("https://min-api.cryptocompare.com/data/news/feeds?extraParams=" + app.data.settings.account.user.firstname);
  
  $(".feed_loading").show();
  
  
  //TODO decide which feeds to include 
  /**
   * CoinDesk
   * CoinTelegraph
   * NewsBTC
   */
  function filterFeeds(feed){
    return "CoinDeskCoinTelegraphNewsBTC".includes(feed.name);
  }
  feeds = feeds.filter(filterFeeds);

  let newArray;
  for (feed of feeds){
    feed.feedData = await getApi( makeUrl(feed.key));
    feed.feedData.Data.length = app.data.currentState.newsArticlesToShow;  //limit to 10 articles

    if(!newArray){
      newArray = feed;
    }else{
      newArray.feedData.Data = newArray.feedData.Data.concat(feed.feedData.Data);
    }
  }

  newArray.key = "Bullcryp";
  newArray.name = "Bullcryp";

  newArray.feedData.Data = newArray.feedData.Data.sort((x,y)=> y.published_on - x.published_on );

  
  news = [newArray];

  app.data.currentState.visibility.newsCurrentPage = 0;
  
  makeNewsFeeds();
  

  function makeUrl(url){
    return baseUrl + url + baseUrlEnd;
  }


  console.log("Loaded Feeds");


};
window.loadNews = loadNews;

var firstNewsLoad=true;
function makeNewsFeeds(direction){
  //:style="{backgroundImage:'url(' + feed.img + ')' }"
  let scrolldirection = 1;
  if(direction == "down"){
    scrolldirection = -1;
    app.data.currentState.visibility.newsCurrentPage-=1;
  }else if(direction == "up"){
    scrolldirection = 1;
    app.data.currentState.visibility.newsCurrentPage+=1;
  }
  let articles;
  try{
      articles = getNewsFeeds();
  }catch(e){
      articles = [];
  }
  let newsFeed;

  try {
    newsFeed = 
    `
    <div style="float:left"><h3 class="sectiontitle">Latest News</h3></div>
    <div style="clear: both"></div>
    <!--Newsfeeds Panels -->
        <div class="feed_loading">Loading News Feeds <img src="imgs/spinner.gif" style="width: 30px;height:30px"><br>
          <img src="imgs/news_placeholder.png"><br>
          <img src="imgs/news_placeholder.png"><br>
          <img src="imgs/news_placeholder.png"><br>
          <img src="imgs/news_placeholder.png"><br>
          <img src="imgs/news_placeholder.png"><br>
          <img src="imgs/news_placeholder.png">
        </div>
        <div  id="feed-panel1" class="in show tab-pane fade active homw" role="tabpanel">`;
            
        let article;
        let pageSize = 6;
        let start = app.data.currentState.visibility.newsCurrentPage;
        start = start * pageSize;
        let end = start + pageSize -1;
        //app.currentState.visibility.newsCurrentPage
        //article = articles(x)[1]

                // begin news item -->
        if(articles.length > 1){
          // return
          for (i=start; i <= end; i++){
              article = articles[i][1];
              newsFeed = newsFeed + 
                      `<div class="item-news">
                          <div class="col-thumb">
                              <img src='` + article.imageurl + `'>
                          </div>
                          <div class="col-body">
                              <div class="news-data">
                                  <div class="news-source">`
                                    + article.source_info.name +
                                  `</div>
                                  <div class="news-date">`
                                    + app.getLocalDate(article.published_on) +
                                  `</div>
                              </div>
                              <h3>
                              <a target="_blank" href='`+article.url+"' rel=\"nofollow\">"+article.title+`</a>
                              </h3>
                              <div class="news-body">`+app.filterArticle(article.body)+`</div>
                          </div>
                      </div>`;
                      //<a href='#' rel="nofollow" onclick="openArticle('`+article.url+`'); return false;">`+article.title+`</a>
          }
        }
{/* <div class="news-footer">
    <div class="news-tags" id="news_0">
      Categories:`+ article.categories +
    `</div>
</div> */}
                // end news item -->
            
                newsFeed = newsFeed  + `</div>
    <!--End Newsfeeds Panels -->`;
  // add paging
  let paging = "<div class=\"newsnav\">";
  if(app.data.currentState.visibility.newsCurrentPage > 0){
    paging += "<a onClick=\"makeNewsFeeds('down')\"><< Newer </a>";
  }
  if( (app.data.currentState.visibility.newsCurrentPage+1) * 6 < articles.length ){
    paging += "<a onClick=\"makeNewsFeeds('up')\"> Older >></a>";
  }
  paging += "</div>";
  newsFeed += paging;
}catch(e){ console.log(e); }

if( firstNewsLoad == false ){
  let scr = 150 * scrolldirection;
  // $("#news_overview").animate({left: -scr + '%'},500, function(){
  //   $("#news_overview").html(newsFeed) 
  //   $("#news_overview").animate({left:scr + '%'},0)
  //   $(".feed_loading").hide()
  //   $("#news_overview").animate({left: '0%'},500)
  // })
  $("#news_overview").fadeOut(200,function(){
    $("#news_overview").html(newsFeed); 
    $(".feed_loading").hide();
    $("#news_overview").fadeIn(200);
  });
}else{
  $("#news_overview").html(newsFeed);
}
 
  if(articles && articles.length > 1){ 
    $(".feed_loading").hide();
    firstNewsLoad = false;
   }
}
window.makeNewsFeeds = makeNewsFeeds;

function openArticle(link){
  let w = $("#feed-panel1").width();
  let h = $("#feed-panel1").height();
  let iframe = `
    <a href="#" onclick="makeNewsFeeds(); return false">Back to Articles</a>
    <iframe src="`+link+"\" style=\"width:"+w+"px;height:"+h+`px"></iframe>
  `;
  $("#feed-panel1").replaceWith(iframe);
}

function getNewsFeeds(){
  try {  
      let tmp = news[0].feedData.Data;
      let result = objectToArray(tmp);
      return result; //.slice(0,this.data.currentState.newsFeedsToShow)
  }catch(e){ 
     return [];
  }
};
window["getNewsFeeds"] = getNewsFeeds;
