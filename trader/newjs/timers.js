
var timersEnabled = true;
//timed objects start intervals
var msNow = new Date().getTime(); 
var fiveSecondInterval = 5;
var fifteenSecondInterval = 15;
var sixtySecondInterval = 60;
var twoMinuteInterval = 120;
var thirtyMinuteInterval = 1800;
var sixtyMinuteInterval = 3600;

var startNow = msNow - 59000;  //59 second offset (start in one second)
var start15 = msNow - 45000;  //start in 15 seconds
var start30 = msNow-30000;  //30 second offset from msNow
var start45 = msNow - 15000;  //start in 45 seconds

function startTimers(){
  startTimerIntervals();
}
window.startTimers = startTimers;

// timer function variables
var intFunctions = [];
var masterInterval;
var MasterIntervalTime = 1000;  //timer frequency to check sub-objects if it's time to execute them
var timeSpacing = 500;  //how much time between calls

function initTimeIntervals(){
 
  //timer function objects
  var get_movers = requestMoversData;
  var get_Holdings = requestHoldings;
  var get_Orders = requestOrders;
  var get_poloTicker = updatePoloTicker;
  var set_accountHistory = setAccountHistory;
  var get_CoinMarketCapTicker = getCoinMarketCapTicker;
  var get_hourlyTasks = hourlyTasks;

  var _reloadiFrames = reloadiFrames;

  //push the objects into the array
  //movers
  intFunctions.push({ name: "getPoloMovers", object: () => { get_movers(exchangeEnum.polo); }, interval: sixtySecondInterval * 5, last:  sixtySecondInterval * 4.9}); //********** build Polo MoversData (1 minute interval) */
  intFunctions.push({ name: "getBinanceMovers", object: () => { get_movers(exchangeEnum.binance); }, interval: sixtySecondInterval * 5, last:  sixtySecondInterval * 4.8});  //********** build Binance MoversData (1 minute interval) */

  //misc
  intFunctions.push({ name: "runHourlyTasks", object: () => { get_hourlyTasks(); }, interval: sixtyMinuteInterval, last: msNow}); //********** run hourly tasks */
  intFunctions.push({ name: "reloadiFrames", object: () => { _reloadiFrames(); }, interval: thirtyMinuteInterval, last: msNow}); //********** refresh charts etc */
  if(app.getExchangesEnabled()) {intFunctions.push({ name: "setAccountHistory", object: () => { set_accountHistory(); }, interval: twoMinuteInterval, last: msNow}); } //********** refresh charts etc */

  //Coin Market Cap Data
  intFunctions.push({ name: "getCoinMarketCapTicker", object: () => { get_CoinMarketCapTicker(); }, interval: thirtyMinuteInterval, last: msNow}); //********** refresh Coin Market Cap Ticker */
  //tickers
  if(app.getExchangeEnabled(exchangeEnum.polo)){intFunctions.push({ name: "getPoloTicker", object: () => { get_poloTicker(); }, interval: fiveSecondInterval, last: startNow}); }
  
  //holdings
  if(app.getExchangeEnabled(exchangeEnum.polo)){intFunctions.push( {name: "getPoloHoldings", object: () => { get_Holdings(exchangeEnum.polo); }, interval: sixtySecondInterval, last: startNow }); }
  if(app.getExchangeEnabled(exchangeEnum.binance)){intFunctions.push( {name: "getBinanceHoldings", object: () => { get_Holdings(exchangeEnum.binance); }, interval: sixtySecondInterval, last: startNow }); }
  
  //orders
  if(app.getExchangeEnabled(exchangeEnum.polo)){intFunctions.push( {name: "getPoloOrders", object: () => { get_Orders(exchangeEnum.polo); },interval: twoMinuteInterval, last: startNow });   }
  if(app.getExchangeEnabled(exchangeEnum.binance)){intFunctions.push( {name: "getBinanceOrders", object: () => { get_Orders(exchangeEnum.binance); },interval: twoMinuteInterval, last: startNow });   }
  
  //run immediately
  setTimeout(() => { //one second
    // get_movers(exchangeEnum.polo) 
    // get_movers(exchangeEnum.binance) 
    get_Holdings(exchangeEnum.polo); 
    get_Holdings(exchangeEnum.binance); 
    get_Orders(exchangeEnum.polo);
    get_Orders(exchangeEnum.binance);
  }, 1000);

  setTimeout(() => {
    getCoinMarketCapTicker();
  }, 6000);

  setTimeout(() => {
    hourlyTasks(true);
  }, 15000);


}
window.initTimeIntervals = initTimeIntervals;

/** create and start the array of interval function calls */
function startTimerIntervals() {

  initTimeIntervals();
  
  var startIntFunction = () => {
      masterInterval = setInterval(() => {  //1 second interval master function
          var delay = 0;
          intFunctions.forEach((execObject, index) => {
              var msNow = new Date().getTime();                                                //get current time in ms
              if (!execObject.last || execObject.last + (execObject.interval * 1000) < msNow) { // check if last + interval < now
                  execObject.last = msNow;                                                     //set object last to current time
                  delay = (index + 1) * timeSpacing;                                             //set start delay for spacing calls
                  setTimeout(execObject.object, delay);                                        //set an interval to run the function object
              }
          });
      }, MasterIntervalTime);
  };
  startIntFunction();

}  // e
window.startTimerIntervals = startTimerIntervals;

function reloadiFrames() {
  $("#tv-trade_basic iframe").each(function() {
    $(this).attr("src", $(this).attr("src"));
  });
  console.log("Refreshed Charts");
}
window.reloadiFrames = reloadiFrames;

function setAccountHistory(force) {
  //check if there is a BTC value
  if( app.totalBTC() <= 0 || !app.totalBTC() ){return;}
  
  //save more often if few data points.. eventually building up to 6 hours
  let interval = 60000; //6 hr / 60 = 1 minute
  let dataLength = Math.max(2,histories.accountBalanceHistory.data.length);
  if(dataLength < 5){
    interval = interval * (dataLength -1);
  }else{
    if(dataLength < 50){
      interval = interval * dataLength * 3;
    }else if(dataLength < 100){
      interval = 60000 * 60 * 6;  //6 hours
    }else{
      interval = 60000 * 60 * 12;  //12 hours
    }
  }
  // check here to verify we've gotten holdings for all enabled accounts
  if( !checkExchangesGotHoldings() ){return;}

  let bh = histories.accountBalanceHistory;
  let msNow = new Date().getTime(); 
  let age = msNow - bh.lastSave; 
  if (force || bh.data.length == 0 || msNow - bh.lastSave > interval ){ 
    try {
      bh.lastSave = msNow;
      bh.data.push(
        {
          date: msNow,
          totalBtc: app.totalBTC(),
          BTCprice: app.data._BTCPrice,  // set current BTC price
          holdings: []
        }
      );
      histories.accountBalanceHistory = bh;

      let data = histories.accountBalanceHistory.data;
      data = data.filter(e => e.BTCprice && e.BTCprice > 0);
      data = data.filter(e => e.totalBtc > 0);
      histories.accountBalanceHistory.data = data;
      setLs("histories", histories,true,true);
    }catch(e){ console.log(e); }
  }

  makeHoldingsHistoryChart();

}
window.setAccountHistory = setAccountHistory;

function checkExchangesGotHoldings(){
  let result = true;

  //binance
  if (result){
    if(app.getExchangeEnabled(exchangeEnum.binance) ){
      result = app.data.currentState.exchangeStatus.binance.gotInitialHoldings;
    }
  }
  //poloniex
  if (result){
    if( app.getExchangeEnabled(exchangeEnum.polo) ){
      result = app.data.currentState.exchangeStatus.poloniex.gotInitialHoldings;
    }
  }

  return result;
  
}
window.checkExchangesGotHoldings = checkExchangesGotHoldings;

function hourlyTasks(isImmediate) {

  // if (!isImmediate) {  // already run in 10 second timer
  //   //set account history
  //  // setAccountHistory()
  // }
  
  //re-load news feed
  loadNews();
 

}



function forceUpdateBalancesAndHoldings(){
  let forceUpdateCount=0, forceUpdateInt;
  let count = 0;
  forceUpdateInt = setInterval(() => {
    forceUpdateCount ++;
    if (forceUpdateCount >= 1){clearInterval(forceUpdateInt);}
        requestHoldings(exchangeEnum.polo); 
        requestHoldings(exchangeEnum.binance); 
        requestOrders(exchangeEnum.polo);
        requestOrders(exchangeEnum.binance);
  }, 5000);
}
window.forceUpdateBalancesAndHoldings = forceUpdateBalancesAndHoldings;

// lastSave: 99999,
// data:[
// {
//     date: 99999,
//     totalBtc: 0,
//     BTCprice: 0,
//     holdings: [
//         {
//             coin: "BTC",
//             amount: 0
//         },
//         {
//             coin: "ETH",
//             amount: 0
//         }
//     ]
// }
// function startMoversTimer(interval,offset){
//   offset = offset || 0
//   interval = interval || 60000
  
//   setTimeout(() => {
//     requestMoversData(exchangeEnum.polo)   // once on the start
//     if(timersEnabled){
//       setInterval(() => {
//         requestMoversData(exchangeEnum.polo)
//         //requestMoversData(exchangeEnum.binance)
//       }, interval);
//     }
//   }, offset);
//   setTimeout(() => {
//     requestMoversData(exchangeEnum.binance)  //  once of the start
//     if(timersEnabled){
//       setInterval(()=>{
//         requestMoversData(exchangeEnum.binance)
//       }, interval)
//     }
//   }, 10000 + offset);

// }