

function doMoversSort(index, col){
  var dataRow = $("#movers" + index);  // container for this col
//   var index = Number( $(dataRow).attr('data-index') )
  var direction = dataRow.data("sort") ? dataRow.data("sort") : "desc";
  var column = col ? $(col).index() - 1 : dataRow.data("col") || 0;
  var columnChanged = dataRow.data("col") != column;
  
  if(col){
      if(!columnChanged){ direction = direction == "desc"? "asc" : "desc"; }
      $(col).siblings().removeClass("arrow-up arrow-down");
      $(col).removeClass("arrow-up arrow-down");
      var cls = direction=="asc"?"arrow-up":"arrow-down";
      $(col).addClass(cls);
  }
  

  dataRow.data("col",column);
  dataRow.data("sort",direction);

  sortArrayofArrays( app.data.settings.exchanges[index-1].movers.pairMovers,column,direction);
}
window["doMoversSort"] = doMoversSort;  //closure-compiler export

function makeQr(){
    try {
        let set = JSON.parse(getLs("settings.exchanges"));
        let settings = [];
        for(ex of set){
          settings.push({
            name: ex.name,
            settings: ex.settings,
            // trading: ex.trading
          });
        }
        let tmptext = JSON.stringify(settings);
        tmptext = LZString.compressToBase64(tmptext);

        let qroptions = {
          // render: "image",
          size: 250,
          text: tmptext,
          eclevel: "H",
          quiet: 1,
          label: "Bullcryp",
          fontcolor: "#f80",
          radius: 0.5,
          mode: 4,
          image: $("#imgbuf")[0],
          mSize: .2
        };
        $("#qrcode").empty().qrcode(qroptions);
        //$("#qrcode").append("<div>On your mobile device, click 'Import' to scan the QR code</div>")
        setTimeout(() => {
            showMessage("Export API Keys","On your mobile device, click 'Import' to scan the QR code");
        }, 1000);
        
    }catch(e){  }
}
window["makeQr"] = makeQr;  //closure-compiler export

function pulse(img, times) {
    if(times < 1){return;}
    if(!$(img).length){
        console.log("no "+ img + " times: " + (1000-times));
        return;
    }
    // let w = $(img).width
    // let h = $(img).height
    $(img).animate({
        // width: w + 20, height: h + 20, // sets the base height and width
        opacity: .4
    }, 700, function() {
        $(img).animate({
            // width: w, height: h, // sets the alternative height and width
            opacity: 1
        }, 700, function() {
            pulse(img,times - 1);
        });
    }); 
};
window["pulse"] = pulse;  //closure-compiler export

async function startOnboard(){
    // change to settings screen
    doNav("#Settings");
    await sleep(1000);

    // show message
    let title = "Configure your Exchanges";
    let mess = "Enter your API key and secret from at least one exchange. These will be stored on your local system, they will never be stored by Bullcryp.com";
    showMessage(title,mess,"info",undefined,"top");
}
window.startOnboard = startOnboard;  //closure-compiler export

function setGreeting(message){
    let usr;
    if(!user.firstname){
        message = "Welcome ";
        usr = "";
        $("#currPortfolio").text("Your portfolio balance is $0 USD, 0 BTC");
    }else{
        usr = user.firstname;
    }

    $("#greeting").html(message +  usr);
};
window["setGreeting"] = setGreeting;  //closure-compiler export

function sortArrayofArrays(array,column,direction){
  direction = direction || "asc";

  if(direction =="asc"){
      array.sort (sortAscFunction);
  }else{
      array.sort (sortDescFunction);
  }
  function sortAscFunction(a, b) {
      if (a[column] === b[column]) {
          return 0;
      }
      else {
          return (a[column] < b[column]) ? -1 : 1;
      }
  }
  function sortDescFunction(a, b) {
      if (b[column] === a[column]) {
          return 0;
      }
      else {
          return (b[column] < a[column]) ? -1 : 1;
      }
  }
}
window["sortArrayofArrays"] = sortArrayofArrays;  //closure-compiler export

function showAlert(title, text, type, timer){

    if (typeof text === "object"){
        if (!text == [] && !text.success && !text.resultingTrades && !text.status=="REJECTED"){
            type = "error";
        }
        
    }else{
        if(text.toLowerCase().includes("error")){
            type = "error";
        }
    }
    text = JsonHuman.format(text);
    type = type || "success";

    swal.close();
    swal({
        title: title,
        html: text,
        type: type,
        showCancelButton: false,
        confirmButtonColor: "#3085d6",
        confirmButtonText: "OK",
        timer: timer,
        onOpen:makeSwalDraggable()
    }).then((result) => {

    }).catch(swal.noop);   
}
window["showAlert"] = showAlert;  //closure-compiler export

function showMessage(title,text, type, timer, pos, image, callback){
    type = type || "success";
    pos = pos || "center";
    swal({
        position: pos,
        imageUrl: image,
        title: title,
        html: text,
        type: type,
        timer: timer,
        onOpen:makeSwalDraggable()
    }).then(
          value=>{
              if(callback){
                callback();
              }
          }
    ).catch(swal.noop);
}
window["showMessage"] = showMessage;  //closure-compiler export

async function getUserInput(title,obj){
    const item = await swal({
        title: title,
        input: "text",
        inputValue: "",
        showCancelButton: true,
        // inputValidator: (value) => {
        //   return !value && 'You need to write something!'
        // }
      }).catch(swal.noop);

      if(item){
        if (typeof obj == "string") {
            $(obj).val(item.value);
        }else if(typeof obj == "function"){
            obj(item.value);
        }
    }
}
window["getUserInput"] = getUserInput;  //closure-compiler export

function doOkCancel(title, text, confirmText,cancelText, type, callback){

    confirmText = confirmText || "OK";
    cancelText = cancelText || "Cancel";
    type = type || "warning";

    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: confirmText,
        cancelButtonText: cancelText,
        // useRejections: true,
        onOpen:makeSwalDraggable()
      }).then(
          value => {
            if(callback){
                callback(true);
            }
          },
          dismiss => {
            if(callback){
                callback(false);
            }
          }
      );
}
window["doOkCancel"] = doOkCancel;  //closure-compiler export

function makeSwalDraggable(){
    setTimeout(function() {
        $( ".swal2-modal" ).draggable();
    }, 1000);
}
window["makeSwalDraggable"] = makeSwalDraggable;  //closure-compiler export

function flashItem(itemId, color){
    if (!color){color="red";}
    jQuery(itemId).stop(true, true).effect("highlight", { color: color }, 1000);
}
window["flashItem"] = flashItem;  //closure-compiler export

function convertPoloForTV(pair){
    let base = pair.split("_")[0];
     pair = pair.split("_")[1];

    return pair + base;
}
window["convertPoloForTV"] = convertPoloForTV;  //closure-compiler export

function setBuyPair(ex){
    
    ex = ex || app.data.currentState.clicked.exchange;
    //search options to see if pair exists
    //if not, it isn't available on that market
    let options = $("#buypair" + ex.id + " option");
    let values = options.filter(function() {
        return this.text == app.data.currentState.clicked.pair.pair;
    });

    if(values.length > 0){
        $("#buypair" + ex.id).val(app.data.currentState.clicked.pair.pair);
        $("#buypair" + ex.id).trigger("change");
        flashItem(".select2-selection","green");
        if(app.data.currentState.clicked.pair.pair == "BTCUSDT" ||
        app.data.currentState.clicked.pair.pair == "USDT_BTC"){
            app.data.currentState.userMarketToggleUSD = true;
        }
    }else{
        flashItem(".select2-selection");
        app.data.currentState.userMarketToggleUSD = app.data.currentState.userMarketPreferredToggleUSD;
    }
    
}
window["setBuyPair"] = setBuyPair;  //closure-compiler export

function makeHelpButton(tip,help,image){
    let button =  "<button id=\"tooltipbutton\" class=\"btn btn-sm btn-warning noshadow bullbutton waves-effect waves-light tooltipbutton\"";
    if(help) {tip = tip + ". click for more info..."; button = button + " onClick=\"showMessage('Bullcryp Help','" + help + "','info')\"";}
    button = button + " data-toggle=\"tooltip\" title=\"" + tip + "\">?</button>";

    return button;
}
window["makeHelpButton"] = makeHelpButton;  //closure-compiler export

// showMessage()
function setToolTips(){
    try {
        $("[data-toggle=\"tooltip\"]").tooltip();
      }catch(e){ }
      Vue.directive("tooltip", function(el, binding){
        $(el).tooltip({
                 title: binding.value,
                 placement: binding.arg,
                 trigger: "hover",
                 delay: { show: 500, hide: 0 }
             });
    });
}
window["setToolTips"] = setToolTips;  //closure-compiler export

function makeSectionsCollapsible(){
    let sc = 
    `<div class="sectionCollapse" onclick="toggleSection(this)">
       <i class="fas fa-arrow-up fa-2x"></i>
    </div>`;

    // $("#holdings_overview > div:first-child").on("click",function(e){
    //     toggleSection(e.target);
    // });
    // $("#trade_section > div:first-child").on("click",function(e){
    //     toggleSection(e.target);
    // });
    $("#holdings_overview").prepend(sc);
    $("#trade_section").prepend(sc);
    //$("#trade_charts").prepend(sc);

}
window.makeSectionsCollapsible = makeSectionsCollapsible;

function toggleSection(section){
    section = $(section).parent();
    if(section.hasClass("sectionCollapsed")){
        section.removeClass("sectionCollapsed");
    }else{
        section.addClass("sectionCollapsed");
    }
}
window.toggleSection = toggleSection;

var lastMarket;
function setTVChart(pairObj, period, advanced){

    let containerId="tv-trade_basic";
    let tabName, tabId, exists = false;

    if(advanced == undefined){
        advanced = app.data.currentState.advancedChartToggle;
    }
    
    //set frame height for basic, advanced
    if(advanced){
        $("#tv-trade_basic").css("height","80vh").css("max-height","80vh");
    }else{
        $("#tv-trade_basic").css("height","").css("max-height","");
    }

    
    // if(devmode){return}
    pairObj = pairObj || app.makePairObject();
    let symbols, Asymbol, Aperiod = "3";

    let exchange = (pairObj && pairObj.exchange) || app.data.currentState.clicked.exchange;

    if(typeof exchange != "object"){
        exchange = app.getExchangeByName(exchange);
    }
    // if(advanced){
    //     //set chart tab and container name for advanced
    //     containerId = "tv-trade_" + exchange.name + "_" + pairObj.chartSymbol;
    //     tabName = exchange.name + " | " + pairObj.chartSymbol;
    //     tabId = exchange.name + pairObj.chartSymbol;

    //     //check if tab exists, or create a new one
    //     if($("#" + containerId).length > 0){
    //         //exists
    //         exists = true;
    //         //click the tab
    //         $("#" + tabId + " a").trigger("click");

    //     }else{
    //         //doesn't exist
    //         //create the tab
    //         let newtab = "<li id="+tabId + " class='nav-item waves-effect'><a class='nav-link' title='' href='#"+ containerId + "'>" + tabName + "</a></li>";
    //         $(newtab).insertAfter("#trade_tabs ul #add_advanced");

    //         //creat the tab content holder
    //         $("#trade_home .tradingview-widget-container").append("<div id='"+containerId + "' class='tvchart'></div>");

    //         //hide the basic content holder
    //         $("#tv-trade_basic").hide();

    //         //click the new tab
    //         $("#" + tabId + " a").trigger("click");

    //     }
    // }else{
    //     //show the basic content holder
    //     // $(".tradingview-widget-container > div").hide();
    //     // $("#tv-trade_basic").show();
    //     $("#trade_tabs > ul > li:first a").trigger("click");
    //     jQuery("html,body").animate({ scrollTop: 0 }, 100);
    // }

    symbols = pairObj.chartSymbol;
    period = period || "1d";

    exchange = exchange.name.toUpperCase();
    symbols = symbols.toUpperCase();

    //check for polo pair
    if(symbols.includes("_")){
        symbols = convertPoloForTV(symbols);
    }
    //check for BTCUSD or BTCBTC and correct
    if(symbols == "BTCUSD" || symbols == "BTCBTC"){
        symbols = "BTCUSDT";
    }
    if (exchange + symbols + advanced == lastMarket){
        return;
    }else{
        lastMarket = exchange + symbols + advanced;
    }
    Asymbol = exchange + ":" + symbols;
    symbols = [exchange + ":" + symbols + "|" + period];


    containerId="tv-trade_basic"; //tmp debug

    if(!advanced){
        new TradingView.MediumWidget(
        {
            "container_id": containerId,
            "symbols": symbols,
            "greyText": "Quotes by",
            "gridLineColor": "rgba(101, 101, 101, 0)",
            "underLineColor": "rgba(252, 229, 205, 0.15)",
            "trendLineColor": "rgba(255, 152, 0, 1)",
            "width": "100%",
            "height": "540px",
            "chartOnly": false,
            "locale": "en"
        }
        );
    }else if(!exists){
        let style = "1";
        let showDates = true;
        let showSideToolbar = true;
        let allowSymbolChange = true;
        let showPopupButton = true;
        let showDetails = true;
        let autosize = true;

        new TradingView.widget(
            {
                "width": "100%",
                "height": "400px",
                "autosize": autosize,
                "symbol": Asymbol,
                "interval": Aperiod,
                "timezone": "Etc/UTC",
                "theme": "Light",
                "style": style,
                "locale": "en",
                "toolbar_bg": "#f1f3f6",
                "enable_publishing": false,
                "withdateranges": showDates,
                "hide_side_toolbar": !showSideToolbar,
                "allow_symbol_change": allowSymbolChange,
                "show_popup_button": showPopupButton,
                "details": showDetails, //chart needs to be 500px tall for this to show
                "save_image": false,
                "popup_width": "1000",
                "popup_heiht": "650",
                "container_id": containerId
            }
        );
    }

    // https://www.tradingview.com/referral-program/
    
    
}
window["setTVChart"] = setTVChart;  //closure-compiler export

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms ));
}
window["sleep"] = sleep;  //closure-compiler export

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}
window["getRandomArbitrary"] = getRandomArbitrary;  //closure-compiler export


function unixToLocalDate(time){
    return moment(time).format();  //:mm:ss
}
window["unixToLocalDate"] = unixToLocalDate;  //closure-compiler export

function unixToHumanDate(time){
    return moment(time).format("YYYY-MM-DD h:mm a");
}
window["unixToHumanDate"] = unixToHumanDate;  //closure-compiler export

function makeNewObj(obj){
    return JSON.parse(JSON.stringify(obj));
}
window["makeNewObj"] = makeNewObj;  //closure-compiler export

function rOund(value, decimals, down) {

    if(down){
        let f = Math.pow(10,decimals);
        return Math.floor(value*f)/f;
    }else{
        return Number(Math.round(value+"e"+decimals)+"e-"+decimals);
    }
}
window["rOund"] = rOund;  //closure-compiler export

function makePercentSlider(amount,coin){
    return `<output id="rangepercentin">Percent of holdings to sell: 100%</output>
            <input oninput="var amount = ` + amount + ";rangepercentin.value = 'Percent of holdings to sell: ' + rangepercentout.value + '%  -- '+ parseFloat((rangepercentout.value/100) * amount).toFixed(8) + ' ' + '"+ coin +`'"
             id="rangepercentout" class="rangepercent" type="range" min="1" max="100" value="100" step="1"><br>`;
}
window["makePercentSlider"] = makePercentSlider;  //closure-compiler export

function scientificToDecimal(num, decimals) {
    decimals = decimals || 8;
    //if the number is in scientific notation remove it
    if(/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
        var zero = "0",
            parts = String(num).toLowerCase().split("e"), //split into coeff and exponent
            e = parts.pop(),//store the exponential part
            l = Math.abs(e), //get the number of zeros
            sign = e/l,
            coeff_array = parts[0].split(".");
        if(sign === -1) {
            var decpart = (new Array(l).join(zero) + coeff_array.join(""));
            if(decpart.length > decimals){
                decpart = decpart.substring(0,decimals + 1); //+1 too long
                var lastNum = Math.round(parseFloat(decpart.slice(-2))/10);  //get last 2, and round to 1
                if(lastNum>9){lastNum = 9;}
                decpart = decpart.substring(0,decpart.length-2) + lastNum.toString();
            }
            num = zero + "." + decpart;
        }
        else {
            var dec = coeff_array[1];
            if(dec) l = l - dec.length;
            num = coeff_array.join("") + new Array(l+1).join(zero);
        }
    }else{
        num = parseFloat(num).toFixed(decimals);
    }
    
    return num;
};
window["scientificToDecimal"] = scientificToDecimal;  //closure-compiler export

// function generateColorPalett(R,G,B,levels)
// {
  
//   var str="";
//   for(var i=0;i<levels;i++)
//   {
//     r+=33;
//     g+=33;
//     b+=33;
//     str+="<div class='swatch' style='background-color:rgb("+r+","+g+","+b+")'></div>";
//   }
//   document.querySelector("#op").innerHTML = str;
//   console.log(str);
// }