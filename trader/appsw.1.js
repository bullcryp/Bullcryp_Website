console.log("Bullcryp SW updated");

const version = 2.05;
const t1day = 24 * 60 * 60;
const t7day = 7 * 24 * 60 * 60;
const t30day = 30 * 24 * 60 * 60;
const t150day = 150 * 24 * 60 * 60;

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js");

workbox.setConfig({
  debug: false
});

self.addEventListener("install", e => {
  self.skipWaiting();
});

const bgSyncPlugin = new workbox.backgroundSync.Plugin("bullcrypqueue", {
  maxRetentionTime: 24 * 60 // Retry for max of 24 Hours
});

workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);

const matchNews = ({url,event}) => {
  return url.pathname.includes("/data/news/feeds") ||
  url.origin.includes("cryptocompare");
};
const matchBase = ({url,event}) => {
  return url.pathname.includes("/s/roboto/") || url.pathname.includes("fontawesome");
};
const matchcmc = ({url,event}) => {
  //console.log("cmcMatch pathname: " + url.pathname + " = " + url.pathname.includes("static/img/coins/16x16"))
  return url.pathname.includes("static/img/coins/16x16");
};

const matchjs = ({url,event}) => {
  return url.pathname.includes(".js") &&
  !url.pathname.includes("googletagmanager") &&
  !url.pathname.includes(".json") &&
  !url.pathname.includes("trader_sandbox") &&
  !url.pathname.includes("google-analytics");
};

workbox.routing.registerRoute(
  matchNews,
  // Use cache but update in the background ASAP
  workbox.strategies.cacheFirst({
    // Use a custom cache name
    cacheName: "bullcryp-news-cache",
    plugins: [
      new workbox.expiration.Plugin({
        //cache 2000 images max
        maxEntries: 1000,
        //max age 30 mins
        maxAgeSeconds: t1day / 2,
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      })
    ]
  })
);

workbox.routing.registerRoute(
  // Cache image files
  matchcmc,
  // Use cache but update in the background ASAP
  workbox.strategies.cacheFirst({
    // Use a custom cache name
    cacheName: "cmc-image-cache",
    plugins: [
      new workbox.expiration.Plugin({
        //cache 2000 images max
        maxEntries: 3000,
        //max age 150 days
        maxAgeSeconds: t150day,
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      })
    ]
  })
);

workbox.routing.registerRoute(
  // Cache image files
  /.*\.(?:png|jpg|jpeg|svg|gif)/,
  // Use cache but update in the background ASAP
  workbox.strategies.staleWhileRevalidate({
    // Use a custom cache name
    cacheName: "image-cache",
    plugins: [
      new workbox.expiration.Plugin({
        //cache 1000 images max
        maxEntries: 1000,
        //max age 30 days
        maxAgeSeconds: t30day,
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      })
    ]
  })
);

workbox.routing.registerRoute(
  matchjs, //new RegExp(".*\.js"),
  workbox.strategies.staleWhileRevalidate({
    cacheName: "js-cache",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        //cache 1000 images max
        maxEntries: 1000,
        //max age 30 days
        maxAgeSeconds: t30day,
        purgeOnQuotaError: true,
      }),
      bgSyncPlugin
    ]
  })
);

workbox.routing.registerRoute(
  // Cache CSS files
  /.*\.css/,
  // Use cache but update in the background ASAP
  workbox.strategies.staleWhileRevalidate({
    // Use a custom cache name
    cacheName: "css-cache",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        //cache 1000 images max
        maxEntries: 1000,
        //max age 30 days
        maxAgeSeconds: t30day,
        purgeOnQuotaError: true,
      })
    ]
  })
);

workbox.routing.registerRoute(
  matchBase,
  // Use cache but update in the background ASAP
  workbox.strategies.staleWhileRevalidate({
    // Use a custom cache name
    cacheName: "bullcryp-main-cache",
    plugins: [
      new workbox.expiration.Plugin({
        //cache 2000 images max
        maxEntries: 100,
        //max age 3 days
        maxAgeSeconds: t30day,
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      })
    ]
  })
);









