




function showData(data) {
        //flash the data received indicator
            flash.show(0).delay(10).hide(0, ()=>{
                if (flash.queue("fx").length > 75){
                    flash.clearQueue();
                }
            });
}

function makeSwalDraggable(){
    setTimeout(function() {
        $( ".swal2-modal" ).draggable()
    }, 1000);
}

function showAlert(title, text, type, timer){

    if (typeof text === 'object'){
        if (!text.success && !text.resultingTrades){
            type = "error"
        }
        text = JsonHuman.format(text)
    }else{
        if(text.includes("error")){
            type = "error"
        }
    }
    type = type || "success"

    swal.close()
      swal({
        title: title,
        html: text,
        type: type,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK',
        timer: timer
      }).then((result) => {
          location.href = settings.oauthProfile.logoutLink
      }).catch(swal.noop)   
}

function getPairPrice(pair){
    return poloPairs.data.pairHistories.find(x => x.pair == pair).data[0].ls
}
function getPairLowAsk(pair){
    itemIndex = polo_O.orderBook.books.findIndex(x=>x.pair==pair)
    return  polo_O.orderBook.books[itemIndex].asks[0][0]
}
function getPairHighBid(pair){
    itemIndex = polo_O.orderBook.books.findIndex(x=>x.pair==pair)
    return  polo_O.orderBook.books[itemIndex].bids[0][0]
}
function getOrdersforCoin(pair){  /** TODO */
    var obj = accountModel.data.orders.filter(function(arr){
        return arr[0] == pair
    })
    //got the order object
    if(obj.length > 0){

    }else{
        return
    }
}
function getCoinAvailableAmount(coin){
    var obj = accountModel.data.holdings.filter(function(arr){
        return arr[0] == coin
    })
    var amount
    if (obj.length == 0){
        amount = 0
    }else{
        amount = obj[0][1].available
    }
    return amount
}
var nnOnce;
function nOnce(){
    if(!nnOnce){
        nnOnce = Date.now();
    }
    nnOnce += 1;
    return nnOnce;
}

function doEvery(times){  //return if random int matches - 
    if(times <= 1){
        return true
    }
    return (getRandomInt(1,times) == times-1)
}

function getTradeStreamPairs(){
    return [pairsModel.data.pairs[0].name,pairsModel.data.pairs[1].name,pairsModel.data.pairs[2].name]
}
function getPoloName(pair){
    var a = pair.split("_");
    return a[1] + a[0];
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function sortArrayofArrays(array,column,direction){
    direction = direction || "asc"

    if(direction =="asc"){
        array.sort (sortAscFunction)
    }else{
        array.sort (sortDescFunction)
    }
    function sortAscFunction(a, b) {
        if (a[column] === b[column]) {
            return 0;
        }
        else {
            return (a[column] < b[column]) ? -1 : 1;
        }
    }
    function sortDescFunction(a, b) {
        if (b[column] === a[column]) {
            return 0;
        }
        else {
            return (b[column] < a[column]) ? -1 : 1;
        }
    }
}

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function scientificToDecimal(num, decimals) {
    //if the number is in scientific notation remove it
    if(/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
        var zero = '0',
            parts = String(num).toLowerCase().split('e'), //split into coeff and exponent
            e = parts.pop(),//store the exponential part
            l = Math.abs(e), //get the number of zeros
            sign = e/l,
            coeff_array = parts[0].split('.');
        if(sign === -1) {
            var decpart = (new Array(l).join(zero) + coeff_array.join(''))
            if(decpart.length > decimals){
                decpart = decpart.substring(0,decimals + 1) //+1 too long
                var lastNum = Math.round(parseFloat(decpart.slice(-2))/10)  //get last 2, and round to 1
                if(lastNum>9){lastNum = 9}
                decpart = decpart.substring(0,decpart.length-2) + lastNum.toString()
            }
            num = zero + '.' + decpart
        }
        else {
            var dec = coeff_array[1];
            if(dec) l = l - dec.length;
            num = coeff_array.join('') + new Array(l+1).join(zero);
        }
    }else{
        num = parseFloat(num).toFixed(decimals)
    }
    
    return num;
};

//Highlight method ******************************************
function flashPairItem(divId, count, gain, origColor) {
    var elem = "#" + divId + " table tr:nth-child(" + count + ") td:nth-child(2)";
    var color = gain ? "green" : "red";
    origColor = origColor || "transparent"

    // jQuery(elem).stop(true, true).effect("highlight", { color: color }, 1000);  //stop(true,true).
    // var current = jQuery(elem).css( 'background-color' );
    jQuery(elem).stop(true,true).animate( { backgroundColor: color }, 200 );
    jQuery(elem).animate( { backgroundColor: origColor }, 500 );

    elem = "#" + divId + " table tr:nth-child(" + count + ") td:nth-child(3)";
    jQuery(elem).css('background-color', color)
}
function flashItem(itemId, color){
    if (!color){color="red"}
    jQuery(itemId).stop(true, true).effect("highlight", { color: color }, 1000);
}

function makeRangeBar(model, data) {
    model["range24hr"] = ((data[pairModelEnum.last] - data[pairModelEnum.low24hr]) * 100) / (data[pairModelEnum.high24hr] - data[pairModelEnum.low24hr]);
    model["range24hr"] = model["range24hr"].toFixed(1)
    var div = "#" + model["pair"] + " table tbody tr td .progress div"
    var s = "progress-bar-success";
    var d = "progress-bar-danger";
    var w = "progress-bar-warning";
    if (model["range24hr"] < 50 && model["range24hr"] > 25) {
        jQuery(div).addClass(w).removeClass(s).removeClass(d);
    } else if (model["range24hr"] < 25) {
        jQuery(div).addClass(d).removeClass(w).removeClass(s);
    } else {
        jQuery(div).addClass(s).removeClass(d).removeClass(w);
    }
}
function playSound(sound){
    if (sound == "buy"){sound = "notification-up.wav"}
    else
    if (sound == "sell"){sound = "down-chime.wav"}
    else
    {sound = "beep.wav"}
    // "../sound/beep.wav"
    console.log("playing sound ../polo/sound/" + sound)
    var audio = new Audio("../polo/sound/" + sound);
    try {
        audio.play();
    }catch(e){ console.log(e) }
    
    
}

// var beep = (sound)=> {
//     if (!sound){sound = "../sound/beep.wav"}
//     var snd = new Audio("../sound/" + sound)
//     snd.play();
// }

// var interval = false;
// function trendBeep(sound){
//     var count = 0;
//     var times = 2;
//     if(interval){
//         return
//     }else{
//         interval = setInterval(()=>{
//             beep(sound);
//             count ++
//             if (count >= times){
//                 clearInterval(interval)
//                 interval=false
//             }
//         }, 2000)
//     }
// }


function logMessage(message){

    message = message.replace(/"/g, "'")
    
    
    accountModel.data.userData.console.push(moment().format("llll") + ": " + message)
    //TODO fix saveuserdata functions
    renderConsole()
    // sendToServer("saveUser",accountModel.data.userData,true) 
    var textarea = document.getElementById('console')
    textarea.scrollTop = textarea.scrollHeight
}
function renderConsole(){
    var newline = String.fromCharCode(13, 10);
    var consoleMessage = ""
    accountModel.data.userData.console.forEach(function(value){
        consoleMessage += value + newline
    })
    var textArea = document.getElementById('console')
    textArea.value = consoleMessage
    textArea.scrollTop = textArea.scrollHeight;
}
function percentIncrease(first, second) {
    return ((second - first) / first * 100);
}

function setUpdates(state){
    window.holdupdates = state;
    if(state=true){
        setTimeout(()=>{
            window.holdupdates = false;
        }, 7500)
    }
}

//case insensitive contains
$.expr[':'].icontains = $.expr.createPseudo(function (text) {
        return function (e) {
                return $(e).text().toUpperCase().indexOf(text.toUpperCase()) >= 0;
        };
});

// http://codepen.io/tofanelli/pen/waadRY

// var snd = new Audio("../sound/beep.wav")
    // "data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
// snd.volume = .5

function roughSizeOfObject( object ) {
    var objectList = [];
    var recurse = function( value ) {
        var bytes = 0;
 
        if ( typeof value === 'boolean' ) {
            bytes = 4;
        } else if ( typeof value === 'string' ) {
            bytes = value.length * 2;
        } else if ( typeof value === 'number' ) {
            bytes = 8;
        } else if (typeof value === 'object'
                 && objectList.indexOf( value ) === -1) {
            objectList[ objectList.length ] = value;
            for( i in value ) {
                bytes+= 8; // assumed existence overhead
                bytes+= recurse( value[i] )
            }
        }
        return bytes;
    }
    return recurse( object );
}

function numberWithCommas(x){
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

var timeEvent_start;
function timEvent(start){
    var timeStamp = Date.now()/1000;
    if (start){
        timeEvent_start = timeStamp
    }else{
        console.log(timeStamp - timeEvent_start)
    }
}
function secsToTimeString(seconds){
    var numdays = Math.floor(seconds / 86400);
    var numhours = Math.floor((seconds % 86400) / 3600);
    var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
    //var numseconds = ((seconds % 86400) % 3600) % 60;
    var days = numdays > 0?numdays + " days ":""
    var hours = numhours + (numhours > 1?" hours ":" hour ")
    var minutes = numminutes + " minutes"
    return days + hours + minutes;
}

// LZW-compress a string
function lzw_encode(s) {
    var dict = {};
    var data = (s + "").split("");
    var out = [];
    var currChar;
    var phrase = data[0];
    var code = 256;
    for (var i=1; i<data.length; i++) {
        currChar=data[i];
        if (dict[phrase + currChar] != null) {
            phrase += currChar;
        }
        else {
            out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
            dict[phrase + currChar] = code;
            code++;
            phrase=currChar;
        }
    }
    out.push(phrase.length > 1 ? dict[phrase] : phrase.charCodeAt(0));
    for (var i=0; i<out.length; i++) {
        out[i] = String.fromCharCode(out[i]);
    }
    return out.join("");
}

// Decompress an LZW-encoded string
function lzw_decode(s) {
    var dict = {};
    var data = (s + "").split("");
    var currChar = data[0];
    var oldPhrase = currChar;
    var out = [currChar];
    var code = 256;
    var phrase;
    for (var i=1; i<data.length; i++) {
        var currCode = data[i].charCodeAt(0);
        if (currCode < 256) {
            phrase = data[i];
        }
        else {
           phrase = dict[currCode] ? dict[currCode] : (oldPhrase + currChar);
        }
        out.push(phrase);
        currChar = phrase.charAt(0);
        dict[code] = oldPhrase + currChar;
        code++;
        oldPhrase = phrase;
    }
    return out.join("");
}

function doMoversSort(col){
    var parent = $("#moversWrap > .moversrow:first-child")
    var direction = parent.data('sort') ? parent.data('sort') : "desc"
    var column = col ? $(col).index() - 1 : parent.data('col') || 0
    var columnChanged = parent.data('col') != column
    
    if(col){
        if(!columnChanged){ direction = direction == "desc"? "asc" : "desc" }
        $(col).siblings().removeClass("arrow-up arrow-down");
        $(col).removeClass("arrow-up arrow-down")
        var cls = direction=="asc"?"arrow-up":"arrow-down"
        $(col).addClass(cls)
    }
    

    parent.data('col',column)
    parent.data('sort',direction)

    sortArrayofArrays( moversModel.data.pairMovers,column,direction)
}
function orderDivsInParent(Parent,target,storageName, childClass){
    try {
        var divList = getLs(storageName,true).data.reverse()
        if(target==""){target=Parent}
        var child
        for(div of divList){
            child = Parent.find("#" + div + "." + childClass)
            target.prepend(child)
        }
    }catch(e){  }
    // child = Parent.find(".buy")
    // if(child){
    //     child.parent().prepend(child)
    // }
}
function sizeHoldings(shrink){
    if(shrink){
        $("#account .panel-body > div").addClass("shrunk")
    }else{
        $("#account .panel-body > div").removeClass("shrunk")
    }
}
function sizeSelectedHolding(holding){
    sizeHoldings(true) //shrink all
    $(holding).removeClass("shrunk")
}
function holdingsClicks(){
    //set holdings and orders click events
    $(".balances.hoverzoom").each(function(index,item){
        $(item).off()
        $(item).click(function(e){

            //highlight this holding
            $(this).css({'border': '1px solid blue'}).removeClass("shadow")

            //if this is a human clicked event...
            if(e.originalEvent !== undefined){
                if( $(this).hasClass("shrunk")  ){
                    sizeSelectedHolding(this)
                }
                doPairClick(this)
            }

        })
        $(item).dblclick(function(e){
            // $("#collapseone").slideToggle()
        })
    })
    $(".orders.hoverzoom").each(function(index,item){
        $(item).off()
        $(item).click(function(e){

            //highlight this order
            $(this).css({'border': '1px solid blue'}).removeClass("shadow")

            //if this is a human clicked event...
            if(e.originalEvent !== undefined){
                if( $(this).hasClass("shrunk")  ){
                    sizeSelectedHolding(this)
                }
                doPairClick(this)
            }
        })
        $(item).dblclick(function(e){
            
        })
    })
    
}
function setBuyPair(pair){
    $('#buypair').val(pair);
    $('#buypair').trigger("chosen:updated");
    polo_O.selectedPair = pair
    account.buyPairChange()
    flashItem("#buy", "lightgreen")
}
function setPairMonitor(pair){
    //determine which monitor to highlight / change
    var pairNum, poloPair
    if( $("#selpair1").val() == pair){
        pairNum = "1"
        pair1Model.pair = pair
        poloPair = getPoloName(pair)
    }else{
        pairNum = "2"
        pair2Model.pair = pair
        poloPair = getPoloName(pair)
    }
    // }
    // }else{
    //     pairNum = "3"
    //     pair3Model.pair = pair
    // }

    var $selPair = $("#selpair" + pairNum)
    var $pair = $(".pair" + pairNum)
    //set coin monitor selector and selected pair
    $selPair.val(pair);
    $selPair.trigger("chosen:updated")

    $pair.attr("id", pair)

    //deselect all pairMonitors
    $(".pair").css({'border': 'none'}).addClass("shadow")

    //select chosen pair monitor
    $pair.css({'border': '1px solid blue'}).removeClass("shadow")

    //set pairmonitors chart
    $('#pg1').attr('src', 'pair_graph.html?pair=' + poloPair  + '&small=true&int=' + $("#charttime").val());

    // if(ths != $(".pair" + pairNum)[0]){
    //     $(".pair" + pairNum).trigger("click")
    // }
}
function doPairClick(ths,pair){
    //set the clicked pair, and coin
    var $this = $(ths)
    pair = pair || $this.attr("id").replace("o_","")
    var orderPair = "o_" + pair
    var coin = pair.split("_")[1]

    //deselect all holdings
    $(".balances.hoverzoom").not($this).css({'border': 'none'}).addClass("shadow")

    //deselect all orders
    $(".orders.hoverzoom").not($this).css({'border': 'none'}).addClass("shadow")
    
    //deselect coin monitor pair
    $(".pair").not($this).css({'border': 'none'}).removeClass("shadow")
    

    //set value to current pair
    var value = pair //this.attributes[0].nodeValue
    if(value == "BTC_BTC" || value == "BTC_USDT"){
        value = "USDT_BTC"
    }
    
    //set buy pair control
    setBuyPair( pair )

    //set pair monitor
    setPairMonitor( pair )
    
    // $("#selpair" + pairNum).val(value);
    // $("#selpair" + pairNum).trigger("chosen:updated")
    // pairM.pair = value

    // $(".pair" + pairNum).attr("id", value)
    
    
    // if(ths != $(".pair" + pairNum)[0]){
    //     $(".pair" + pairNum).trigger("click")
    // }
    //click each object, if not native
    if(ths != $("#account").find("#"+pair)[0]){ //if not same holding
        $("#account").find("#"+pair).trigger("click")
    }
    if(ths != $("#account").find("#o_"+pair)[0]){ //if not same order
        $("#account").find("#o_"+pair).trigger("click")
    }

    //set vue model pair
    polo_O.selectedPair = pair

    //reset orderbook to 5 items
    // sizeObook(false)

    //send to server which orderbook to refresh
    //sendToServer('account.getorderbook',pair)

    //set trends search
    $('#trends-widget-1').attr('src',
        `https://trends.google.com:443/trends/embed/explore/TIMESERIES?req=%7B%22comparisonItem%22%3A%5B%7B%22keyword%22%3A%22`+ coin +`%22%2C%22geo%22%3A%22%22%2C%22time%22%3A%22now%207-d%22%7D%5D%2C%22category%22%3A0%2C%22property%22%3A%22%22%7D&tz=480&eq=date%3Dnow%207-d%26q%3D` + coin
    )

    //upodate the charts
    setCharts(10)

    //temporary measure until I figure out why the 
    //holdings ID's are changing sometimes after clicking
    fixHoldings()

}
function fixHoldings(){
    setTimeout(() => {
        $(".balances.hoverzoom").each(function(index,item){
            
            if(this.attributes[0].nodeValue != this.id){
                this.id = this.attributes[0].nodeValue
            }
        })
    }, 1000);
}
// function openSettings(){
//     var page = "settings.html";
//     window.blockDialog = true
//     window.dialog1 = $('<div>').dialog({ 
//         autoOpen: false,
//         modal:true,
//         height: 400,
//         width: 450,
//         closeText: "",
//         dialogClass: "settingsModal",
//         show: { effect: "blind", duration: 500 },
//         hide: { effect: "explode", duration: 500 },
//         beforeClose: function( event, ui ) {
//             // if( window.blockDialog){
//             //     alert("Are you sure?")
//             //     return false;
//             // }
//         },
//         buttons: {
//             "Save": function() {
//                 // window.blockDialog = false
//                 $( this ).dialog( "close" );
//                 deleteElement('.settingsModal')
//             },
//             Cancel: function() {
//                 // $( this ).dialog( "close" );
//                 window.blockDialog = false
//                 deleteElement('.settingsModal')
//             }
//         }
//     });
//     window.dialog1.load(page).dialog('open');
// }
function deleteElement(el){
    setTimeout(() => {
        $(el).remove()
    }, 1000);
}

function loadSettings(){  
    //load last known settings from local storage, ahead of getting them live
    if(accountModel){
        accountModel.data.holdings = getLs("balances",true)
        if( accountModel.data.holdings && accountModel.data.holdings.length > 0) {
            accountModel.data.holdings.forEach(function(e){
                if(!e[1].buyat){
                    e[1].buyat = {pair:e[0],price:0}
                }
            })
        }
        accountModel.data.orders = getLs("orders",true)
        if( accountModel.data.orders && accountModel.data.orders.length > 0){
            accountModel.data.orders.forEach(function(e){
                if(!e[1].buyat){
                    e[1].buyat = {pair:e[0],price:0}
                }
            })
        }
        // accountModel.data.userData.userName = defaultUsername
        // sendToServer("loadUser", accountModel.data.userData.userName)
    }

    
}
function setCharts(delay){
    delayAdd = delay?500:2500
    delay = delay || 1000
    
    /** load the tradingview charts, after 10 - 20 seconds of page loading */
    //top chart
    setTVChart(null,getPoloName(polo_O.selectedPair),"1m")
    // if( $('#pg1').attr('src')!= 'pair_graph.html?pair=' + getPoloName(polo_O.selectedPair) + '&small=true&int=' + $("#charttime").val() ){
    //     setTimeout(()=>{ 
    //             $('#pg1').attr('src', 'pair_graph.html?pair=' + getPoloName(polo_O.selectedPair) + '&small=true&int=' + $("#charttime").val() ) 
    //     },delay)
    // }
    //panel charts
    if( $('#if1').attr('src')!= 'pair_graph.html?pair=' + getPoloName(polo_O.selectedPair) + '&int=' + $("#charttime").val() ){
        setTimeout(()=>{ 
                $('#if1').attr('src', 'pair_graph.html?pair=' + getPoloName(polo_O.selectedPair) + '&int=' + $("#charttime").val() ) 
        },0) //delay + delayAdd)
        delayAdd += delayAdd
    }
    // if( $('#if2').attr('src')!= 'pair_graph.html?pair=' + getPoloName(pair2Model.pair) + '&int=' + $("#charttime").val() ){
    //     setTimeout(()=>{ 
    //             $('#if2').attr('src', 'pair_graph.html?pair=' + getPoloName(pair2Model.pair) + '&int=' + $("#charttime").val() ) 
    //     },delay + delayAdd)
    // }
}
var glb_exchange, glb_symbols, glb_period
function setTVChart(exchange,symbols, period){
    exchange = exchange || "POLONIEX:"
    symbols = symbols || "BTCUSDT"
    period = period || "1m"

    if(glb_exchange == exchange && glb_symbols == symbols && glb_period == period){return}
    glb_exchange = exchange
    glb_symbols = symbols
    glb_period = period

    $('.tvsym').text(symbols + " " + period)
    var symPer = exchange + symbols + "|" + period
    new TradingView.MediumWidget(
    {
        "container_id": "tv-medium-widget1",
        "symbols": [
            symPer
        ],
        "greyText": "Quotes by",
        "gridLineColor": "#e9e9ea",
        "fontColor": "#83888D",
        "underLineColor": "#dbeffb",
        "trendLineColor": "#4bafe9",
        "width": "100%",
        "height": "100%",
        "locale": "en",
        "chartOnly": true
    });
}
function setIntervalAndCall(method, interval, immediate, once){
    if(immediate){method()}
    if(!once){
        setInterval(function(){
            method()
        },interval)
    }else{
        setTimeout(function(){
            method()
        },interval)
    }
}


