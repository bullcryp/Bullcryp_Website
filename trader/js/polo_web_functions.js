
/** polo API functions */

const PUBLIC_API_URL = "https://poloniex.com/public?command="
const PRIVATE_API_URL = 'https://bullcryp.com/api/polo/'//'https://poloniex.com/tradingApi';
const USER_AGENT = 'bullcryp-trader 1.1'

var request = window.returnExports


function buildCommandObject(command, params, nokey){
  var obj = {}
  params = params || {}
  obj.command = command
  obj.data = {}
  obj.data.params = JSON.stringify(params)
  if(!nokey){
    obj.data.key = settings.exchanges[settings.system.currentExchange.name].key,
    obj.data.secret = settings.exchanges[settings.system.currentExchange.name].secret
  }
  try {
    obj.data.token = getLs("token") //settings.oauthProfile.token.access_token.substring( last 20 )
  }catch(e){ }
  
  // Object.assign(obj.data, data)
  return obj
}

/** AJAX API Call */
function agetAPIDataAsync(command,callback, timeout ) {
  
  //callback signature: callback(strErr, Data)

  //defaults to JSON datatype, 2 second timeout
  var URL, dataToSend, method, private

  //if command is an object, this is a private API call
  private = typeof command === 'object'
  
  if(private){
    URL = PRIVATE_API_URL + command.command
    dataToSend =  command.data
    method = 'POST'
  }else{
    URL = PUBLIC_API_URL + command
    method = 'GET'
  }

  timeout = timeout || settings.system.ajaxTimeout
  
  try {
      $.ajax({
          url: URL,
          method: method,
          dataType: 'text',
          timeout: timeout,
          data: dataToSend,
          success: function(result){
            if (callback){
              if(result.includes('error')){  //TODO check for 'unauthorized'
                callback(result, null)
              }else{
                try {
                  callback(null, JSON.parse(result))
                }catch(e){ 
                  console.error(e) 
                }
              }
          }
          },
          error: function(xhr,status,error){
              // callback(null,error)
          }
      })
  }catch(e){ console.log(e) }
}



function sendBuy(currency, rate, amount, fillOrKill, immediateOrCancel,postOnly){
  var params = {
    currency: currency,
    rate: rate,
    amount: amount,
    fillOrKill: fillOrKill,
    immediateOrCancel: immediateOrCancel,
    postOnly: postOnly
  }
  var command = buildCommandObject("buy",params )
  agetAPIDataAsync(command,function(err,data){
    if (err){
      data = err
    }
    poloLog(data)
    showAlert('Buy Order Confirmation',data) 
    forceUpdateBalancesAndHoldings()
  })
}
function sendSell(currency, rate, amount, fillOrKill, immediateOrCancel,postOnly){
  var params = {
    currency: currency,
    rate: rate,
    amount: amount,
    fillOrKill: fillOrKill,
    immediateOrCancel: immediateOrCancel,
    postOnly: postOnly
  }
  var command = buildCommandObject("sell",params )
  agetAPIDataAsync(command,function(err,data){
    if (err){
      data = err
    }
      poloLog(data)
      showAlert('Sell Order Confirmation',data) 
      forceUpdateBalancesAndHoldings()
  })
}
function sendMoveOrder(orderNum, price, amount, immediateOrCancel, postOnly){
  var params = {
    orderNumber: orderNum,
    rate: price,
    amount: amount,
    postOnly: postOnly,
    immediateOrCancel: immediateOrCancel
  }
  var command = buildCommandObject("moveOrder", params)
  agetAPIDataAsync(command,function(err,data){
    if(err){
      data = err
    }
    poloLog(data)
    showAlert('Modify Order Confirmation',data) 
    forceUpdateBalancesAndHoldings()
  })
}
function sendCancelOrder(orderNum){
  var params = {
    orderNumber: orderNum
  }
  var command = buildCommandObject("cancelOrder", params)
  agetAPIDataAsync(command,function(err,data){
    if(err){
      data = err
    }
    poloLog(data)
    showAlert('Cancel Order Confirmation',data) 
    forceUpdateBalancesAndHoldings()
  })
}
var forceUpdateCount=0, forceUpdateInt
function forceUpdateBalancesAndHoldings(){
  count = 0
  forceUpdateInt = setInterval(() => {
    forceUpdateCount ++
    if (forceUpdateCount > 4){clearInterval(forceUpdateInt)}
    requestBalances()
    requestOrders()
  }, 2000);
  
}


function poloLog(data){
  console.log(data)
  //TODO log every poloniex response
}


//timer functions - called regularly
function startTimers(){
  //one second delay to allow Vue object to complete initialization
  setTimeout(() => {
    startTickerTimer()
    startOrderBookTimer()
    startMoversTimer(null,10000)
    startBalancesTimer()
    startOrdersTimer()
  }, 1000);
}

/** public API timed functions */

function startTickerTimer(interval,offset){
  offset = offset || 0
  interval = interval || 3000
  setTimeout(() => {
    setInterval(() => {
      updateTicker()
    }, interval);
  }, offset);
}
function updateTicker(force){

  if(!force && (typeof initComplete == "undefined" || initComplete == false) ){return}
    
  agetAPIDataAsync('returnTicker', function (err,ticker){
  //getTickerAPI(function (ticker) {

    if(err){return}

    //flash the little green LED
    showData();


    //ticker = ticker
    var pairHistory
    var same
    var percentChange = 0
    var tradeVolume = 0
    var count = 0

    for (var pair in ticker) {
      count += 1
      pairHistory = null
      if (!ticker.hasOwnProperty(pair)) continue;
      var item = ticker[pair]
      try {
        pairHistory = poloPairs.data.pairHistories.find(x => x.pair == pair); // see if object with this pairname already exists
      } catch (e) { }

      if (pairHistory) {
        if (pairHistory.data[0].ls == item.last && pairHistory.data[0].cov == item.quoteVolume) {
          //price and volume are the same, don't record
          // rgs[1] = rgs[1];
        } else {
          // if price not the same as last, calculate percent Increase
          same = (item.last == pairHistory.data[0].ls)
          if (!same) {
            percentChange = percentIncrease(pairHistory.data[0].ls, item.last)
          }
          tradeVolume = pairHistory.data[0].cuv - item.baseVolume
          pairHistory.data.unshift({
            ts: Date.now() / 1000,  //.. and append to the beginning of data
            ls: item.last,
            la: item.lowestAsk,
            hb: item.highestBid,
            pc: percentChange, //args[4] 
            cuv: item.baseVolume,
            cov: item.quoteVolume,
            tv: tradeVolume
          }
          );
          // trim the length to the max length
          if (pairHistory.data.length > settings.system.dataPairHistoryLength) {
            pairHistory.data.length = settings.system.dataPairHistoryLength
          }
        }
      } else {
        poloPairs.data.pairHistories.push(  //create a new pairHistories child object with this pair name, add to pairHistories
          {
            pair: pair,
            data: [{
              ts: Date.now() / 1000,
              ls: item.last,
              la: item.lowestAsk,
              hb: item.highestBid,
              pc: item.percentChange,
              cuv: item.baseVolume,
              cov: item.quoteVolume
            }],
            totalVolume: 0,
          }
        )
      }

      if (window.holdupdates == true) {

      } else {
        /**
         * ['BTC_BBR','0.00069501','0.00074346','0.00069501','-0.00742634','8.63286802','11983.47150109',0,'0.00107920','0.00045422']

        Appropriate labels for these data are, in order: currencyPair, last, lowestAsk, highestBid, percentChange, baseVolume, quoteVolume, isFrozen, 24hrHigh, 24hrLow
         */
        var args = [
          pair, parseFloat(item.last), parseFloat(item.lowestAsk), parseFloat(item.highestBid), parseFloat(item.percentChange), parseFloat(item.baseVolume), parseFloat(item.quoteVolume), parseInt(item.isFrozen), parseFloat(item.low24hr), parseFloat(item.high24hr)
        ]
        if (pair == poloPairs.data.pairs[0].name) {
          // showData();
          var p = poloPairs.data.pairs[0];
          updateModel(pair1Model, args, p.name, p.digits);
        }
        if (pair == poloPairs.data.pairs[1].name) {
          // showData();
          var p = poloPairs.data.pairs[1];
          updateModel(pair2Model, args, p.name, p.digits);
        }
        // if (pair == poloPairs.data.pairs[2].name) {
        //         // showData();
        //         var p = poloPairs.data.pairs[2];
        //         updateModel(pair3Model, args, p.name, p.digits);
        // }
        if (!poloPairs.data.pairSets.includes(pair)) {
          poloPairs.data.pairSets.push(pair)
          poloPairs.data.pairSets.sort()
          // pair3Model.pairs = poloPairs.data.pairSets
          setLs("pairSets", poloPairs.data.pairSets, true)
          // sendToServer("pairsets", poloPairs.data.pairSets)
        }
      }
    }
  }) //end getTickerAPI call
}
function startOrderBookTimer(offset){
  updateOrderBook("all",10000,5)  //fill all orderbooks once
  offset = offset || 3000
  var interval = 5000
  setTimeout(() => {
    setInterval(() => {
      if(settings.currentState.orderbookVisible){
        updateOrderBook(polo_O.selectedPair)
      }
    }, interval);
  }, offset);
}
function updateOrderBook(pair,timeout,depth) {
  timeout = timeout || 2000
  depth = depth || settings.system.orderbookLength
  
  function getOrderBookAPI(pair, depth, timeout,callback){
    pair = pair || "all"
    pair = '&currencyPair=' + pair 
  
    depth = depth || settings.system.orderbookLength
    depth = '&depth=' + depth
    
    agetAPIDataAsync('returnOrderBook' + pair + depth,callback,timeout)
  }
  
  getOrderBookAPI(pair,depth,timeout,function (err, data) {

    if(err){return}

    pair = pair || polo_O.selectedPair

    //get the current state of the order book
    console.log("got orderBook")

    //clear all highlighted items
    //TODO - check if highlighted item matches our current bid -- .data('orderPrice')
    setTimeout(() => {
      var $cell = $('.orders-table tr td:nth-child(2)')
      $cell.css("font-weight", "normal").css("background-color", "").css("cursor", "default")
      $cell.off()
    }, 499);


    // poloO.data.orderBook = data
    if (data.hasOwnProperty("asks")){
      //this is a single orderbook
      var itemIndex = polo_O.orderBook.books.findIndex(x => x.pair == pair)
      if (itemIndex < 0) {
        polo_O.orderBook.books.push(
          {
            pair: pair,
            asks: data.asks,
            bids: data.bids
          }
        )
      } else {
        polo_O.orderBook.books[itemIndex].asks = data.asks
        polo_O.orderBook.books[itemIndex].bids = data.bids
      }
    }else{
      //this is multiple order books
      //find the pair that gets this data
      for (var pr in data) {
        if (!data.hasOwnProperty(pr)) continue;
        var itemIndex = polo_O.orderBook.books.findIndex(x => x.pair == pr)
        
        if (itemIndex < 0) {
          polo_O.orderBook.books.push(
            {
              pair: pr,
              asks: data[pr].asks,
              bids: data[pr].bids
            }
          )
        } else {
          polo_O.orderBook.books[itemIndex].asks = data[pr].asks
          polo_O.orderBook.books[itemIndex].bids = data[pr].bids
        }
      }
    }

    //highlight any current orders in the lists
    setTimeout(() => {

      $('.orders-table:first-child td:nth-child(2)').each(function () {
        $this = $(this)
        findOrderRate($this, function (ths, orderNum, rate, amount, pair) {
          if (ths) {
            ths.css("font-weight", "bold").css("background-color", "#cbe2d8").css("cursor", "pointer")
            ths.data('orderPrice',rate)
            ths.click(function (e) {
              account.moveOrder(orderNum, rate, amount, pair)
            })
          }
        })
      });
      $('.orders-table:nth-child(2) td:nth-child(2)').each(function () {
        $this = $(this)
        findOrderRate($this, function (ths, orderNum, rate, amount, pair) {
          if (ths) {
            ths.css("font-weight", "bold").css("background-color", "#cbe2d8").css("cursor", "pointer")
            ths.data('orderPrice',rate)
            ths.click(function (e) {
              account.moveOrder(orderNum, rate, amount, pair)
            })
          }
        })
      });
      // subItem.orderNumber, subItem.rate,subItem.amount, item[0]
      function findOrderRate(ths, callback) {
        var rate = ths.html()
        var found = false
        var pair
        for (order of accountModel.data.orders) {
          if (polo_O.selectedPair == order[0]) {
            for (orderp of order[1]) {
              if (orderp.rate == rate) {
                found = true
                if (callback) {
                  callback(ths, orderp.orderNumber, orderp.rate, orderp.amount, order[0])
                }

              }
            }
          }
        }
        if (!found && callback) {
          callback(false) //found?"bold":"normal"
        } else {
          return found
        }

        // accountModel.data.orders[0][1][0].rate
        // return 
      }

    }, 500);

  })
}

/** private API timed functions */
//request the market change data once per minute
function startMoversTimer(interval,offset){
  offset = offset || 0
  interval = interval || 60000
  requestMoversData() // once on the start
  setTimeout(() => {
    requestMoversData()
    setInterval(() => {
      requestMoversData()
    }, interval);
  }, offset);
}
function requestMoversData(){
  var command = buildCommandObject("movers",undefined,true)
  agetAPIDataAsync(command,function(err,data){
    
    if(err){return}

    updateMovers(data) 
  })
}
function updateMovers(data){
  try {
    moversModel.data.pairMovers = data
    doMoversSort()
  }catch(e){ console.log(e) }
}

function startBalancesTimer(interval,offset){
  offset = offset || 0
  interval = interval || 10000
  requestBalances()
  setTimeout(() => {
    setInterval(() => {
      requestBalances()
    }, interval);
  }, offset);
}
function requestBalances(){
  var command = buildCommandObject("balances" )
  agetAPIDataAsync(command,function(err,data){
    
    if(err){return}

    updateBalances( objectToArray(data) )
  })
}
function updateBalances(data){
  var gotErr
  if(accountModel){
    if(settings.system.holdingsCount != data.length){
        setTimeout(() => {
            holdingsClicks()
        }, 5000);
        settings.system.holdingsCount = data.length
    }
    data.forEach(function(item, index){
        item[1]['buyat']= {pair:item[0],price:0}
        try{
            var prev_buyat = accountModel.data.holdings.filter((i)=>{return i[1].buyat.pair==item[0]})
            if(prev_buyat.length){
                item[1].buyat.price=prev_buyat[0][1].buyat.price
            }
        }catch(e){
          // gotErr = true
        }
    })

    if(gotErr){
      return
    }
    /** move BTC to first spot */
    if(data.find(x => x[0] == "BTC")){
        data.unshift(                      // add to the front of the array
            data.splice(                     // the result of deleting items
                data.findIndex(                // starting with the index where
                x => x[0] === 'BTC'), // the name is BTC
            1)[0]                             // and continuing for one item
        )
    }
    /** move USDT holdings to front of the array */
    if(data.find(x => x[0] == "USDT")){
        data.unshift(                      // add to the front of the array
            data.splice(                     // the result of deleting items
                data.findIndex(                // starting with the index where
                x => x[0] === 'USDT'), // the name is USDT
            1)[0]                             // and continuing for one item
        )
    }
    

    accountModel.data.holdings = data;
    setLs("balances",data,true)
    console.log("got balances")
  }
}


function startOrdersTimer(interval,offset){
  offset = offset || 5000
  interval = interval || 60000
  requestOrders()
  setTimeout(() => {
    setInterval(() => {
      requestOrders()
    }, interval);
  }, offset);
}
function requestOrders(callback){
  var command = buildCommandObject("orders" )
  agetAPIDataAsync(command,function(err,data){

    if(err){return}

    updateOrders( objectToArray(data) )
  })
}
function updateOrders(data){
  if(accountModel){
    if(settings.system.ordersCount != data.length){
        setTimeout(() => {
            holdingsClicks()
        }, 5000);
        settings.system.ordersCount = data.length
    }
    data.forEach(function(item, index){
        item[1]['buyat']= {pair:item[0],price:0}
        try{
            var prev_buyat = accountModel.data.orders.filter((i)=>{return i[1].buyat.pair==item[0]})
            if(prev_buyat.length){
                item[1].buyat.price=prev_buyat[0][1].buyat.price
            }
        }catch(e){}
    })
    accountModel.data.orders = data;
    setLs("orders",data,true)
    console.log("got orders")
  }
}



/** helper functions */

function objectToArray(data){
  var holdings = []; //var allpairs = [];
    Object.keys(data).forEach(function (key) {
        //if (data[key]['btcValue'] > 0 || data[key]['available'] > 0 || data[key]['onOrders'] > 0) {
            holdings.push([key, data[key]]);
        //}
    });
    return holdings
}

function nonce(length) {
  var last 
  , repeat = 0

  length = length || 19

    var now = Math.pow(10, 7) * new Date()

    if (now == last) {
      repeat++
    } else {
      repeat = 0
      last = now
    }

    var s = (now + repeat).toString()
    return +s.substr(s.length - length)
  
}