//VUE models ****************************************************
var pairsModel;
var poloPairs;
var pair1Model, pair1Vue;
var pair2Model, pair2Vue;
// var pair3Model, pair3Vue;
var accountModel, account;

var polo_O = {}

// Vue.config.productionTip = false;

function makePairModel() {
    if(getLs("pairs",true)==null){
        var pairs = '[{"name":"USDT_BTC","digits":8,"lastUpdated":1522707100181},{"name":"BTC_XMR","digits":8,"lastUpdated":1522707100181},{"name":"BTC_LTC","digits":8,"lastUpdated":1522707100181}]'
        setLs("pairs",pairs,false)
    }
    pairsModel = {
        data: {
            pairs: getLs("pairs",true),
            pairSets: [],
            pairHistories: [{ pair: "USDT_BTC", data: [{ ts: 0, ls: 0, la: 0, hb: 0, pc: 0, cuv: 0, cov: 0 }], totalVolume: 0, changed: true }],
            // pairTrades: [{pairModel:0, data:[]},{pairModel:1, data:[]},{pairModel:2, data:[]} ],
            // pairHistoryBar: [ [],[],[] ]
            
        }
    }
    poloPairs = new Vue({
        el: '#poloP',
        data: pairsModel,
        computed: {

        }
    })
    setTimeout(() => {
        //request initial movers data
        // sendToServer("movers")
        moversModel = {
            data: {
                filterPair: "",
                pairMovers: [["Loading..."]]
                
            }
        }
        moversVue = new Vue({
            el: '#movers',
            data: moversModel,
            computed:{
                filteredMovers: ()=>{
                    var fp = moversModel.data.filterPair
                    var pm = moversModel.data.pairMovers
                    try {
                        if(fp == "" || fp == undefined){
                            return pm
                        }else{
                            var fpm = pm.filter(function(i){
                                return i[0].toUpperCase().indexOf(fp.toUpperCase()) >= 0
                                // return i[0] == fp
                            })
                            return fpm
                        }
                    }catch(e){ }
                }
            }
        })
    }, 2000);
    
    loadPairData()
    if (getLs("pairSets", true) != null) {
        pairsModel.data.pairSets = getLs("pairSets", true)
        // sendToServer("pairsets", pairsModel.data.pairSets)
    }
}
function makeAccountingModels() {

    

    polo_O = {
        orderBook: { books:[{pair:"USDT_BTC", bids: [], asks: [] }], got: "notyet" },
        selectedPair: "USDT_BTC",
        bidAskRatio: 0,
        spread: 0
    }
    poloO = new Vue({
        el: '#poloO',
        data: polo_O,
        methods: {

        },
        computed: {
            cumSumOrderBook() {
                try {
                    var temp = cumulativePairOrderVolume(this.orderBook, this.selectedPair)
                    if (temp == undefined || temp.bids == undefined) { return [] }
                    this.bidAskRatio = temp.bids[temp.bids.length - 1][2] / temp.asks[temp.asks.length - 1][2]
                    this.spread = temp.spread
                    return temp
                } catch (e) { }
            },
            getBTCToPrice() {

            },
            getPriceToBTC() {

            }
        },
    })
}



function makePairViews() {
    pair1Model = makeData(poloPairs.data.pairs[0].name);
    pair1Vue = new Vue({
        el: '#' + pair1Model.pair,
        data: pair1Model,
        watch: {
            pair: function () {
                $("#" + poloPairs.data.pairs[0].name).attr("id", this.pair)
                poloPairs.data.pairs[0].name = this.pair;
                // $('#if1').attr('src', 'pair_graph.html?pair=' + this.polo_name  + '&int=' + $("#charttime").val());
                setLs("pairs", poloPairs.data.pairs, true)
                resetPair(this)
                // jQuery("#historybarpair1").html("Loading..")
            }
        },
        computed: {
            pairSets: function () {
                if (poloPairs.data.pairSets) {
                    var pairSets = []
                    for(pair of poloPairs.data.pairSets){

                    }
                    return poloPairs.data.pairSets.map(function (x) {
                        return { text: x, value: x }
                    })
                }
            },
            base: function () {
                return this.pair.split("_")[0]
            },
            coin: function () {
                return this.pair.split("_")[1]
            },
            // polo_name: function () {
            //     var a = this.pair.split("_");
            //     return a[1] + a[0];
            // }
        },
        methods: {
            changed: function () {
                setUpdates(false)
            },
            focus: function () {
                setUpdates(true)
            },
            blur: function () {
                setUpdates(false)
            }
        }
    });

    pair2Model = makeData(poloPairs.data.pairs[1].name);
    pair2Vue = new Vue({
        el: '#' + pair2Model.pair,
        data: pair2Model,
        watch: {
            pair: function () {
                $("#" + poloPairs.data.pairs[1].name).attr("id", this.pair)
                poloPairs.data.pairs[1].name = this.pair;
                // $('#pg1').attr('src', 'pair_graph.html?pair=' + this.polo_name  + '&small=true&int=' + $("#charttime").val());
                setLs("pairs", poloPairs.data.pairs, true)
                resetPair(this)
                // jQuery("#historybarpair2").html("Loading..")
            }
        },
        computed: {
            pairSets: function () {
                if (poloPairs.data.pairSets) {
                    return poloPairs.data.pairSets.map(function (x) {
                        return { text: x, value: x }
                    })
                }
            },
            base: function () {
                return this.pair.split("_")[0]
            },
            coin: function () {
                return this.pair.split("_")[1]
            },
            // polo_name: function () {
            //     var a = this.pair.split("_");
            //     return a[1] + a[0];
            // }
        },
        methods: {
            changed: function () {
                setUpdates(false)
            },
            focus: function () {
                setUpdates(true)
            },
            blur: function () {
                setUpdates(false)
            }
        }
    });

    // pair3Model = makeData(poloPairs.data.pairs[2].name);
    // pair3Vue = new Vue({
    //     el: '#' + pair3Model.pair,
    //     data: pair3Model,
    //     watch: {
    //         pair: function () {
    //             $("#" + poloPairs.data.pairs[2].name).attr("id", this.pair)
    //             poloPairs.data.pairs[2].name = this.pair;
    //             $('#if3').attr('src', 'pair_graph.html?pair=' + this.polo_name  + '&int=' + $("#charttime").val());
    //             setLs("pairs", poloPairs.data.pairs, true)
    //             resetPair(this)
    //             jQuery("#historybarpair3").html("Loading..")
    //         }
    //     },
    //     computed: {
    //         pairSets: function () {
    //             if (poloPairs.data.pairSets) {
    //                 return poloPairs.data.pairSets.map(function (x) {
    //                     return { text: x, value: x }
    //                 })
    //             }
    //         },
    //         base: function () {
    //             return this.pair.split("_")[0]
    //         },
    //         coin: function () {
    //             return this.pair.split("_")[1]
    //         },
    //         polo_name: function () {
    //             var a = this.pair.split("_");
    //             return a[1] + a[0];
    //         }
    //     },
    //     methods: {
            
    //         changed: function () {
    //             setUpdates(false)
    //         },
    //         focus: function () {
    //             setUpdates(true)
    //         },
    //         blur: function () {
    //             setUpdates(false)
    //         }
    //     }
    // });

    //loads the initial pair display data from poloPairs
    resetPair(pair1Vue)
    resetPair(pair2Vue)
    // resetPair(pair3Vue)


    accountModel = {
        data: {
            holdings: [["btc", { "available": "0", "btcValue": "0", "buyat": { pair: "USDT_BTC", price: 0 }, "onOrders": "0" }]],
            orders: [],
            margin: { "totalValue": "0", "pl": "0", "lendingFees": "0", "netValue": "0", "totalBorrowedValue": "0", "currentMargin": "0" },
            marginValue: 0,
            buyPair: "USDT_BTC",
            buyPairPrice: 0,
            userData: {
                userName: "",
                poloKeys: [],
                alerts: [],
                console: []
                
            }
        }
    }
    account = new Vue({
        el: '#account',
        data: accountModel,
        watch: {
        },
        computed: {
            totalBTC: function () {
                if (!this.data) { return }
                var totalBTC = 0;
                this.data.holdings.forEach(function (el) {
                    if (el[0] == "USDT") {
                        totalBTC += account.totalUSD / account.BTCPrice
                    } else {
                        totalBTC += parseFloat(el[1].btcValue);
                    }
                });
                //console.log(this.BTCtoUSD(totalBTC))
                return totalBTC.toFixed(6);

            },
            totalUSD: function () {
                var USDT = 0;
                if (!this.data) { return }
                this.data.holdings.forEach(function (obj) {
                    if (obj[0] == 'USDT') {
                        USDT += parseFloat(obj[1]['available']) + parseFloat(obj[1]['onOrders']);
                    }
                });
                return USDT.toFixed(2);
            },
            USDvalue: function () {
                var USDvalue = 0;
                if (!this.data) { return }
                USDvalue = parseFloat(this.BTCPrice * this.totalBTC) //+ parseFloat(this.totalUSD);
                document.title = "$" + USDvalue.toFixed(2);
                return USDvalue.toFixed(2);
            },
            BTCPrice: function () {
                try {
                    return poloPairs.data.pairHistories[0].data[0].ls;
                } catch (e) {
                    return 0
                }
            },
            pairSets: function () {
                if (poloPairs.data.pairSets) {
                    return poloPairs.data.pairSets.map(function (x) {
                        return { text: x, value: x }
                    })
                }
            },
        },
        methods: {
            buyPairChange(selector) {
                this.data.buyPair = $("#buypair").val()
                this.data.buyPairPrice = this.getCoinPrice(this.data.buyPair)
            },
            buyCoin(amt) {
                var pair = this.data.buyPair
                var base = this.getBaseCoin(pair)
                var coin = this.getPairCoin(pair)
                var price = this.getCoinPrice(pair).replace(/0+$/, "")
                // price = price.replace(/0+$/, "")
                var available = getCoinAvailableAmount(base)
                if(amt){
                    var buyAmt = parseInt($(amt).val(), 10)  //get USD buy amount
                    if(base != "USDT") { buyAmt = round(buyAmt/account.BTCPrice,8) } //convert it to BTC
                    buyAmt = buyAmt || available  //if buyAmt is 0, use available
                    if(available < buyAmt){ buyAmt = available} //if available is less than buyAmt, use available
                    available = buyAmt
                }
                if (available <= 0) {
                    swal({ title: "No " + base + " available", text: "You do not have any " + base + " available to buy " + coin, type: "info", timer: 3000 }).catch(swal.noop)
                } else {
                    swal({
                        title: 'Buy ' + coin,
                        input: 'text',
                        showCancelButton: true,
                        inputValue: price,
                        html: 'Enter the amoint of ' + base + ' to spend, and the price for ' + coin + '<br><br><label for="swal-amount">Amount ' + base + '</label><input id="swal-amount" class="swal2-input" value="' + available + '"><label>Price</label>',
                        inputValidator: function (value) {
                            return new Promise(function (resolve, reject) {
                                if (jQuery.isNumeric(value) && jQuery.isNumeric($('#swal-amount').val())) {
                                    //TODO calculate amount of coin from amount of base
                                    var amount = round(jQuery("#swal-amount").val() / value, 8)
                                    
                                    //data = JSON.stringify(data)
                                    // sendToServer("buy", data)
                                    sendBuy(pair,value,amount,0,0,0)
                                    resolve()
                                } else {
                                    reject('Please re-check your Amount and Price')
                                }
                            })
                        },
                        onOpen:makeSwalDraggable()
                    }).then(function (result) {
                        swal({
                            type: 'success',
                            html: 'Buy order request for: ' + pair + " at bid of: " + result + " sent to server",
                            timer: 1500
                        }).catch(swal.noop)
                    }).catch(swal.noop)
                }
            },
            buyCoinNow(amt) {
                var pair = this.data.buyPair
                var base = this.getBaseCoin(pair)
                var coin = this.getPairCoin(pair)
                var price = this.getCoinPrice(pair)
                var available = getCoinAvailableAmount(base)
                if(amt){
                    var buyAmt = parseInt($(amt).val(), 10)  //get USD buy amount
                    if(base != "USDT") { buyAmt = round(buyAmt/account.BTCPrice,8) } //convert it to BTC
                    buyAmt = buyAmt || available  //if buyAmt is 0, use available
                    if(available < buyAmt){ buyAmt = available} //if available is less than buyAmt, use available
                    available = buyAmt
                }
                if (available <= 0) {
                    swal({ title: "No " + base + " available", text: "You do not have any " + base + " available to buy " + coin, type: "info", timer: 3000 }).catch(swal.noop)
                } else {
                    swal({
                        title: 'Buy ' + coin,
                        input: 'text',
                        showCancelButton: true,
                        inputValue: available,
                        html: 'This will buy ' + coin + ' now at the current market asks<br>Enter the amount of ' + base + ' to spend<br><br><label>Amount</label>',
                        inputValidator: function (value) {
                            return new Promise(function (resolve, reject) {
                                if (jQuery.isNumeric(value)) {
                                    //TODO calculate amount of coin from amount of base
                                    var amount = round( (value / price) * .99, 8)
                                    // var data = {
                                    //     pair: pair,
                                    //     price: price,
                                    //     amount: amount
                                    // }
                                    // data = JSON.stringify(data)
                                    price = price * settings.system.buyNowFactor
                                    sendBuy(pair,price,amount,0,0,0)
                                    resolve()
                                } else {
                                    reject('Please re-check your Amount and Price')
                                }
                            })
                        }, 
                        onOpen:makeSwalDraggable()
                    }).then(function (result) {
                        swal({
                            type: 'success',
                            html: 'Buy order request for: ' + pair + " at bid of: " + result + " sent to server",
                            timer: 1500
                        }).catch(swal.noop)
                    }).catch(swal.noop)
                }
            },
            dumpCoin(coin, amount, curPrice) {
                var base = coin == "BTC" ? "USDT" : "BTC"
                var pairSet = base + "_" + coin
                var available = getCoinAvailableAmount(coin)
                if (available <= 0) {
                    swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 2000 }).catch(swal.noop)
                } else {
                    swal({
                        title: "Are you sure you want to dump all " + coin + " to " + base + "?",
                        text: "This will sell the coin starting at the highest bids price. Be sure to check the orders. You will not be able to undo once you agree",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, dump it!",
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                setTimeout(function () {
                                    resolve();
                                }, 1000);
                            })
                        }, 
                        onOpen:makeSwalDraggable()
                    }).then(
                    function () {
                        swal({ title: "Sending...", text: "Dump order being sent to server", type: "info", timer: 1500 }).catch(swal.noop)
                        // var dumpOrder = {
                        //     pair: pairSet,
                        //     amount: amount,
                        //     price: curPrice
                        // }
                        sendSell(pairSet,curPrice * settings.system.dumpFactor,amount,0,0,0)
                        /** TODO if oder exists, change the order rather then putting in new order */
                        // sendToServer("dumpcoin", JSON.stringify(dumpOrder))
                    },
                    function (dismiss) { }
                    ).catch(swal.noop)
                }
            },
            moveOrder(orderId, price, amount, pair) {
                var base = coin == "BTC" ? "USDT" : "BTC"
                var coin = this.getPairCoin(pair)
                var pairSet = base + "_" + coin
                swal({
                    title: 'Change Bid for ' + pair,
                    input: 'text',
                    showCancelButton: true,
                    inputValue: price.replace(/0+$/, ""),
                    html: '<label for="swal-amount">Amount ' + base + '</label><input id="swal-amount" class="swal2-input" value="' + amount + '"><label>Price</label>',
                    inputValidator: (value) => {
                        return new Promise((resolve, reject) => {
                            if(value == 0 || value == undefined || value == ""){
                                this.cancelOrder(orderId)
                                return
                            }
                            if (jQuery.isNumeric(value)) {
                                // var data = {
                                //     orderNumber: orderId,
                                //     price: value,
                                //     amount: $('#swal-amount').val()
                                // }
                                // data = JSON.stringify(data)
                                // sendToServer("moveorder", data)
                                sendMoveOrder(orderId, value, $('#swal-amount').val(), 0,0)
                                resolve()
                            } else {
                                jQuery(".swal2-input").val(price)
                                reject('Please re-check your new price')
                            }
                        })
                    }, 
                    onOpen:makeSwalDraggable()
                }).then(function (result) {
                    swal({
                        type: 'success',
                        html: 'Change request for: ' + pair + " at bid of: " + result + " sent to server",
                        timer: 1500
                    }).catch(swal.noop)
                }).catch(swal.noop)
            },
            cancelOrder(orderID) {
                swal({
                    title: "Are you sure you want to cancel this order?",
                    text: "You will not be able to undo once you agree",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, cancel it!", 
                    onOpen:makeSwalDraggable()
                }).then(
                    function (result) {
                        swal({
                            title: "Cancelling...",
                            text: "Cancel order being sent to server",
                            type: "info",
                            timer: 1500
                        }).catch(swal.noop)
                        var btn = $("#" + orderID + ".cancelorder")
                        // sendToServer('cancelOrder', orderID)
                        sendCancelOrder(orderID)
                        flashItem("." + orderID)
                        btn.text("Cancelling..")
                        setTimeout(function () {
                            btn.text('Cancel Order')
                        }, 5000)
                    }
                    ).catch(swal.noop)
            },
            sell(coin, amount, curPrice) {
                var available = getCoinAvailableAmount(coin)
                if (available <= 0) {
                    swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 3000 }).catch(swal.noop)
                } else {
                    var base = (coin == "BTC") ? "USDT" : "BTC"
                    var pairSet = base + "_" + coin
                    var lowAsk = parseFloat(getPairLowAsk(pairSet))
                    var highBid = parseFloat(getPairHighBid(pairSet))
                    var sellPrice = round(highBid + ((lowAsk - highBid) * settings.system.sellFactor), 8)
                    // sellPrice = sellPrice.replace(/0+$/, "")
                    var percentAmount = 100
                    if (sellPrice == lowAsk) { sellPrice = highBid }
                    swal({
                        title: "Sell " + coin + " to " + base + "?",
                        // text: ,
                        html: "This will place an order just below the low ask price. While not guaranteed, this should ensure you do not pay the taker fee."
                        + "<br><br>"+makePercentSlider(amount,coin)+"<div>Low ask: " + lowAsk + "</div><div>High bid: " + highBid + "</div><br><div>Estimated bid: " + sellPrice + "</div>",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, sell it!",
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                percentAmount =  parseFloat(amount * parseFloat(jQuery(".rangepercent").val()/100)).toFixed(8)
                                setTimeout(function () {
                                    resolve();
                                }, 1000);
                            })
                        }, 
                        onOpen:makeSwalDraggable()
                    }).then(
                        function () {
                            swal({ title: "Sending...", text: "Sell order being sent to server", type: "info", timer: 1500 }).catch(swal.noop)
                            var lowAsk = parseFloat(getPairLowAsk(pairSet))
                            var highBid = parseFloat(getPairHighBid(pairSet))
                            var sellPrice = round(highBid + ((lowAsk - highBid) * .8), 8)  //calculate amount 80% between low and high
                            if (sellPrice == lowAsk) { sellPrice = highBid }
                            // var sellOrder = {
                            //     pair: pairSet,
                            //     amount: percentAmount,
                            //     price: sellPrice
                            // }
                            // sendToServer("sell", JSON.stringify(sellOrder))
                            sendSell(pairSet,sellPrice,percentAmount,0,0,1)
                        },
                        function (dismiss) { }
                    ).catch(swal.noop)
                }
            },
            sellAt(coin, amount, curPrice) {
                var base = coin == "BTC" ? "USDT" : "BTC"
                var pairSet = base + "_" + coin
                var available = getCoinAvailableAmount(coin)
                if (available <= 0) {
                    swal({ title: "No " + coin + " available", text: "Check if you have pending orders", type: "info", timer: 3000 }).catch(swal.noop)
                } else {
                    swal({
                        title: "Sell " + coin + " to " + base + "?",
                        text: "Enter your sell price",
                        html: makePercentSlider(amount,coin) + "This will place an immediate sell order for your Coin.<br><b>High bid: </b>" + getPairHighBid(pairSet) + 
                              " <b>Low Ask: </b>" + getPairLowAsk(pairSet)  + "<br><center><table style='margin-top:10px'><tr><td><label style='margin-right:5px;margin-bottom:0'>Bid: </label> </td><td id='swalbid'> " + scientificToDecimal(curPrice,8) + "</td></tr></table></center>" ,
                        input: 'range',
                        inputValue: 0, //round(curPrice, 8),
                        inputAttributes: {
                            min: -10, //round(Math.min(getPairHighBid(pairSet), curPrice) * (1 - sellAtRangeFactor), 8),
                            max: 200, //round(Math.max(getPairLowAsk(pairSet), curPrice) * (1 + sellAtRangeFactor), 8),
                            step: .5
                        },
                        type: "info",
                        showCancelButton: true,
                        //inputValue: curPrice,
                        inputValidator: (value) => {
                            return new Promise((resolve, reject) => {
                                // if (jQuery.isNumeric(value)) {
                                    var sellOrder = {
                                        pair: pairSet,
                                        amount: parseFloat(amount * parseFloat(jQuery(".rangepercent").val()/100)).toFixed(8),
                                        price: jQuery("#swalbid").text()
                                    }
                                    //sendToServer("sellat", JSON.stringify(sellOrder))
                                    
                                    sendSell(pairSet,sellOrder.price,sellOrder.amount,0,0,0)

                                    resolve()
                                // } else {
                                //     reject('Please re-check your price')
                                // }
                            })
                        }, 
                        onOpen:makeSwalDraggable()
                    }).then(
                        function (result) {
                            swal({
                                type: 'success',
                                html: 'Order request for: ' + coin + " at bid of: " + result + " sent to server",
                                timer: 1500

                            }).catch(swal.noop)
                        }
                        ).catch(swal.noop)
                    jQuery(".swal2-range input").on("input", function () {
                        var range = jQuery(".swal2-range input")
                        var percent = (range.val()/100) + 1
                        var value = scientificToDecimal(curPrice * percent,8)
                        jQuery("#swalbid").text(  value  )
                        // range.prop('max', round(val * (1 + sellAtRangeFactor), 8))
                        // range.prop('min', round(val * (1 - sellAtRangeFactor), 8))
                    })
                }
            },
            saveUser() {
                // tmp  = accountModel.data.userData.console.replace("\r\n", "**n**")
                // sendToServer("saveUser",JSON.stringify(accountModel.data.userData),true)
            },
            timeSince(start) {  //return s m h since last update
                let timeNow = Date.now()
            },
            fixPair(pair){
                if(pair == "BTC_BTC" || pair == "BTC_USDT"){
                    pair = "USDT_BTC"
                }
                return pair
            },
            getBaseCoin(pair) {
                return pair.split("_")[0]
            },
            getPairCoin(pair) {
                return pair.split("_")[1]
            },
            getCoinPrice(coin) {
                try {
                    let pair;
                    if (coin == 'USDT') {
                        return round(1 / this.BTCPrice, 8)
                    } else if (coin.includes('_')) {
                        pair = coin
                    } else if (coin != 'BTC') {
                        pair = 'BTC_' + coin
                    } else {
                        pair = 'USDT_BTC'
                    }
                    return getPairPrice(pair)
                } catch (err) {
                    return 0
                }
            },
            getCoinAmountFromBaseAmount(coin, base, amount) {
                var baseAmount = round(amount / this.getCoinPrice(coin), 8)
                return baseAmount
            },
            calcPercentIncrease(second, first) {
                var increase = round((second - first) / first * 100, 4)
                if (increase == Infinity) { increase = "-" }
                return increase;
            },
            calcMargin(curMargin) {
                this.data.marginValue = curMargin
                return curMargin

            }
        }
    });

    

}

// FUNCTIONS*******************************************************

function loadPairData() {
    // var tmp = getLs("pairHistories", true, true)
    // if (tmp != null) {
    //     pairsModel.data.pairHistories = tmp
    // }
    tmp = getLs("pairs", true, true)
    if (tmp != null && Array.isArray(tmp)) {
        pairsModel.data.pairs = tmp
        // pairsModel.data.pairs[0].name = "USDT_BTC"  //temp fix for startup with bad pair
    }
}

function makePercentSlider(amount,coin){
    return `<output id="rangepercentin">Percent of holdings to sell: 100%</output>
            <input oninput="var amount = ` + amount + `;rangepercentin.value = 'Percent of holdings to sell: ' + rangepercentout.value + '%  -- '+ parseFloat((rangepercentout.value/100) * amount).toFixed(8) + ' ' + '`+ coin +`'"
             id="rangepercentout" class="rangepercent" type="range" min="1" max="100" value="100" step="1"><br>`
}

var cumulativePairOrderVolume = function (OrderBook, pair, rows) {
    //make a copy of the orderbook, and add a 3rd element to each order array element,
    //which is cumulative volume
    try {
        //key is the pairset we are on
        if (!rows) { rows = settings.system.orderbookLength }
        var itemIndex = polo_O.orderBook.books.findIndex(x=>x.pair==pair)
        if(itemIndex < 0){ return undefined}

        var askRows = polo_O.orderBook.books[itemIndex].asks.length < rows ? polo_O.orderBook.books[itemIndex].asks.length : rows
        var bidRows = polo_O.orderBook.books[itemIndex].bids.length < rows ? polo_O.orderBook.books[itemIndex].bids.length : rows

        if (bidRows + askRows == 0) { return }
        var asks = [] //OrderBook[pair].asks.slice(0,20)
        var bids = []//OrderBook[pair].bids.slice(0,20)

        var cumA = 0, cumB = 0
        for (i = 0; i < rows; i++) {  //much more accurate than using map, as far as vue is concerned
            if (i < askRows) { asks[i] = [polo_O.orderBook.books[itemIndex].asks[i][0], polo_O.orderBook.books[itemIndex].asks[i][1], cumA += polo_O.orderBook.books[itemIndex].asks[i][1]] }
            if (i < bidRows) { bids[i] = [polo_O.orderBook.books[itemIndex].bids[i][0], polo_O.orderBook.books[itemIndex].bids[i][1], cumB += polo_O.orderBook.books[itemIndex].bids[i][1]] }
        }
        // asks.map((x)=>{x.push(cumA+=x[1])})
        // bids.map((x)=>{x.push(cumB+=x[1])})
        //add rebuilt items back into new object
        var temp = { asks: asks, bids: bids, spread: ((asks[0][0]-bids[0][0])/bids[0][0])*100 }

        return temp


    } catch (e) {
        // console.log(e)
        return {}
    }
}

var totalOrderVolume = function (summedPairOrders) {

    try {
        var totalOrderVol = {}
        try {
            var asks = summedPairOrders.asks[summedPairOrders.asks.length - 1][2]
            var bids = summedPairOrders.bids[summedPairOrders.bids.length - 1][2]

            totalOrderVol = { asks: asks.toFixed(2), bids: bids.toFixed(2) }
        } catch (e) { }

        return totalOrderVol
    } catch (e) {
        return {}
    }
}

function resetPair(model) {
    try {
        var lastPairData = poloPairs.data.pairHistories.find(x => x.pair == model.pair).data[0]
        model.low24hr = 0
        model.high24hr = 0
        model.lowestAsk = lastPairData.la
        model.highestBid = lastPairData.hb
        model.range24hr = 0
        model.percentChange = lastPairData.pc
        model.baseVolume = lastPairData.cov
        model.quoteVolume = lastPairData.cuv
        model.last = lastPairData.ls
    } catch (e) { }
    if (document.activeElement) {
        document.activeElement.blur();
    }
}

function makeData(pair) {
    //return ["pair",0,0,0,0,0,0,0,0,0,50]
    return {
        pair: pair,
        last: 0,
        lowestAsk: 0,
        highestBid: 0,
        percentChange: 0,
        baseVolume: 0,
        quoteVolume: 0,
        isFrozen: 0,
        high24hr: 0,
        low24hr: 0,
        range24hr: 50,
        WS: "nowrap", //for progressbar style
        C: "black", //for progressbar style
        PL: "20px", //for progressbar style
        // historyBarLength: 600,
        sameCount: 0
    };
}

function updateModel(model, data, divId, roundTo) {
    var count = 0;
    var flash = false;
    var value;
    var gain;

    makeRangeBar(model, data);

    for (var i in pairModelEnum) {
        if (model.hasOwnProperty(i)) {

            switch (i) {
                case "pair":
                case "pairs":
                case "range24hr":
                case "priceHistory":
                case "historyBarLength":
                case "sameCount":
                case "WS":
                case "C":
                case "PL":
                    break;
                case "percentChange":
                    value = (data[pairModelEnum[i]] * 100).toFixed(8);
                    value = value < 0 ? value : "+" + value;
                    flash = (model[i] != value)
                    gain = value > model[i];
                    model[i] = value;
                    break;
                case "last":
                    if (data[pairModelEnum.pair] == "USDT_BTC") {
                        document.title = parseFloat(data[pairModelEnum[i]]).toFixed(2)
                    }
                default:
                    value = parseFloat(data[pairModelEnum[i]]).toFixed(roundTo)
                    flash = (model[i] != value)
                    gain = value > model[i];
                    model[i] = value;
            }
            if (flash) {
                flashPairItem(divId, count, gain);
            }
            count++;
        }
    }
    // if(model.pair == "USDT_BTC"){
    // updateHistory(model)

    // }
}

