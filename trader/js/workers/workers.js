



async function generateKeyPairs (callback) {
  console.log('Generating a new keypair now.')

  // Initialize crypto webworker thread
  var cryptWorker = new Worker('js/workers/crypto_worker.js')

  // receive keypair
  var keys = await getWebWorkerResponse(cryptWorker,'generate-keys')
  keys = JSON.parse(keys)

  window.cryptokeys = {}
  window.cryptokeys = keys

  console.log ('Keypair Generated')

  if(callback){callback(keys)}
}


/** Post a message to the web worker and return a promise that will resolve with the response.  */
function getWebWorkerResponse (webworker, messageType, messagePayload) {
  return new Promise((resolve, reject) => {
    // Generate a random message id to identify the corresponding event callback
    const messageId = Math.floor(Math.random() * 100000)

    // Post the message to the webworker
    webworker.postMessage([messageType, messageId].concat(messagePayload))

    // Create a handler for the webworker message event
    const handler = function (e) {
      // Only handle messages with the matching message id
      if (e.data[0] === messageId) {
        // Remove the event listener once the listener has been called.
        e.currentTarget.removeEventListener(e.type, handler)

        // Resolve the promise with the message payload.
        resolve(e.data[1])
      }
    }

    // Assign the handler to the webworker 'message' event.
    webworker.addEventListener('message', handler)
  })
}