//import { settings } from "cluster";



$(document).ready(function () {

    // if(typeof initComplete == "undefined"){
    //     getScripts(initialize)  //load scripts, with the initialize function as the callback when done
    // }else{
    //     initialize()
    // }

    initialize()

    //reload the page every hour
    // setInterval(function(){
    //     window.location.reload(true);
    // },60*60000)
    
})

function initialize(){
    flash = jQuery(".data")
    

    //in vuemodels
    makePairModel()                

    makePageElements()

    makeAccountingModels()
    
    makePairViews()  
    
    // setUpTroll()

    //load some settings
    loadSettings()

    // setCharts()

    finalizeUI()

    startTimers()

    //crypto
    // generateKeyPairs()
    
    initComplete = true
    
}

function runLogoAnimation(){
    $('.bclogo').click(function(){
        $('.bclogo').remove()
    })
    ProgressBar.init(
          [ 'Initializing Encryption',
            'Loading Account',
            'Creating Charts',
            'Final Security Check',
            'Dashboard Initialized'
          ],
          'Dashboard Initialized',
          'progress-bar-wrapper',
          2000
        );
        
    //bullcryp logo overlay effect
    $('.bclogo > div.logo > img').fadeIn(1000)
    setTimeout(() => {
        if($('.bclogo').length < 1){return}
        $('.bclogo > div.logo > img').css('animation','none')
        $("div.loading").fadeOut(1500)
        $('.progress-bar-wrapper').fadeOut(1000)
        $(".bclogo > div.mask").fadeOut(1000,function(){
            $('.bclogo > div.logo').css('padding-top','320px')
            explodeImage(".bclogo img")
            setTimeout(() => {
                $('canvas').fadeOut(1500,function(){
                    $('.bclogo').remove()
                })
            }, 500);
        })
    }, 9000);
}
function resetImage(){
    $('.mainframe').before('<div class = "bclogo"><div class="logo"><img src="images/logo_amber.png"></div><div class="loading"><img src="images/loading_coin.gif"></div><div class="mask"></div></div>')
    $(".bclogo > div.mask").css("background-color","transparent")
    $('.bclogo > div.logo > img').css('display','inline')
}

function explodeImage(selector){
    $(selector).explode({
        maxWidth: 18,
        minWidth: 14,
        radius: 500,
        release: true,
        recycle: true,
        explodeTime: 2000,
        canvas: false,
        round: false,
        maxAngle: 360,
        gravity: -.8,
        groundDistance: 500
    });
}


function finalizeUI(){

    runLogoAnimation()

    // set frame covers for charts
    $(".framecover, .framecoverpg1").click(function(){
            clearTimeout( $(this).data('covertimer') )
            // $(this).slideToggle("slow").delay(10000).slideToggle("slow")
            if($(this).hasClass("toggled")) {
                $(this).animate({"height": "100%"}).removeClass("toggled")
                .css("background-color" ,"rgba(0,0,0,0.07)")
              } else {
                $(this).animate({"height": "17px"}).addClass("toggled")
                .css("background-color", "rgba(0,0,0,0.3)")
                $(this).data('covertimer', setTimeout(() => {
                    $(this).animate({"height": "100%"}).removeClass("toggled")
                    .css("background-color" ,"rgba(0,0,0,0.07)")
                }, 60000)  )
            }
    })

    $(".frameholder").addClass("shadow")
    $(".pair").addClass("shadow")

    $(".data_container").appendTo(".panel-title:first")

    
    // set chosen selector events
    $("#selpair1").chosen({search_contains: true, display_disabled_options:false}).change(function(){
        pair1Model.pair = $("#selpair1").val()
        setBuyPair(pair1Model.pair)
        //upodate the charts
        setCharts(10)
    })
    $("#selpair2").chosen({search_contains: true, display_disabled_options:false}).change(function(){
        pair2Model.pair = $("#selpair2").val()
        setBuyPair(pair2Model.pair)
        //upodate the charts
        setCharts(10)
    })
    // $("#selpair3").chosen({search_contains: true, display_disabled_options:false}).change(function(){
    //     pair3Model.pair = $("#selpair3").val()
    //     setBuyPair(pair3Model.pair)
    // })

    //set coin monitor click events
    $(".pair").click(function(e){
        //highlight this pairMonitor
        $(this).css({'border': '1px solid blue'}).removeClass("shadow")

        //if this is a human clicked event...
        if(e.originalEvent !== undefined){
            doPairClick(this)
        }
    })

    //click the first pair
    $('.pair:first').trigger('click')
    doPairClick($('.pair:first'))

    $('.sizeholdingsbut').click(function(e){
        return false;
    })
    $('#charttime').click(function(e){
        $(this).focus();
        return false;
    })
    $('#charttime').change(function(e){
            setCharts(10)
    })
    
    
    holdingsClicks()

    //set keyboard capture for buy keycodes
    $(document).keypress(function(event){
        if(event.shiftKey){
            if(event.charCode == 78){ //shift-n
                $(".buy button:nth-child(2)").trigger("click")
            }
            if(event.charCode == 66){ // shift-b
                $(".buy button:first").trigger("click")
            }
        }
    })
    
    setTimeout(() => {
        holdingsClicks()
        $(".balances.hoverzoom").css({'border': 'none'})

        //stop click event from propagating to the accordion header
        $(".chosen-container").on("click", function(e){
            e.stopPropagation()
        })

        $('#moversWrap').on('click','> div:not(:first-child)',function(event){
            event.preventDefault();
            var pair  = this.children[1].textContent

            flashItem($(this.children[1]),"yellow")

            doPairClick(this,pair)
            
        })

        //when scrolling thru the orderbook or movers data, stop the page from scrolling
        $('#orderbookWrap, #moversWrap').mouseenter(function(e){
            document.body.style.overflow='hidden'
            clearTimeout(growOrdersTimeout)
        }).mouseleave(function(e){
            document.body.style.overflow='auto'
            setOrderbookShrinkTimer()
        })

        //enable sorting of holdings and orders
        $("#collapsetwo .panel-body").sortable({
            containment: "parent",
            cursor: "move",
            opacity: 0.8,
            tolerance: "pointer",
            items : "div.sortable",
            connectWith: "#collapsedivider > .panel-body",
            update: function() {
                var hArray=[]
                $( "#collapsetwo .panel-body" ).children().each(function(index,item){
                    hArray.push(item.id)
                });
                setLs("holdings_order",{data:hArray},true);
            }
          })
        $("#collapsedivider > .panel-body").sortable({
            connectWith: "#collapsetwo .panel-body",
            containment: false,
            update: function() {
                var hArray=[]
                $( "#collapsedivider > .panel-body" ).children().each(function(index,item){
                    hArray.push(item.id)
                });
                setLs("holdingslt_order",{data:hArray},true);
            }
        })
        $("#collapsethree .panel-body").sortable({
            containment: "parent",
            cursor: "move",
            opacity: 0.8,
            tolerance: "pointer",
            items : "div.sortable",
            
            update: function() {
                var hArray=[]
                $( "#collapsethree .panel-body" ).children().each(function(index,item){
                    hArray.push(item.id)
                });
                setLs("orders_order",{data:hArray},true);
            }
        })

        //load the sort order of holdings and orders
        orderDivsInParent($("#collapsetwo .panel-body:first"),"","holdings_order","sortable")
        orderDivsInParent($("#collapsetwo .panel-body"),$("#collapsedivider > .panel-body"),"holdingslt_order","sortable")
        orderDivsInParent($("#collapsethree .panel-body"),"","orders_order","sortable")
        
        // setTimeout(() => {
        //     $(".balances").each(function(index){
        //         setTimeout(() => {
        //             $(this).css("opacity","1")
        //         }, index * 250);
        //     })
        //     $(".orders").each(function(index){
        //         setTimeout(() => {
        //             $(this).css("opacity","1")
        //         }, (index * 250)+100 ) ;
        //     })
        // }, 4000);

        //fix the orders panel mystery "NaN" object - tmp fix
        $("#collapsethree .panel-body")[0].lastChild.nodeValue = ""
        // $('#account .panel-body:first-child').masonry({
        //     // options
        //     itemSelector: '.balances.hoverzoom',
        //     columnWidth: 198
        //   });

        //move trends widget into proper panel
        setTimeout(() => {
            jQuery("#trends-widget-1").appendTo("#collapseeight div.panel-body")

            //set movers table click event for header sort
            $("#moversWrap > .moversrow:first-child > div:not(:first-child)").on('click',function(e){
                doMoversSort($(this))
            })
        }, 3000);
    }, 2000);

    $("#buypair").chosen({search_contains: true}).change(function(){
        account.buyPairChange()
    })
    // })
    // setTimeout(function(){
    //     var textarea = document.getElementById('console')
    //     textarea.scrollTop = textarea.scrollHeight
    // },5000)

    //settings window
    settings.loadSettings()
    $( "#settings_modal" ).draggable()
    $( "#settings_modal .modal-dialog" ).load("settings.html")
}







function getScripts(callback){
    var script_arr = [
                        "lib/vue",
                        // "lib/socket.io",
                        "lib/autobahn.min",
                        "global_constants",
                        "analysis_functions",
                        "web_functions",
                        "storage",
                        "vuemodels",
                        "sio",
                        "trollbox",
                        "autobahn_subscribe",
                        "makeHTMLElements",
                        "pkeys",
                        "linkify/linkify.min",
                        "linkify/linkify-html.min",
                        "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"
                     ]
    processScriptArray(script_arr,callback)
}

function processScriptArray(script_arr,callback){
    var scrpt;
    if(script_arr[0].includes("http")){
        scrpt = script_arr[0]
    }else{
        scrpt = "/js/" + script_arr[0] + ".js"
    }
    $.getScript(scrpt)
        .done(()=>{ 
                    $("#startup_output").val( $("#startup_output").val() + "loaded " + script_arr[0] + "\n")
                    setTimeout(function(){
                        script_arr.shift()  //remove first element from scripts array
                        if(script_arr.length){
                            processScriptArray(script_arr,callback)
                        }else{
                            // $("#startup_output").val( $("#startup_output").val() + "\nStarting Up.....")
                            callback()
                        }
                    },25)
                    
                  }
        )
        .fail(()=>{ 
                    //if script loading fails, processing array stops
                    $("#startup_output").val( $("#startup_output").val() + "failed to load " + script_arr[0] + "\n\n**Startup Halted**") 
                  })
}

