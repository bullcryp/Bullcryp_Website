



function detectDump2(history, index, largeChangeAmount, pair){
    //detect rapid down trend
    
    var count = 0;
    if(!history){ return false}

    var maxLength = history.length - index - 1;
    var length = Math.min(maxLength,detectDumpOn)
    for(i=0;i < length; i++){
        if(history[index+i].pc > -largeChangeAmount ){
            return false
        }
        count ++
        // console.log(history[index].pi)
    }
    if (length >= detectDumpOn){
        // console.clear();
        // console.log(pair + ": " + history[index].lp + " ---- " + history[index].pi + " ---- time: " + (new Date().toLocaleTimeString()) )
        // if(index < 1){trendBeep("down-chime.mp3")}
        if(index < 1){speak(pair.split("_")[1] + " possible dump")}
        return true
    }
    return false
    
}


function detectPump2(history, index, largeChangeAmount,pair){
    //detect rapid down trend
    
    var count = 0;
    if(!history){ return false}

    var maxLength = history.length - index - 1;
    var length = Math.min(maxLength,detectPumpOn)
    for(i=0;i < length; i++){
        if(history[index+i].pc < largeChangeAmount ){
            return false
        }
        count ++
        // console.log(history[index].pi)
    }
    if (length >= detectPumpOn){
        // console.clear();
        // console.log(pair + ": " + history[index].lp + " ---- " + history[index].pi + " ---- time: " + (new Date().toLocaleTimeString()) )
        // if(index < 1){trendBeep("notification-up.wav")}
        if(index < 1){speak(pair.split("_")[1] + " possible pump")}
        return true
    }
    return false
    
}