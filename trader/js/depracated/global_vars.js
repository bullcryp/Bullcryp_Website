/***global variables */

//flag to use for script loader knowing if we've already loaded scripts in index file for debugging
var initComplete = false

//global flash object for data input 
var flash;

/**Pair Vue Models Enum */
const pairModelEnum = {
    pair: 0,
    last: 1,
    lowestAsk: 2,
    highestBid: 3,
    percentChange: 4,
    baseVolume: 5,
    quoteVolume: 6,
    isFrozen: 7,
    low24hr: 8,
    high24hr: 9,
    range24hr: 10,
    WS: 11,
    C: 12,
    PL: 13,
    historyBarLength: 14,
    sameCount: 15
}