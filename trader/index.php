
<!DOCTYPE html>

<?php

header( 'Cache-Control: max-age=37739520, public' );
  /**
   * initialize Hybridauth.
   */
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  include '../oauth/vendor/autoload.php';
  include '../oauth/config.php';

  use Hybridauth\Hybridauth;

  $hybridauth = new Hybridauth($config);
  $adapters = $hybridauth->getConnectedAdapters();

  if (!$adapters){
  // Not Logged In With Oauth -->
        //.. so Check for Regular login -->
        // First we execute our common code to connection to the database and start the session 
        require_once("../login/common.php"); 

         // At the top of the page we check to see whether the user is logged in or not 
         if(empty($_SESSION['user'])) 
         { 
             // If they are not, we direct to the login page. 
             header('Location: https://bullcryp.com/oauth/login.php');
         } 
  }
?>
             <!-- we are logged in.. stay here -->
              
          <?php if(!empty($_SESSION['user'])): ?>
             <script id="session_user" type="text/javascript">
              var idr = <?php echo json_encode($_SESSION['user']); ?>;
              var oauthProviders = undefined ;
             </script>
         <?php endif; ?>


<!-- login check end-->

<!-- login check - if logged in -->
<?php if ($adapters) : ?> 
    <!-- user is logged in -->
    <script id="oauth">
        var oauthProviders = [{}]
        // var token = {}
        // var logoutLink = {}
        <?php $pcount = 0; ?>
        <?php foreach ($adapters as $Name => $adapter) : ?>
            <?php $name = strtolower($Name) ?>
            oauthProviders<?php echo '[' . $pcount . ']'; ?> = <?php echo json_encode($adapter->getUserProfile()) . "\n\r"; ?>
            oauthProviders<?php echo '[' . $pcount . ']'; ?>.token = <?php echo json_encode($adapter->getAccessToken()) . "\n\r"; ?>
            oauthProviders<?php echo '[' . $pcount . ']'; ?>.providerName = '<?php echo $name . "' \n\r"; ?>
            oauthProviders<?php echo '[' . $pcount . ']'; ?>.logoutLink = '<?php print $config['callback'] . "?logout={$Name}"; ?>'
            <?php $pcount++ ?>
        <?php endforeach; ?>

    
    </script>
<?php endif; ?>
<!-- login end -->


<!-- Page start -->
<html>

<head>
    <title>Bullcryp Trader</title>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="Public, max-age=30, ">

    <!-- Service worker manifest -->
      <link rel="manifest" href="manifest.json" crossorigin="use-credentials">

      <script src="swloader.js"></script>

<!-- @@@ load early css files -->
<?php include("earlycss.html") ?>

    <!-- Favicon -->
        <link rel="icon" href="favicon.ico" sizes="32x32">

</head>

<body>
<!-- Main Container -->
<div id="app">

  <!-- Left Toolbar -->
  <div id="toolbar" class="transition">
      
      <div id="logo" class="transition">
        <img id="bullcryp_logo" src="./imgs/logo.png" alt="Bullcryp logo" class="logo transition" style="width: 60px;">
        <br>BETA
      </div>
      <div class="navigation" id="nav">
          <!-- <i class="fas fa-angle-double-left"></i> -->
          <div class="navel-wrap">
            <div class="nav_element transition waves-effect selected" id="Overview" title="Overview" data-sec="overview"><div><img src="./imgs/Overview_icon.png" alt="overview icon"></div><div>Overview</div></div>
            <div class="nav_element transition waves-effect" id="Trade" title="Trade" data-sec="trade"><div><img src="./imgs/Trade_icon.png" alt="trade icon"></div><div>Trade</div></div>
            <div class="nav_element transition waves-effect" id="Settings" title="Settings"  data-sec="settings"><div><img src="./imgs/Settings_icon.png" alt="settings icon"></div><div>Settings</div></div>
            <div class="nav_element transition waves-effect" id="Feedback" title="Feedback"  data-sec="feedback"><div><img src="./imgs/feedback_icon.png" alt="Feedback icon"></div><div>Feedback</div></div>
            <div class="nav_element transition waves-effect" id="Logout" title="Log Out"><div><img src="./imgs/power-button.png" alt="logout icon"></div><div id = "logoutlink">Log Out</div></div>            
            <!-- <div class="controls" id="dash_control"><img class="icon_images" src="./imgs/keyboard-right-arrow-button.png"></div> -->
          </div>
        </div>
  </div>
  <!-- end left toolbar -->

  <!-- Right side content -->
  <div id="main" class="transition">
      
      <!-- Header -->
      <div id="header">
        <div class="headertint"></div>
        <div id="exStatus" style="opacity: 1;">
          <div>Exchange Status: </div>
          <div v-for="ex in data.settings.exchanges">{{ex.name}} <div :class="{'redot':!getExchangeAPIStatus(ex), 'greendot':getExchangeAPIStatus(ex)}"></div></div>
        </div>
        <div style="display: grid;grid-template-columns: 1fr 0.2fr;position: absolute;left: 50%;top: 50%;transform: translateX(-50%) translateY(-50%);">
          <h1 class="greeting transition" id="greeting"></h1>
          <div class="userpic transition">
            <img id="userpic" class="transition">
          </div>
          <div class="greeting-wrap transition">
            <h3 class="currPortfolio transition" id="currPortfolio">Your Portfolio Value is: <span v-if="!data.currentState.visibility.portBalanceBTC">${{ totalUSD(null,2) }} USD</span> <span v-if="data.currentState.visibility.portBalanceBTC">{{calcBTC.total}} BTC</span><div class="pbswitch"><input type="checkbox"  id="portBalance" v-model="data.currentState.visibility.portBalanceBTC" @input="()=>{data.currentState.visibility.portBalanceBTC = !data.currentState.visibility.portBalanceBTC}" ></div> </h3>
          </div>
        </div>
      </div>
      <!-- End Header -->
      <div id="tvtickerholder">
        <div id="tvtickerBTC" class="tvtick tvtickerbtc"></div>
        <div id="tvtickerUSD" class="tvtick tvtickerusd"></div>
      </div>
      <!-- TradingView Widget END -->

      <!-- Sections Holder -->
      <div class="sections">
              
          <!-- Overview Section -->
          <div id="overview" class="section">

            <div id="holdings_overview" class="tabcontentholder">
              <div id="portfolio-title">
                <div><h3 class="sectiontitle">Portfolio Overview</h3></div>
                <!-- <div class="btcprice"><span v-if="getExchangeAPIStatus({shortname:'bin'})">Binance</span><span v-if="!getExchangeAPIStatus({shortname:'bin'})">Poloniex</span> BTC Price: {{BTCPrice()}} <img id="btchistmini" class="sparkline" title="7 Day Trend"  alt="7 Day Trend" src=""></div>https://s2.coinmarketcap.com/generated/sparklines/web/7d/usd/1.png -->
              </div>
              <div id="overview_home" class="tab-pane fade in show active" role="tabpanel">
                <div id="accounthistorywrap">
                  <img id="hhph" src="imgs/line_chart_placeholder.png">
                  <!-- <canvas id="account_history">Loading...</canvas> -->
                </div>
                <div id="overviewChartWrap">
                  <!-- <canvas id="holdings" width="100%" height="100%">Loading...</canvas> -->
                  <img id="hph" src="imgs/pie_placeholder.png">
                </div>
              </div>
            </div>

            <div id="market_status" class="tabcontentholder">
              <div id="status_title"><h3 class="sectiontitle">Market Overview</h3></div>
              <div style="width: 300px;display: grid;grid-template-columns: 1fr;align-items: center;grid-column-start: 2;">
              <div>
                <button id="refreshCMC" class="btn btn-warning noshadow bullbutton" onclick="refreshCMC()">
                <i class="fas fa-sync-alt fa-5x" ></i>
                </button>
              </div>  
              <div>
                  <P>Of the top 700 coins, there are:</P>
                  <span id="refreshcmc" class="medium-heavy hide" ><img src="imgs/spinner.gif" style="width: 30px"> </span>
                  <span class="heavy hide gainvslose"> {{getGainers(1000).length}}</span> Gainers vs <span class="heavy hide gainvslose">{{getLosers(1000).length}}</span> Losers
                </div>
              </div>
              <div id="status_chart">
                <div class="mov-wrap">
                  <div class="status_gainers status-gl"><div class="sg_label">Gainers</div></div>
                  <div class="status_losers status-gl"><div class="sl_label">Losers</div></div>
                </div>
              </div>
            </div>

            <div id="gainers_overview" class="tabcontentholder">
              <div><h3 class="sectiontitle">Today's Top Gainers</h3></div>
              <div id="gainers"></div>
            </div>
            <div id="losers_overview" class="tabcontentholder">
              <div><h3 class="sectiontitle">Today's Top Losers</h3></div>
              <div id="losers"></div>
            </div>
            <div id="news_overview" class="tabcontentholder">
              
               <div><h3 class="sectiontitle">Latest News</h3></div>

               
              
            </div>

          </div>
          <!-- End Overview Section -->

          <!-- Trade Section -->
          <div id="trade" style="display: none" class="section">
              
            <!-- Trade Content Panels -->
            <div id="trade_charts" class="tabcontentholder">
              <!-- <div id="trade_home" class="tab-pane fade in show active" role="tabpanel"> -->
                  
              <!-- <div><h3 class="sectiontitle">Charts</h3></div> -->
                <!-- Trade Nav Tabs -->
                <!-- <div id="trade_tabs" class="tabsholder">
                    <ul class="nav nav-tabs nav-justified">
                      <li class="nav-item waves-effect">
                          <a class="nav-link active" data-toggle="tab" href="#tv-trade_basic" role="tab">Home</a>
                      </li>
                      <li class="tab-gap">Advanced Charts -></li> -->
                            <!-- <li class="nav-item waves-effect">
                                <a class="nav-link" data-toggle="tab" href="" role="tab">Binance:USDTBTC</a>
                            </li> -->
                      <!-- <li id="add_advanced" class="nav-item waves-effect">
                          <a class="nav-link add_advanced" title="Create Advanced Chart" href="#">+</a>
                      </li>
                    </ul> -->
                <!-- </div> -->
                <!-- <div id="trade_advanced" class="tab-pane fade" role="tabpanel">
                  <!-- Chart Tabs 
                  <div id="chart_tabs" class="tabsholder">
                    <ul class="nav nav-tabs nav-justified">
                      <li class="nav-item waves-effect charttab">
                          <a class="nav-link active" data-toggle="tab" href="" role="tab">BTCUSD</a>
                      </li>
                      <li class="nav-item waves-effect charttab">
                          <a class="nav-link" data-toggle="tab" href="" role="tab">+ Add Chart</a>
                      </li>
                    </ul>
                  </div>
                  <!-- END Chart Tabs 
                </div> -->
                <!-- TradingView Widget -->
                <div id="chart_selector" class="chart_selector">
                    <!-- <div style="float:left;margin: 0 10px;">Buy with </div> -->
                    <span style="font-weight:bold">Basic </span>
                      <input type="checkbox" id="chart_type" v-model="data.currentState.advancedChartToggle" @click="toggleChartType(data.currentState.advancedChartToggle)">
                    <span style="font-weight:bold"> Advanced</span>  
                </div>
                <div id="tvlabel">{{data.currentState.clicked.exchange.name || "Binance"}} </div> <!-- {{data.currentState.clicked.coin}} -> {{data.currentState.clicked.market}}-->
                <div class="tradingview-widget-container"><div id="tv-trade_basic" class="tvchartb"></div></div>
              <!-- </div> -->
            </div>
            
            <div id="trade_section" class="tabcontentholder">
          <!-- make buy section -->
              <div id="buy"></div>
          <!-- end buy section -->
            </div>

            <div id="mystuff" class="tabcontentholder">
              <!-- holdings, orders, mover, watchlist Tabs -->
              <div><h3 class="sectiontitle">My Coins</h3></div>
              
              
              <!--END Exchange Tabs -->
              <!-- <div style="float:left;margin: 10px 20px 0 10px;">Select Exchange</div> -->
              <div id="homw_tabs" class="tabsholder">
                <ul class="nav nav-tabs nav-justified">
                  <li class="nav-item waves-effect exchangetab">
                      <a class="nav-link active" data-toggle="tab" role="tab" @click='homwTab("holdingspane")'>Holdings</a>
                  </li>
                  <li class="nav-item waves-effect exchangetab">
                      <a class="nav-link" data-toggle="tab" role="tab" @click='homwTab("orderspane")'>Orders</a>
                  </li>
                  <li class="nav-item waves-effect exchangetab">
                      <a class="nav-link" data-toggle="tab" role="tab" @click='homwTab("moverspane")'>Market Movers</a>
                  </li>
                  <!-- <li class="nav-item waves-effect exchangetab">
                      <a class="nav-link" data-toggle="tab" role="tab" @click='homwTab("watchpane")'>Watchlist</a>
                  </li> -->
                </ul>
              </div>
              <!--END holdings, orders, mover, watchlist Tabs -->
              <!--holdings, orders, mover, watchlist Panels -->

              <div id="mystuff-wrap">
                <div id="homw_wrap">
                  <div id="holdingspane" :class="{'in show':homwTab() == 'holdingspane', 'tab-pane fade active homw':true}" role="tabpanel">
                    <div id="ho"></div>
                  </div>
                  <div id="orderspane" :class="{'in show':homwTab() == 'orderspane', 'tab-pane fade active homw':true}" role="tabpanel">
                    <div id="or"></div>
                  </div>
                  <div id="moverspane" :class="{'in show':homwTab() == 'moverspane', 'tab-pane fade active homw':true}" role="tabpanel">
                    <div id="mo"></div>
                  </div>
                  <div id="watchpane" :class="{'in show':homwTab() == 'watchpane', 'tab-pane fade active homw':true}" role="tabpanel">
                    <div id="wa"></div>
                  </div>
                </div>

                
                
              </div>


                      <!--End holdings, orders, mover, watchlist Panels -->
            </div>




              



                
          </div>
          <!-- End Trade Section -->

          <!-- Settings Section -->
          <div id="settings" style="display: none"  class="section">
              <!-- settings content -->
              <!-- <div id="settings_content" class="tabcontentholder">
                <div><h3 class="sectiontitle">Settings</h3></div> -->
                <!-- Settings tabs -->
                <!-- <div id="settings_tabs" class="tabsholder">
                  <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item waves-effect">
                        <a class="nav-link" data-toggle="tab" href="#settings_general" role="tab">General</a>
                    </li>
                    <li class="nav-item waves-effect">
                        <a class="nav-link active" data-toggle="tab" href="#settings_exchanges" role="tab">Exchanges</a>
                    </li>
                    <li class="nav-item waves-effect">
                        <a class="nav-link" data-toggle="tab" href="#settings_dashboard" role="tab">Dashboard</a>
                    </li>
                  </ul>
                </div> -->
                <!-- End settings tabs -->
                <!-- settings panels -->
                <div id="settings_exchanges" class="tabcontentholder"></div>
                <div id="settings_dashboard" class="tabcontentholder"></div>
                <!-- <div id="settings_general" class="tabcontentholder"> -->
                <!-- <div><h3 class="sectiontitle">General Settings</h3></div>
                </div> -->
                <!-- end settings panels -->

              <!-- </div> -->
              <!-- End settings content -->
          </div>
          <!-- End settings section -->

          <!-- feedback Section -->
          <div id="feedback" style="display: none"  class="section"></div>
          <!-- End feedback section -->

          <!-- Info (Terms, privacy) Section -->
          <div id="info" style="display: none"  class="section">
          </div>
          <!-- End Terms section -->

      </div>
      <!-- end sections holder-->


      <footer class="page-footer tabcontentholder">
    <!-- scroll to top button -->
    <!-- <a href="#" id="sctop" class="r_but" title="Go to top" style="display: block;">Top</a> -->
    <div class="footer-container">
  
      <div class="footer-logo-container">
      <div class="footer-logo"></div>
        <div>
          <h5>BullCryp</h5>
          <span class="footer-text">Cryptocurrency Trading, Reinvented.</span><br>
          <span class="footer-orange">Made with ♥ in Vancouver, Canada</span>
        </div>
      </div>
      <div>
          <h5>Connect</h5>
          <div class="socialgrid ">
              <div>
                <a href="https://www.facebook.com/bullcryp " target="_blank " title="BullCryp Facebook">
                  <img class="social " src="images/icons/facebook.png ">
                </a>
              </div>
              <div>
                <a href="https://www.instagram.com/bullcryp/ " target="_blank " title="BullCryp Instagram">
                  <img class="social " src="images/icons/instagram.png ">
                </a>
              </div>
              <div>
                <a href="https://twitter.com/realbullcryp " target="_blank " title="BullCryp Twitter ">
                  <img class="social" src="images/icons/twitter.png ">
                </a>
              </div>
              <div>
                <a href="https://www.linkedin.com/company/bullcryp/" target="_blank " title="BullCryp LinkedIn">
                  <img class="social" src="images/icons/linkedin.png ">
                </a>
              </div>
              <div>
                <a href="https://medium.com/bullcryp" target="_blank " title="BullCryp Medium">
                  <img class="social" src="images/icons/medium.png ">
                </a>
              </div>
          </div>
      </div>
      <div class="footer-contact">
        <a href="">Contact </a> <br>
        <a href="https://bullcryp.com/terms.html" onclick="javascript:void window.open('https://bullcryp.com/terms.html','1537382275936','width=700,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Terms / Privacy</a> <br>
        <span class="footer-orange">
          Copyright © 2018 Bullcryp
        </span>
      </div>
    </div>
  </footer>
  </div>
  <!-- End right side content -->

</div>
<!-- End app Container -->

</body>
  
<!-- @@@ load all js files -->
<?php include("scripts.html") ?>

  <!-- late css -->
  <!-- Sweet Alert 2 -->
  <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.10/dist/sweetalert2.min.css" type="text/css" rel="stylesheet">
  <!-- select2 -->
  <link href='css/select2.min.css' type='text/css' rel='stylesheet'>
  <!-- JSON Human -->
  <link href="css/json.human.css" rel="stylesheet" type="text/css">

<!-- @@@ load tracking js file -->
<?php include("tracking.html") ?>
    



</html>
