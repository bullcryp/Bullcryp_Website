<?php

namespace AndreasGlaser\Helpers\Interfaces;

/**
 * Interface FactoryInterface
 *
 * @package AndreasGlaser\Helpers\Interfaces
 * @author  Andreas Glaser
 */
interface FactoryInterface
{
    public static function f();
}