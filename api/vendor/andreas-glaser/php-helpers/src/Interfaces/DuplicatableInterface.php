<?php

namespace AndreasGlaser\Helpers\Interfaces;

/**
 * Interface DuplicatableInterface
 *
 * @package AndreasGlaser\Helpers\Interfaces
 * @author  Andreas Glaser
 */
interface DuplicatableInterface
{
    public function duplicate();
}