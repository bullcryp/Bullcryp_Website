<?php

namespace AndreasGlaser\Helpers\Interfaces;

/**
 * Interface RendererInterface
 *
 * @package AndreasGlaser\Helpers\Interfaces
 * @author  Andreas Glaser
 */
interface RendererInterface
{
    public function render($data);
}