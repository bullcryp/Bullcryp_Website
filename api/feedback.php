<?php

// First we execute our common code to connection to the database
//require 'db.php'; // mysql pdo functions
require_once("../login/common.php"); 
  
if(!empty($_POST)){
  if(isset($_POST["email"])){

    $email = $_POST["email"];
    $exchange = $_POST["newexchange"];
    $dislike = $_POST["dislike"];
    $features = $_post["features"];

    $query = ' 
                INSERT INTO feedback ( 
                    email, 
                    nextexchange, 
                    dislike, 
                    features
                ) VALUES ( 
                    :email, 
                    :nextexchange, 
                    :dislike, 
                    :features
                ) 
            '; 
    $query_params = array( 
      ':email' => $email, 
      ':nextexchange' => $exchange, 
      ':dislike' => $dislike, 
      ':features' => $features
    ); 

    $stmt = $conn->prepare($query); 
    $result = $stmt->execute($query_params);

  };
};