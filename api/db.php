<?php
$servername = "localhost";
$username = "polo_user";
$password = "";
$dbname = "bullcryp";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } 
catch(PDOException $e) 
    {
	echo "Error: " . $e->getMessage();
    }

function db_check_email($email) { // check if email is registered, returns 0 or 1
     global $conn;
	$stmt = $conn->prepare("SELECT count(*) as count from useraccounts where email=:email");  
	$stmt->bindParam(':email', $email);
	$stmt->execute();

	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	return $row['count'] ;
}

//echo db_check_email('alkhoo@gmail.com'); // debug

function db_check_token($token) { // check if email is registered, returns 0 or 1
     global $conn;
	$stmt = $conn->prepare("SELECT count(*) as count from useraccounts where access_token=:token");  
	$stmt->bindParam(':token', $token);
	$stmt->execute();

	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	// if($row['count']<1){
	// 	//check the salt for regular login
	// 	$stmt = $conn->prepare("SELECT count(*) as count from useraccounts where salt=:token");  
	// 	$stmt->bindParam(':token', $token);
	// 	$stmt->execute();
	// 	return $row['count'];
	// }
	return $row['count'] ;
}


function db_update_oauth($email, $oauth, $oauth_expiry) { // add $oauth + $oauth_expiry with $email as key
  global $conn;
  try {
	//$mysql_next_day =  date("Y-m-d H:i:s",strtotime('+1 day', $oauth_expiry) );

	$stmt = $conn->prepare("UPDATE useraccounts SET access_token=:access_token  where email=:email");
	$stmt->bindParam(':access_token', $oauth);
	//$stmt->bindParam(':access_token_expiry', $mysql_next_day);
	$stmt->bindParam(':email', $email);
	$stmt->execute();

	return $stmt->rowCount();
  }
  catch(PDOException $e) {
	echo $e->getMessage();
  }
}
//echo db_add_oauth('alkhoo@gmail.com', uniqid(), time()); // debug
function db_update_settings($oauth, $settings) { // add $oauth + $oauth_expiry with $email as key
  global $conn;
  try {
	//$mysql_next_day =  date("Y-m-d H:i:s",strtotime('+1 day', $oauth_expiry) );

	$stmt = $conn->prepare("UPDATE useraccounts SET account_settings=:settings WHERE access_token=:access_token ");
	$stmt->bindParam(':settings', $settings);
	$stmt->bindParam(':access_token', $oauth);
	//$stmt->bindParam(':access_token_expiry', $mysql_next_day);
	$stmt->execute();

	return $stmt->rowCount();
  }
  catch(PDOException $e) {
	echo $e->getMessage();
  }
}

function db_account_login($email, $oauth, $firstname, $lastname) {
  global $conn;
  try {

	$stmt = $conn->prepare("INSERT INTO useraccounts (access_token, email, firstname, lastname, IP) VALUES (:access_token, :email, :firstname, :lastname, :IP)  ON DUPLICATE KEY UPDATE logins = logins + 1, IP=:IP, access_token=:access_token2");
	$stmt->bindParam(':access_token', $oauth);
	$stmt->bindParam(':access_token2', $oauth);
	$stmt->bindParam(':firstname', $firstname);
	$stmt->bindParam(':lastname', $lastname);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':IP', $_SERVER['REMOTE_ADDR']);
	$stmt->execute();

	return $stmt->rowCount();
  }
  catch(PDOException $e) {
	echo $e->getMessage();
  }
}

?>
