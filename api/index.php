<?php

require 'vendor/autoload.php';
require 'db.php'; // mysql pdo functions

header("Access-Control-Allow-Origin: *");

$settings =  [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];



$app = new Slim\App($settings);
###
# Database connection
###
$redis = new Redis();
$redis->connect('127.0.0.1', 6379);

###
# Poloniex connection
###
use AndreasGlaser\PPC\PPC;

$app->post('/polo/{cmd}', function ($request, $response, $args)  {
	$allPostPutVars = $request->getParams();
	$polo_apiKey = $allPostPutVars['key'];
	$polo_apiSecret = $allPostPutVars['secret'];
	$polo_apiParams =  isset($allPostPutVars['params']) ? json_decode($allPostPutVars['params']) : '';
	$token_partial = $allPostPutVars['token'];

	if (!db_check_token($token_partial)) { 
		return $response->getBody()->write("unauthorized token");
	} else {

		if ( ($args['cmd'] == "dephistory") || ($args['cmd'] == "account") || ($args['cmd'] == "history") || ($args['cmd'] == "movers") || ($args['cmd'] == "api") || (isset($polo_apiSecret) && isset($polo_apiKey)) ) { // found API keys, process it)
		//if (isset($polo_apiSecret) && isset($polo_apiKey)) { // found API keys, process it)
			//$ppc = new PPC($polo_apiKey, $polo_apiSecret);
			//return $response->getBody()->write("Hello (good body), " . $polo_apiKey ." - ". $polo_apiSecret ." - ". $polo_apiParams );
			polo_private($response, $args['cmd'], $polo_apiKey, $polo_apiSecret, $polo_apiParams); 
		} else {
			return $response->getBody()->write("Hello (bad Body), ");
		}
	}

});

$app->post('/binance/{cmd}', function ($request, $response, $args)  {
	$allPostPutVars = $request->getParams();
	$bin_apiKey = $allPostPutVars['key'];
	$bin_apiSecret = $allPostPutVars['secret'];
	$token_partial = $allPostPutVars['token'];

	if (!db_check_token($token_partial)) { 
		return $response->getBody()->write("unauthorized token");
	} else {
		$bin_apiParams =  isset($allPostPutVars['params']) ? json_decode($allPostPutVars['params']) : '';
		
		if ( ($args['cmd'] == "dephistory") || ($args['cmd'] == "history") || ($args['cmd'] == "account") || ($args['cmd'] == "movers") || (isset($bin_apiSecret) && isset($bin_apiKey)) ) { // found API keys, process it)
			//$ppc = new PPC($polo_apiKey, $polo_apiSecret);
			//return $response->getBody()->write("Hello (good body), " . $polo_apiKey ." - ". $polo_apiSecret ." - ". $polo_apiParams );
			binance_private($response, $args['cmd'], $bin_apiKey, $bin_apiSecret, $bin_apiParams); 
		} else {
			return $response->getBody()->write("Hello (bad Body), Requires 'bin_key' and 'bin_secret' in params ");
		}
	}

});

$app->post('/settings/{cmd}', function($request, $response, $args) {
	$allPostPutVars = $request->getParams();
	$settings_params =  isset($allPostPutVars['params']) ? json_decode($allPostPutVars['params']) : '';
	$settings = isset($allPostPutVars['settings']) ? json_decode($allPostPutVars['settings']) : '';
	$token_partial = $allPostPutVars['token'];

	if (!db_check_token($token_partial)) { 
		return $response->getBody()->write("unauthorized token");
	} else {
		settings_private($response, $args['cmd'], $token_partial, $settings_params); 
	}
});

function settings_private($response, $cmd, $params){
	switch ($cmd) {
		case 'load':


		case 'save':


		default:
		return $response->getBody()->write( '{ "status": "failed", "message": "'. $cmd .' command not found" }' );
		break;

	}
};

$app->get('/user/{cmd}/{data}', function($request, $response, $args) {
	$allPostPutVars = $request->getParams();
	$settings = isset($allPostPutVars['param']) ? json_decode($allPostPutVars['param']) : '';
	user_private($response, $args['cmd'], $args['data']); 
});

function user_private($response, $cmd, $data){
	switch ($cmd) {
		case 'subscribed':
			try {
				$user_results = db_check_email($data);
				$response->getBody()->write( '{ "status": "OK", "subscribed": "'. $user_results .'" }' );
				return $response;
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
		break;
		case 'access_token':
			try {
				$user_results = db_check_token($data);
				$response->getBody()->write( '{ "status": "OK", "token_found": "'. $user_results .'" }' );
				return $response;
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
		break;


		default:
		return $response->getBody()->write( '{ "status": "failed", "message": "'. $cmd .' command not found" }' );
		break;

	}
};

$app->run();

function polo_private($response, $cmd, $key, $secret, $params) {
  global $redis;

  if (isset($secret) && isset($key)) { // found API keys, process it)
	$polo = new PPC($key, $secret); 
  }

  switch ($cmd) {
	case 'api':
		try {  //TODO move this to user_private
			$feed = $params->feed;
			$words = $params->words;
			$articles = $params->articles;
			$arr = $params->arr;
			$type = $params->type;
			$isxml = $params->isxml;
			if($type == 'raw'){
				//grab raw API data from outside API
				$data = file_get_contents($feed);
				return $response->getBody()->write(json_encode($data));
			}
			//else, we're looking for an rss feed
			if($arr){
				$feedarr = get_rss_feed_as_array($feed,$articles,true,true,$words, $isxml);
				return $response->getBody()->write(json_encode($feedarr));
			}else{
				$feedBody = get_rss_feed_as_html($feed,$articles,true,true,$words, $isxml);
				return $response->getBody()->write($feedBody);
			}
		}catch (Exception $e) {
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;
	case 'account':
		try {//TODO
			$account = $polo->getFeeInfo();
			return $response->getBody()->write( json_encode( $account ) );
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;
	case 'movers':
		if ($redis->ping()){ 
			$polo_movers = $redis->get('polo_movers'); 
			return $response->getBody()->write($polo_movers );
		} else {
			return [];
		}
		break;
	case 'balances':
		try {
			$polo_results = json_decode($polo->getCompleteBalances()->contents);
			$polo_results_filter = array_filter((array)$polo_results, function($e) {
				return ($e->btcValue != 0  || $e->onOrders !=0 || $e->available !=0) ;
			});
			return $response->getBody()->write(json_encode($polo_results_filter));
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	case 'buy':
		$currencyPair = $params->currency;
		$rate = $params->rate;
		$amount = $params->amount;
		$fillOrKill = $params->fillOrKill;
		$immediateOrCancel = $params->immediateOrCancel;
		$postOnly = $params->postOnly;

		//$response->getBody()->write('Buy API '. $key .' - '. $secret .' - '. json_encode($params) ); // debug
		try {
			$polo_results = ($polo->buy($currencyPair, $rate, $amount, $fillOrKill, $immediateOrCancel, $postOnly)->contents);
			$response->getBody()->write( $polo_results );
			return $response;
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	case 'sell':
		$currencyPair = $params->currency;
		$rate = $params->rate;
		$amount = $params->amount;
		$fillOrKill = $params->fillOrKill;
		$immediateOrCancel = $params->immediateOrCancel;
		$postOnly = $params->postOnly;

		try { 
			$polo_results = ($polo->sell($currencyPair, $rate, $amount, $fillOrKill, $immediateOrCancel, $postOnly)->contents);
			$response->getBody()->write( $polo_results );
			return $response;
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	case 'orders':
		try {
			$polo_results = json_decode($polo->getOpenOrders()->contents);
			$polo_results_filter = array_filter((array)$polo_results, function($element) {
				return (isset($element[0])) ;
				//return true;
			});
			return $response->getBody()->write(json_encode($polo_results_filter));
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	case 'moveOrder':
		$orderNumber = $params->orderNumber;
		$rate = $params->rate;
		$amount = $params->amount;
		$postOnly = $params->postOnly;
		$immediateOrCancel = $params->immediateOrCancel;

		try {
			$polo_results = ($polo->moveOrder($orderNumber, $rate, $amount,$postOnly,$immediateOrCancel)->contents);
			$response->getBody()->write( $polo_results );
			return $response;
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	case 'cancelOrder':
		$orderNumber = $params->orderNumber;

		try {
			$polo_results = ($polo->cancelOrder($orderNumber)->contents);
			$response->getBody()->write( $polo_results );
			return $response;
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;
	case 'history':
		try {//TODO
			$currency = $params->currency;
			$start = $params->start;
			$end = $params->end;

			$history = $polo->getTradeHistory($currency,$start,$end);
			return $response->getBody()->write( json_encode( $history ) );
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;
	case 'dephistory':
		try {//TODO
			$history = $polo->depositHistory();
			return $response->getBody()->write( json_encode( $history ) );
		} catch (Exception $e){
			$response->getBody()->write($e->getMessage());
			return $response;
		}
		break;

	default:
		return $response->getBody()->write( '{ "status": "failed", "message": "'. $cmd .' command not found" }' );
		break;

  }

}

function binance_private($response, $cmd, $binance_apiKey, $binance_apiSecret, $params) {
	global $redis;
  
	if (isset($binance_apiSecret) && isset($binance_apiKey)) { // found API keys, process it)
		$bin = new Binance\API($binance_apiKey, $binance_apiSecret);
		// $bin = new Binance\RateLimiter($bin);  //TODO remove Binance RateLimiter
	}
  
	switch ($cmd) {
		case 'account':
			try {
				$account = $bin->account();
				return $response->getBody()->write( json_encode( $account ) );
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'movers':
			if ($redis->ping()){ 
				$binance_movers = $redis->get('binance_movers'); 
				return $response->getBody()->write($binance_movers );
			} else {
				return [];
			}
			break;
		case 'balances':
			try {
				//$ticker = $bin->prices(); not needed..
				$balances = $bin->balances(); //json_decode( $bin->balances() );
				return $response->getBody()->write( json_encode( $balances)  );
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'buy':
			try{
				$currencyPair = $params->currency;
				$rate = $params->rate;
				$amount = $params->amount;
				$result = $bin->buy($currencyPair,$amount,$rate);
				return $response->getBody()->write( json_encode( $result)  );
			}catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'sell':
			try{
				$currencyPair = $params->currency;
				$rate = $params->rate;
				$amount = $params->amount;
				$result = $bin->sell($currencyPair,$amount,$rate);
				return $response->getBody()->write( json_encode( $result)  );
			}catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'orders':
			try {
				$orders = $bin->openOrders();
				return $response->getBody()->write( json_encode( $orders)  );
			}catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'cancelOrder':
			try{
				$currency = $params->currency;
				$orderId = $params->orderNumber;
				$result = $bin->cancel($currency,$orderId);
				return $response->getBody()->write( json_encode( $result)  );
			}catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'history':
			try {
				$currency = $params->currency;
				$limit = 1000;
				$startTime = $params->startTime;

				$history = $bin->history($currency, $limit, $startTime);
				return $response->getBody()->write( json_encode( $history ) );
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;
		case 'dephistory':
			try {
				$history = $bin->depositHistory();
				return $response->getBody()->write( json_encode( $history ) );
			} catch (Exception $e){
				$response->getBody()->write($e->getMessage());
				return $response;
			}
			break;


		default:
			return $response->getBody()->write( '{ "status": "failed", "message": "'. $cmd .' bin:command not found" }' );
		break;
  
	}
}

function output_rss_feed($feed_url, $max_item_cnt = 10, $show_date = true, $show_description = true, $max_words = 0)
{
    echo get_rss_feed_as_html($feed_url, $max_item_cnt, $show_date, $show_description, $max_words);
}


function get_rss_feed_as_html($feed_url, $max_item_cnt = 10, $show_date = true, $show_description = true, $max_words = 0, $isxml = false, $cache_timeout = 7200, $cache_prefix = "/tmp/rss2html-")
{
    $result = "";
    // get feeds and parse items
    $rss = new DOMDocument();
    $cache_file = $cache_prefix . md5($feed_url);
    // load from file or load content
    if ($cache_timeout > 0 &&
        is_file($cache_file) &&
        (filemtime($cache_file) + $cache_timeout > time())) {
            $rss->load($cache_file);
    } else {
				if($isxml){
					$rss->loadXML($feed_url);
				}else{
					$rss->load($feed_url);
				}
        
        if ($cache_timeout > 0) {
            $rss->save($cache_file);
        }
    }
    $feed = array();
    foreach ($rss->getElementsByTagName('item') as $node) {
        $item = array (
            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
            'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'content' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
            'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
        );
        $content = $node->getElementsByTagName('encoded'); // <content:encoded>
        if ($content->length > 0) {
            $item['content'] = $content->item(0)->nodeValue;
        }
        array_push($feed, $item);
    }
    // real good count
    if ($max_item_cnt > count($feed)) {
        $max_item_cnt = count($feed);
    }
    $result .= '<ul class="feed-lists">';
    for ($x=0;$x<$max_item_cnt;$x++) {
        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
        $link = $feed[$x]['link'];
        $result .= '<li class="feed-item">';
        $result .= '<div class="feed-title"><strong><a href="'.$link.'" target="_blank" title="'.$title.'">'.$title.'</a></strong></div>';
        if ($show_date) {
            $date = date('l F d, Y', strtotime($feed[$x]['date']));
            $result .= '<small class="feed-date"><em>Posted on '.$date.'</em></small>';
        }
        if ($show_description) {
            $description = $feed[$x]['desc'];
            $content = $feed[$x]['content'];
            // find the img
            $has_image = preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $image);
            // no html tags
            $description = strip_tags(preg_replace('/(<(script|style)\b[^>]*>).*?(<\/\2>)/s', "$1$3", $description), '');
            // whether cut by number of words
            if ($max_words > 0) {
                $arr = explode(' ', $description);
                if ($max_words < count($arr)) {
                    $description = '';
                    $w_cnt = 0;
                    foreach($arr as $w) {
                        $description .= $w . ' ';
                        $w_cnt = $w_cnt + 1;
                        if ($w_cnt == $max_words) {
                            break;
                        }
                    }
                    $description .= " ...";
                }
            }
            // add img if it exists
            if ($has_image == 1) {
                $description = '<img class="feed-item-image" src="' . $image['src'] . '" />' . $description;
            }
            $result .= '<div class="feed-description">' . $description;
            $result .= ' <a href="'.$link.'" target="_blank" title="'.$title.'">Continue Reading &raquo;</a>'.'</div>';
        }
        $result .= '</li>';
    }
    $result .= '</ul>';
    return $result;
}

function get_rss_feed_as_array($feed_url, $max_item_cnt = 10, $show_date = true, $show_description = true, $max_words = 0, $xml = false, $cache_timeout = 7200, $cache_prefix = "/tmp/rss2html-")
{
    $result = "";
    // get feeds and parse items
    $rss = new DOMDocument();
    $cache_file = $cache_prefix . md5($feed_url);
    // load from file or load content
    if ($cache_timeout > 0 &&
        is_file($cache_file) &&
        (filemtime($cache_file) + $cache_timeout > time())) {
            $rss->load($cache_file);
    } else {
        $rss->load($feed_url);
        if ($cache_timeout > 0) {
            $rss->save($cache_file);
        }
    }
    $feed = array();
    foreach ($rss->getElementsByTagName('item') as $node) {
        $item = array (
            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
            'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'content' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
            'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
        );
        $content = $node->getElementsByTagName('encoded'); // <content:encoded>
        if ($content->length > 0) {
            $item['content'] = $content->item(0)->nodeValue;
        }
        array_push($feed, $item);
    }
    // real good count
    if ($max_item_cnt > count($feed)) {
        $max_item_cnt = count($feed);
    }
  
    return $feed;
}

?>